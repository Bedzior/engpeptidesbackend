package pl.pw.rb.eng.scan.db.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.MalformedURLException;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import pl.pw.rb.eng.scan.db.DatabaseDownloadTaskTest;
import pl.pw.rb.eng.scan.db.DatabaseDownloader;
import pl.pw.rb.eng.service.exception.NoOngoingDatabaseDownload;
import pl.pw.rb.eng.springconfig.PeptidesConfiguration;
import pl.pw.rb.eng.tests.UnitTest;

@RunWith(SpringRunner.class)
@Category({ UnitTest.class })
@ActiveProfiles("test")
public class DatabaseDownloaderTest {

	@TestConfiguration
	public static class Configuration extends PeptidesConfiguration {

		@Bean
		@Autowired
		public DatabaseDownloader service(FileAlterationMonitor monitor) {
			return new DatabaseDownloaderImpl(monitor);
		}

	}

	@MockBean
	public SimpMessagingTemplate simp;

	@Autowired
	DatabaseDownloader downloader;

	@Test
	public void testRequestDownload() throws Exception {
		final String dbName = randomDBName();
		CompletableFuture<File> future = downloader.requestDownload(dbName, DatabaseDownloadTaskTest.SMALL_DOWNLOAD);
		assertTrue(downloader.isDownloading(dbName));
		assertFalse(future.isCompletedExceptionally());
	}

	@Test
	public void testMalformedURL() throws Throwable {
		CompletableFuture<File> future = downloader.requestDownload(randomDBName(), "htp://nothing.here.com/abc.zip");
		try {
			future.get();
		} catch (ExecutionException e) {
			assertTrue(e.getCause() instanceof MalformedURLException);
		}
		assertTrue(future.isCompletedExceptionally());
	}

	@Test
	public void testCancelDownloading() throws Exception {
		final String dbName = randomDBName();
		downloader.requestDownload(dbName, DatabaseDownloadTaskTest.MEDIUM_DOWNLOAD);
		assertTrue(downloader.isDownloading(dbName));
		downloader.cancelDownloading(dbName);
		assertFalse(downloader.isDownloading(dbName));
	}

	public String randomDBName() {
		return RandomStringUtils.randomAlphabetic(new Random().nextInt(100));
	}

	@Test(expected = NoOngoingDatabaseDownload.class)
	public void testCancelDownloadingWithNoneInProgress() throws Exception {
		final String dbName = randomDBName();
		assertFalse(downloader.isDownloading(dbName));
		downloader.cancelDownloading(dbName);
	}

}
