package pl.pw.rb.eng.scan.db;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

import java.io.FileNotFoundException;
import java.net.ConnectException;
import java.util.concurrent.ExecutionException;

import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import pl.pw.rb.eng.service.integration.NetUtils;
import pl.pw.rb.eng.tests.ExternalSystemsTest;
import pl.pw.rb.eng.tests.IntegrationTest;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Category({ IntegrationTest.class })
@ActiveProfiles("test")
public class DatabaseDownloadTaskTest {

	@MockBean
	SimpMessagingTemplate template;

	@Autowired
	FileAlterationMonitor monitor;

	@Rule
	public TemporaryFolder temp = new TemporaryFolder();

	/** ~80 MB */
	public static final String MEDIUM_DOWNLOAD = "ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.fasta.gz";

	/** ~30 MB */
	public static final String SMALL_DOWNLOAD = "ftp://ftp.ncbi.nlm.nih.gov/blast/db/FASTA/igSeqNt.gz";

	@Test
	@Category(ExternalSystemsTest.class)
	public void testFromString() throws Exception {
		NetUtils.checkInternetConnection(MEDIUM_DOWNLOAD);
		String name = RandomStringUtils.randomAlphabetic(50);
		DatabaseDownloadTask task = new DatabaseDownloadTask(temp.newFolder(), template, name, MEDIUM_DOWNLOAD);
		task.setMonitor(monitor);
		new Thread(task).start();
		Thread.sleep(5 * monitor.getInterval());
		verify(template, atLeastOnce()).convertAndSend(anyString(), (Object) anyObject());
	}

	@Test
	public void testBadLink() throws Throwable {
		String name = RandomStringUtils.randomAlphabetic(50);
		DatabaseDownloadTask task = new DatabaseDownloadTask(temp.newFolder(), template, name, "http://localhost/inexistent");
		task.setMonitor(monitor);
		new Thread(task).start();
		try {
			task.get();
			fail();
		} catch (ExecutionException e) {
			assertTrue(task.isCompletedExceptionally());
			Throwable cause = e.getCause();
			// @formatter:off
			assertTrue(cause instanceof ConnectException || 
					cause instanceof FileNotFoundException);
			// @formatter:on
		}
	}

}
