package pl.pw.rb.eng.springconfig.security;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.google.common.collect.ImmutableMap;

import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.tests.UnitTest;

@RunWith(SpringRunner.class)
@Category({ UnitTest.class })
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class AuthenticationTest {

	@Autowired
	MockMvc mvc;

	@MockBean
	UsersService service;

	@Autowired
	PasswordEncoder encoder;

	@Before
	public void before() {
		when(service.getByLogin(eq("admin"))).thenAnswer(new Answer<User>() {

			@Override
			public User answer(InvocationOnMock invocation) throws Throwable {
				User user = mock(User.class);
				when(user.getLogin()).thenReturn(invocation.getArgumentAt(0, String.class));
				when(user.getEmail()).thenReturn("n@ab.com");
				when(user.getHash()).thenReturn(encoder.encode("admin"));
				when(user.isAdmin()).thenReturn(true);
				when(user.getAuthorities()).thenCallRealMethod();
				when(user.getRoles()).thenCallRealMethod();
				when(user.isActivated()).thenReturn(true);
				return user;
			}
		});
		when(service.getByLogin(eq("unconfirmed"))).thenAnswer(new Answer<User>() {
			
			@Override
			public User answer(InvocationOnMock invocation) throws Throwable {
				User user = mock(User.class);
				when(user.getLogin()).thenReturn(invocation.getArgumentAt(0, String.class));
				when(user.getEmail()).thenReturn("na@ab.com");
				when(user.getHash()).thenReturn(encoder.encode("anypassword"));
				when(user.isAdmin()).thenReturn(false);
				when(user.getAuthorities()).thenCallRealMethod();
				when(user.getRoles()).thenCallRealMethod();
				when(user.isActivated()).thenReturn(false);
				return user;
			}
		});
	}

	@Test
	public void testExistingUser() throws Exception {
		JSONObject form = new JSONObject(ImmutableMap.of("j_username", "admin", "j_password", "admin"));
		mvc.perform(post("/login").accept(MediaType.APPLICATION_JSON).content(form.toString())).andExpect(status().is2xxSuccessful());
	}

	@Test
	public void testExistingUserWithParameters() throws Exception {
		mvc.perform(post("/login").param("username", "admin").param("password", "admin")).andExpect(status().is2xxSuccessful());
	}

	@Test
	public void testExistingUserWithMissingParameter() throws Exception {
		mvc.perform(post("/login").param("username", "admin")).andExpect(status().isUnauthorized());
	}

	@Test
	public void testNonExistingUser() throws Exception {
		JSONObject form = new JSONObject(ImmutableMap.of("j_username", "user", "j_password", "abc"));
		mvc.perform(post("/login").accept(MediaType.APPLICATION_JSON).content(form.toString())).andExpect(status().isUnauthorized());
	}

	@Test
	public void testIncorrectJSON() throws Exception {
		JSONObject form = new JSONObject(ImmutableMap.of("j_username", "user", "j_password", "abc"));
		String content = form.toString();
		mvc.perform(post("/login").accept(MediaType.APPLICATION_JSON).content(content.substring(0, content.length() - 2))).andExpect(status().isUnauthorized());
	}
	
	@Test
	public void testUnconfirmedUser() throws Exception {
		JSONObject form = new JSONObject(ImmutableMap.of("j_username", "unconfirmed", "j_password", "anypassword"));
		mvc.perform(post("/login").accept(MediaType.APPLICATION_JSON).content(form.toString())).andExpect(status().isUnauthorized());
	}
}
