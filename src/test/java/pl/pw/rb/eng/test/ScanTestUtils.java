package pl.pw.rb.eng.test;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

import mscanlib.common.MScanException;
import mscanlib.ms.msms.MsMsPeptideHit;
import mscanlib.ms.msms.MsMsProteinHit;
import mscanlib.ms.msms.dbengines.mscandb.io.MScanDbOutFileReader;
import mscanlib.ms.msms.io.MsMsScanConfig;

public class ScanTestUtils {

	public static MScanDbOutFileReader readData() throws MScanException, IOException {
		try (InputStreamReader reader = readFromResources("/test_data/test.out")) {
			MScanDbOutFileReader msMsReader = new MScanDbOutFileReader(reader, null, new MsMsScanConfig());
			msMsReader.readFile();
			return msMsReader;
		}
	}

	public static Map<String, MsMsProteinHit> proteins() throws MScanException, IOException {
		try (InputStreamReader reader = readFromResources("/test_data/test.out")) {
			MScanDbOutFileReader msMsReader = new MScanDbOutFileReader(reader, null, new MsMsScanConfig());
			msMsReader.readFile();
			LinkedHashMap<String, MsMsProteinHit> proteins = new LinkedHashMap<>();
			msMsReader.createHashes(proteins, new LinkedHashMap<>());
			return proteins;
		}
	}

	public static Map<String, MsMsPeptideHit> peptides(String protein) throws MScanException, IOException {
		try (InputStreamReader reader = readFromResources("/test_data/test.out")) {
			MScanDbOutFileReader msMsReader = new MScanDbOutFileReader(reader, null, new MsMsScanConfig());
			msMsReader.readFile();
			LinkedHashMap<String, MsMsProteinHit> proteins = new LinkedHashMap<>();
			msMsReader.createHashes(proteins, new LinkedHashMap<>());
			return proteins.get(protein).getPeptides();
		}
	}

	public static InputStreamReader readFromResources(String resource) {
		return new InputStreamReader(ScanTestUtils.class.getResourceAsStream(resource));
	}

}
