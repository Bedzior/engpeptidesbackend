package pl.pw.rb.eng.web.context;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.common.collect.Lists;

import pl.pw.rb.eng.PeptidesApplication;
import pl.pw.rb.eng.db.dao.ScansDAO;
import pl.pw.rb.eng.db.dao.UsersDAO;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.scan.db.DatabaseDownloader;
import pl.pw.rb.eng.service.DatabaseService;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.springconfig.PeptidesConfiguration;
import pl.pw.rb.eng.tests.UnitTest;
import pl.pw.rb.eng.web.context.CleanInitializationTest.TestPeptidesConfiguration;

@RunWith(SpringRunner.class)
@Category({ UnitTest.class })
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = { PeptidesApplication.class, TestPeptidesConfiguration.class })
public class CleanInitializationTest {

	@ClassRule
	public static TemporaryFolder temp = new TemporaryFolder();

	@TestConfiguration
	protected static class TestPeptidesConfiguration extends PeptidesConfiguration {

		private File appDir;

		@Override
		public File appDir() {
			if (appDir == null)
				try {
					appDir = temp.newFolder();
				} catch (IOException e) {
					appDir = super.appDir();
				}
			return appDir;
		}
	}

	@BeforeClass
	public static void beforeClass() {
		System.setProperty("clean", "true");
	}

	@Autowired
	private UsersService usersService;

	@Autowired
	private UsersDAO usersDAO;

	@Autowired
	private DatabaseService databaseService;

	@Autowired
	private DatabaseDownloader downloader;

	@Autowired
	private ScansDAO scansDAO;

	@Test
	public void testClean() {
		assertEquals(2L, usersDAO.count());
		assertNotNull(usersService.getByLogin("test"));
		final User adminUser = usersService.getByLogin("admin");
		assertNotNull(adminUser);
		assertTrue(adminUser.isAdmin());
		assertTrue(!databaseService.getDatabases(false).isEmpty() || downloader.isDownloading());
		assertTrue(Lists.newArrayList(scansDAO.findAll()).stream().noneMatch(s -> s.isRunning()));
	}

}
