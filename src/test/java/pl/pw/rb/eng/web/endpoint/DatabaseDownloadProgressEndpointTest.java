package pl.pw.rb.eng.web.endpoint;

import static java.text.MessageFormat.format;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import pl.pw.rb.eng.db.dao.DatabaseDAO;
import pl.pw.rb.eng.db.dao.UsersDAO;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.scan.db.DatabaseDownloadTaskTest;
import pl.pw.rb.eng.service.integration.NetUtils;
import pl.pw.rb.eng.springconfig.security.JWTHelper;
import pl.pw.rb.eng.tests.UnitTest;

@RunWith(SpringRunner.class)
@Category({ UnitTest.class })
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureWebMvc
@ActiveProfiles("test")
@TestPropertySource("/application-test.properties")
public class DatabaseDownloadProgressEndpointTest {

	RestTemplate rest = new RestTemplate();

	@LocalServerPort
	private int port;

	@MockBean
	private UsersDAO users;

	@MockBean
	private DatabaseDAO dbDAO;

	@Autowired
	private PasswordEncoder encoder;

	private String websocketURI;

	private WebSocketStompClient client;

	private String dbName = "db1";

	private boolean initialized = false;

	private String authentication;

	@Before
	public void setup() throws Exception {
		NetUtils.checkInternetConnection(DatabaseDownloadTaskTest.SMALL_DOWNLOAD);
		if (!initialized) {
			JSONObject form = new JSONObject();
			final User user = Mockito.mock(User.class);
			when(user.getEmail()).thenReturn("admin@admin.com");
			when(user.getLogin()).thenReturn("admin");
			when(user.getHash()).thenReturn(encoder.encode("admin"));
			when(user.getAuthorities()).thenCallRealMethod();
			when(user.getRoles()).thenCallRealMethod();
			when(user.isActivated()).thenReturn(true);
			when(users.findByLogin("admin")).thenReturn(user);
			form.put("j_username", "admin").put("j_password", "admin");
			when(dbDAO.exists(anyInt())).thenReturn(false);
			when(dbDAO.findByName(anyString())).thenReturn(null);
			ResponseEntity<String> postForEntity = rest.postForEntity("http://localhost:{port}/api/login", form.toString(0), String.class, port);
			HttpHeaders headers = postForEntity.getHeaders();
			List<String> list = headers.get("Authorization");
			this.authentication = list.get(0);
			websocketURI = format("ws://localhost:{0,number,#}/api/ws", port);
			client = new WebSocketStompClient(new SockJsClient(asList(new WebSocketTransport(new StandardWebSocketClient()))));
			initialized = true;
		}
	}

	@Test
	public void testAuthorized() throws Exception {
		final WebSocketHttpHeaders headers = new WebSocketHttpHeaders();
		headers.add(JWTHelper.HEADER, authentication);
		StompSession session = client.connect(websocketURI, headers, new StompSessionHandlerAdapter() {
		}).get(5, TimeUnit.SECONDS);
		try {
			session.subscribe("/topic/db_upload/" + dbName, new StompSessionHandlerAdapter() {
			});
		} finally {
			session.disconnect();
		}
	}

	@Test(expected = TimeoutException.class)
	public void testUnauthorized() throws Throwable {
		client.connect(websocketURI, new WebSocketHttpHeaders(), new StompSessionHandlerAdapter() {
		}).get(5, TimeUnit.SECONDS);
	}

	@Test
	public void testDownloadInProgress() throws Exception {
		final JSONObject db = requestDownload(dbName, DatabaseDownloadTaskTest.SMALL_DOWNLOAD);
		final WebSocketHttpHeaders websocketHeaders = new WebSocketHttpHeaders();
		websocketHeaders.set(JWTHelper.HEADER, authentication);
		StompSession session = client.connect(websocketURI, websocketHeaders, new StompSessionHandlerAdapter() {
		}).get(5, TimeUnit.SECONDS);
		try {
			CountDownLatch latch = new CountDownLatch(5);
			List<Integer> payloads = new ArrayList<>((int) latch.getCount());
			session.subscribe("/topic/db_upload/" + db.getString("name"), new StompSessionHandlerAdapter() {

				@Override
				public void handleFrame(StompHeaders headers, Object payload) {
					latch.countDown();
					payloads.add(new JSONObject(new String((byte[]) payload)).getInt("size"));
				}

				@Override
				public Type getPayloadType(StompHeaders headers) {
					return byte[].class;
				}
			});
			latch.await(6, TimeUnit.SECONDS);
			for (int i = 0; i < payloads.size() - 1; i++) {
				assertTrue(payloads.get(i) < payloads.get(i + 1));
			}
		} finally {
			session.disconnect();
		}
	}

	public JSONObject requestDownload(final String name, final String url) {
		final JSONObject db = new JSONObject().put("url", url).put("name", name);
		final HttpHeaders headers = new HttpHeaders();
		final String content = db.toString(0);
		headers.add(JWTHelper.HEADER, authentication);
		headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
		headers.setContentLength(content.length());
		headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_UTF8_VALUE);
		HttpEntity<String> req = new HttpEntity<String>(content, headers);
		rest.postForEntity(format("http://localhost:{0,number,#}/api/secured/dbs", port), req, JSONObject.class);
		return db;
	}

	@Test
	public void testDownloadCancel() throws Exception {
		final JSONObject db = requestDownload(dbName, DatabaseDownloadTaskTest.MEDIUM_DOWNLOAD);
		final WebSocketHttpHeaders websocketHeaders = new WebSocketHttpHeaders();
		websocketHeaders.set(JWTHelper.HEADER, authentication);
		StompSession session = client.connect(websocketURI, websocketHeaders, new StompSessionHandlerAdapter() {
		}).get(5, TimeUnit.SECONDS);
		try {
			CountDownLatch latch = new CountDownLatch(2);
			List<Integer> payloads = new ArrayList<>(0);
			CompletableFuture<JSONObject> rsp = new CompletableFuture<>();
			session.subscribe("/topic/db_upload/" + db.getString("name"), new StompSessionHandlerAdapter() {

				@Override
				public void handleFrame(StompHeaders headers, Object payload) {
					latch.countDown();
					final JSONObject response = new JSONObject(new String((byte[]) payload));
					if (response.has("message") && response.has("success"))
						rsp.complete(response);
					else if (response.has("size"))
						payloads.add(response.getInt("size"));
				}

				@Override
				public Type getPayloadType(StompHeaders headers) {
					return byte[].class;
				}
			});
			latch.await(5, TimeUnit.SECONDS);
			session.send("/app/db_upload/" + db.getString("name"), new JSONObject().put("command", "cancel").toString(0).getBytes());
			assertFalse(rsp.isCompletedExceptionally());
			JSONObject jsonObject = rsp.get(1, TimeUnit.MINUTES);
			assertTrue(jsonObject.getBoolean("success"));
			assertNotNull(jsonObject.getString("message"));
		} finally {
			session.disconnect();
		}
	}

}
