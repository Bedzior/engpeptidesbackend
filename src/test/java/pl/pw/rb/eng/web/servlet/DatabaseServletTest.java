package pl.pw.rb.eng.web.servlet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import pl.pw.rb.eng.db.entities.Database;
import pl.pw.rb.eng.service.DatabaseService;
import pl.pw.rb.eng.tests.ServletTest;
import pl.pw.rb.eng.tests.UnitTest;
import pl.pw.rb.eng.web.servlet.exception.db.MissingDatabaseException;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Category({ UnitTest.class, ServletTest.class })
public class DatabaseServletTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private DatabaseService databaseService;

	@Mock
	private Database mock;

	@Test
	@WithMockUser(roles = "ADMIN")
	public void testGetList() throws Exception {
		when(databaseService.getDatabases(anyBoolean())).thenReturn(Arrays.asList(mock(Database.class), mock(Database.class)));
		MvcResult result = mockMvc.perform(get("/secured/dbs")).andExpect(status().isOk()).andReturn();
		assertEquals(2, new JSONArray(result.getResponse().getContentAsString()).length());
	}

	@Test
	@WithMockUser(roles = "ADMIN")
	public void testGetSingle() throws Exception {
		final Database db = mock(Database.class);
		final int id = new Random().nextInt();
		when(db.getId()).thenReturn(id);
		when(db.getName()).thenReturn("someName");
		when(db.getRegexId()).thenReturn("regexId");
		when(db.getRegexName()).thenReturn("regexName");
		when(databaseService.getDatabase(eq(id))).thenReturn(db);
		MvcResult result = mockMvc.perform(get("/secured/dbs/{id}", id).accept(APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
		JSONObject json = new JSONObject(result.getResponse().getContentAsString());
		assertEquals(db.getRegexId(), json.get("regexId"));
		assertEquals(db.getRegexName(), json.get("regexName"));
		assertEquals(db.getName(), json.get("name"));
	}

	@Test
	@WithMockUser(roles = "ADMIN")
	public void testUpload() throws Exception {
		JSONObject json = new JSONObject().put("name", RandomStringUtils.randomAlphabetic(20)).put("regexId", RandomStringUtils.randomAlphabetic(20))
				.put("regexName", RandomStringUtils.randomAlphabetic(20)).put("token", RandomStringUtils.randomAlphabetic(20));
		when(mock.getName()).thenReturn(json.getString("name"));
		when(mock.getRegexId()).thenReturn(json.getString("regexId"));
		when(mock.getRegexName()).thenReturn(json.getString("regexName"));

		when(databaseService.create(anyString(), anyObject(), anyString())).thenReturn(mock);

		MvcResult result = mockMvc.perform(post("/secured/dbs").contentType(APPLICATION_JSON).content(json.toString(0))).andExpect(status().isCreated())
				.andReturn();
		final JSONObject jsonResponse = new JSONObject(result.getResponse().getContentAsString());
		assertEquals(jsonResponse.get("name"), json.get("name"));
		assertEquals(jsonResponse.get("regexId"), json.get("regexId"));
		assertEquals(jsonResponse.get("regexName"), json.get("regexName"));
		assertFalse(jsonResponse.has("path"));
		assertEquals(jsonResponse.get("url"), null);
		assertNotNull(result.getResponse().getHeader("Location"));
	}

	@Test
	@WithMockUser(roles = "ADMIN", username = "user")
	public void testUploadFromRemoteURL() throws Exception {
		JSONObject json = new JSONObject();
		json.put("name", "name");
		json.put("regexId", "id");
		json.put("regexName", "name");
		json.put("description", "description");
		json.put("url", "url://something.here.com");
		MvcResult result = mockMvc.perform(post("/secured/dbs").contentType(APPLICATION_JSON).content(json.toString(0))).andExpect(status().isAccepted())
				.andReturn();
		JSONObject jsonRsp = new JSONObject(result.getResponse().getContentAsString());
		assertTrue(jsonRsp.similar(json));
	}

	@Test
	@WithMockUser(roles = "ADMIN", username = "user")
	public void testUploadWithMissingParam() throws Exception {
		JSONObject json = new JSONObject();
		json.put("name", "name");
		json.put("regexId", "id");
		json.put("regexName", "name");
		mockMvc.perform(post("/secured/dbs").contentType(APPLICATION_JSON).content(json.toString(0))).andExpect(status().isBadRequest());
		// .andExpect(content().string(contains("No database source
		// specified")));

	}

	@Test
	@WithMockUser(roles = "ADMIN")
	public void testUpdate() throws Exception {
		when(databaseService.update(anyInt(), anyObject())).thenReturn(mock(Database.class));
		JSONObject request = new JSONObject().put("regexId", "id").put("regexName", "name");
		mockMvc.perform(put("/secured/dbs/124").contentType(APPLICATION_JSON).content(request.toString(0))).andExpect(status().isOk());

	}

	@Test
	@WithMockUser(roles = "ADMIN")
	public void testDeleteNoSuchElement() throws Exception {
		doThrow(MissingDatabaseException.class).when(databaseService).delete(eq(124));
		mockMvc.perform(delete("/secured/dbs/124")).andExpect(status().isNotFound());
	}

	@Test
	@WithMockUser(roles = "ADMIN")
	public void testDelete() throws Exception {
		when(databaseService.getDatabase(anyInt())).thenReturn(mock);
		mockMvc.perform(delete("/secured/dbs/124")).andExpect(status().isNoContent());
	}

	@Test
	public void testAnonymous() throws Exception {
		mockMvc.perform(get("/secured/dbs").accept(APPLICATION_JSON)).andExpect(status().is4xxClientError());
	}
}
