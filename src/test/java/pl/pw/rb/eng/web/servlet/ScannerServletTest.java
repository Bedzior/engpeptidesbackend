package pl.pw.rb.eng.web.servlet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.pw.rb.eng.springconfig.PeptidesWebMvcConfiguration.DATE_STRING_FORMAT;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.StreamUtils;
import org.springframework.validation.BindException;

import pl.pw.rb.eng.db.entities.Database;
import pl.pw.rb.eng.db.entities.Scan;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.service.DatabaseService;
import pl.pw.rb.eng.service.ScannerService;
import pl.pw.rb.eng.service.UploadFileService;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.tests.ServletTest;
import pl.pw.rb.eng.tests.UnitTest;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Category({ UnitTest.class, ServletTest.class })
public class ScannerServletTest {

	private static final String USER_ROLE = "USER";

	private static final String USERNAME = "user";

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UsersService usersService;

	@MockBean
	private ScannerService service;

	@MockBean
	private DatabaseService databaseService;

	@MockBean
	private UploadFileService uploadFileService;

	@Mock
	private Scan scan;

	@Mock
	private Database database;

	@Rule
	public TemporaryFolder temp = new TemporaryFolder();

	@Mock
	private User user;

	@Before
	public void before() throws Exception {
		when(service.createFor(eq(user), anyString(), anyObject())).thenReturn(scan);
	}

	private String requestFromResource() throws IOException {
		final String configuration;
		try (InputStream is = getClass().getResourceAsStream("/config.json");) {
			configuration = StreamUtils.copyToString(is, StandardCharsets.UTF_8);
		}
		return configuration;
	}

	@Test
	@WithMockUser(roles = USER_ROLE, username = USERNAME)
	public void testStart() throws Exception {
		final String configuration = requestFromResource();
		when(user.getLogin()).thenReturn(USERNAME);
		when(user.getId()).thenReturn(new Random().nextInt());
		when(scan.getId()).thenReturn(UUID.randomUUID().toString());
		when(usersService.getByID(eq(user.getId()))).thenReturn(user);
		when(usersService.getByLogin(eq(USERNAME))).thenReturn(user);
		when(uploadFileService.getFile(eq(user), anyString())).thenReturn(temp.newFile());
		when(databaseService.getDatabase(anyInt())).thenReturn(database);
		when(databaseService.getDatabase(anyString())).thenReturn(database);
		MvcResult result = this.mockMvc.perform(post("/secured/scan").content(configuration).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isAccepted()).andReturn();
		JSONObject jsonObject = new JSONObject(result.getResponse().getContentAsString());
		assertTrue(jsonObject.has("token"));
	}

	@Test
	@WithMockUser(roles = USER_ROLE, username = USERNAME)
	public void test4xxErrors() throws Exception {
		try {
			when(user.getLogin()).thenReturn(USERNAME);
			when(user.getId()).thenReturn(new Random().nextInt());
			when(scan.getId()).thenReturn(UUID.randomUUID().toString());
			when(usersService.getByID(eq(user.getId()))).thenReturn(user);
			when(usersService.getByLogin(eq(USERNAME))).thenReturn(user);
			when(uploadFileService.getFile(eq(user), anyString())).thenReturn(temp.newFile());
			when(databaseService.getDatabase(anyInt())).thenReturn(database);
			when(databaseService.getDatabase(anyString())).thenReturn(database);
			// trivial scenarios
			expect4xxOn("");
			expect4xxOn("{}");
			expect4xxOn("{bio:{}}");
			// modified example
			String requestString = requestFromResource();
			expectAcceptedOn(new JSONObject(requestString));
			{
				JSONObject req = new JSONObject(requestString);
				req.getJSONObject("bio").put("peptideCharge", "");
				expectAcceptedOn(req);
			}
			// 400 BAD REQUEST
			{
				JSONObject req = new JSONObject(requestString);
				req.getJSONObject("bio").put("peptideCharge", "1, s, d, 29");
				expect4xxOn(req);
			}
			{
				JSONObject req = new JSONObject(requestString);
				req.getJSONObject("bio").put("peakDetection", -1);
				expect4xxOn(req);
			}
			{
				JSONObject req = new JSONObject(requestString);
				req.getJSONObject("bio").put("peakDetection", 3);
				expect4xxOn(req);
			}
			{
				JSONObject req = new JSONObject(requestString);
				req.getJSONObject("bio").put("peptideTolerance", -1.0F);
				expect4xxOn(req);
			}
			{
				JSONObject req = new JSONObject(requestString);
				req.getJSONObject("bio").put("tandemMassTolerance", -30F);
				expect4xxOn(req);
			}
			{
				JSONObject req = new JSONObject(requestString);
				req.getJSONObject("bio").put("peptideToleranceUnits", (String) null);
				expect4xxOn(req);
			}
			{
				JSONObject req = new JSONObject(requestString);
				req.getJSONObject("bio").put("tandemMassToleranceUnits", (String) null);
				expect4xxOn(req);
			}
			{
				JSONObject req = new JSONObject(requestString);
				req.getJSONObject("bio").put("enzyme", (String) null);
				expect4xxOn(req);
			}
		} catch (BindException e) {
			
		}
	}

	private void expectAcceptedOn(final JSONObject request) throws Exception {
		this.expectAcceptedOn(request.toString(0));
	}

	private void expectAcceptedOn(final String request) throws Exception {
		this.mockMvc.perform(post("/secured/scan").content(request).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isAccepted());
	}

	private void expect4xxOn(final JSONObject request) throws Exception {
		this.expect4xxOn(request.toString(0));
	}

	private void expect4xxOn(final String request) throws Exception {
		this.mockMvc.perform(post("/secured/scan").content(request).contentType(MediaType.APPLICATION_JSON)).andExpect(status().is4xxClientError());
	}

	@Test
	@WithMockUser(roles = { USER_ROLE }, username = USERNAME)
	public void testList() throws Exception {
		List<Scan> mockedList = new ArrayList<>();
		mockedList.add(mock(Scan.class));
		mockedList.add(mock(Scan.class));
		mockedList.add(mock(Scan.class));
		when(service.getForUser((Principal) anyObject())).thenReturn(mockedList);
		MvcResult result = this.mockMvc.perform(get("/secured/scan").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
		JSONArray response = new JSONArray(result.getResponse().getContentAsString());
		assertEquals(mockedList.size(), response.length());
	}

	@Test
	@WithMockUser(roles = { USER_ROLE }, username = USERNAME)
	public void testGet() throws Exception {
		String token = UUID.randomUUID().toString();
		when(service.get(eq(token))).thenReturn(scan);
		when(scan.getId()).thenReturn(token);
		Date created = new Date();
		Date finished = new Date();
		Date started = new Date();
		when(scan.getCreated()).thenReturn(created);
		when(scan.getFinished()).thenReturn(finished);
		when(scan.getStarted()).thenReturn(started);
		MvcResult result = this.mockMvc.perform(get("/secured/scan/{token}", token)).andExpect(status().isOk()).andReturn();
		JSONObject response = new JSONObject(result.getResponse().getContentAsString());
		assertEquals(token, response.get("token"));
		assertEquals(DATE_STRING_FORMAT.format(created), response.get("created"));
		assertEquals(DATE_STRING_FORMAT.format(started), response.get("started"));
		assertEquals(DATE_STRING_FORMAT.format(finished), response.get("finished"));
	}

	public void testWithoutCredentials() throws Exception {
		this.mockMvc.perform(get("/secured/scan")).andExpect(status().is4xxClientError());
		this.mockMvc.perform(post("/secured/scan")).andExpect(status().is4xxClientError());
		this.mockMvc.perform(get("/secured/scan/{token}", "abc")).andExpect(status().is4xxClientError());
	}
}
