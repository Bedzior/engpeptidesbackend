package pl.pw.rb.eng.web.servlet.admin;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import pl.pw.rb.eng.db.entities.EmailTemplate;
import pl.pw.rb.eng.service.TemplateService;
import pl.pw.rb.eng.tests.ServletTest;
import pl.pw.rb.eng.tests.UnitTest;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Category({ UnitTest.class, ServletTest.class })
public class EmailTemplateServletTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private TemplateService service;

	@Mock
	private EmailTemplate mock;

	@Autowired
	private ObjectMapper mapper;

	@Test
	@WithMockUser(username = "user", roles = "ADMIN")
	public void testGetByName() throws Exception {
		final String name = RandomStringUtils.randomAlphanumeric(20);
		final String type = RandomStringUtils.randomAlphabetic(4);
		when(mock.getName()).thenReturn(name);
		when(mock.getType()).thenReturn(type);
		when(mock.getBody()).thenReturn(RandomStringUtils.randomAlphanumeric(new Random().nextInt(2 << 15)));
		when(service.getTemplate(eq(name), eq(type))).thenReturn(mock);
		mockMvc.perform(get("/secured/admin/templates/{name}/{type}", name, type).accept(MediaType.APPLICATION_JSON_UTF8, MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk()).andExpect(content().json(mapper.writeValueAsString(mock)));
	}

	@Test
	@WithMockUser(username = "user", roles = "ADMIN")
	public void testGetByID() throws Exception {
		final String name = RandomStringUtils.randomAlphanumeric(50);
		final String type = RandomStringUtils.randomAlphanumeric(4);
		final int id = new Random().nextInt(Integer.MAX_VALUE);
		when(mock.getId()).thenReturn(id);
		when(mock.getName()).thenReturn(name);
		when(mock.getType()).thenReturn(type);
		when(mock.getBody()).thenReturn(RandomStringUtils.randomAlphanumeric(new Random().nextInt(2 << 15)));
		when(service.getTemplate(eq(id))).thenReturn(mock);
		mockMvc.perform(get("/secured/admin/templates/{id}", id).accept(MediaType.APPLICATION_JSON_UTF8, MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk()).andExpect(content().json(mapper.writeValueAsString(mock)));
	}

	@Test
	@WithMockUser(username = "user", roles = "ADMIN")
	public void getNonexistentByID() throws Exception {
		MvcResult result = mockMvc
				.perform(get("/secured/admin/templates/{id}", new Random().nextInt(Integer.MAX_VALUE))
						.accept(MediaType.APPLICATION_JSON_UTF8, MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isNotFound()).andReturn();
		JSONObject json = new JSONObject(result.getResponse().getContentAsString());
		assertFalse(json.getBoolean("success"));
		assertNotNull(json.getString("message"));
	}

	@Test
	@WithMockUser(username = "user", roles = "ADMIN")
	public void getNonexistentByName() throws Exception {
		MvcResult result = mockMvc
				.perform(get("/secured/admin/templates/{name}/{type}", RandomStringUtils.randomAlphabetic(20), RandomStringUtils.randomAlphabetic(4))
						.accept(MediaType.APPLICATION_JSON_UTF8, MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(status().isNotFound()).andReturn();
		JSONObject json = new JSONObject(result.getResponse().getContentAsString());
		assertFalse(json.getBoolean("success"));
		assertNotNull(json.getString("message"));
		assertNotNull(json.getString("token"));
	}

	@Test
	@WithMockUser(username = "user", roles = "ADMIN")
	public void testUpdate() throws Exception {
		when(service.update(anyObject())).then(AdditionalAnswers.returnsFirstArg());
		final int id = new Random().nextInt(Integer.MAX_VALUE);
		when(service.getTemplate(eq(id))).thenReturn(new EmailTemplate());
		mockMvc.perform(put("/secured/admin/templates/{id}", id).content(new JSONObject().put("body", "someBody").toString(0))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
				.andExpect(content().json(new JSONObject().put("body", "someBody").toString(0)));
	}

	@Test
	@WithMockUser(username = "user", roles = "USER")
	public void testUnauthorizedUser() throws Exception {
		mockMvc.perform(get("/secured/admin/templates")).andExpect(status().isForbidden());
	}

	@Test
	public void testAnonymousUser() throws Exception {
		mockMvc.perform(get("/secured/admin/templates")).andExpect(status().isUnauthorized());
	}
}
