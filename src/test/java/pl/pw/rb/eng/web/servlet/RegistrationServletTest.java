package pl.pw.rb.eng.web.servlet;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.service.exception.RegistrationEmailNotSentException;
import pl.pw.rb.eng.tests.ServletTest;
import pl.pw.rb.eng.tests.UnitTest;
import pl.pw.rb.eng.web.servlet.exception.user.NoUserException;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Category({ UnitTest.class, ServletTest.class })
public class RegistrationServletTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UsersService usersService;

	private JSONObject form;

	@Before
	public void before() {
		form = new JSONObject();
		form.put("login", "somebody");
		form.put("password", "Abcdefgh123");
		form.put("email", "abc@test.com");
	}
	
	@Test
	public void testRegistration() throws Exception {
		when(usersService.createActivatedUser(anyString(), anyString(), anyString())).thenReturn(mock(User.class));
		this.mockMvc.perform(post("/register").contentType(APPLICATION_JSON).accept(APPLICATION_JSON).content(form.toString()))
				.andExpect(status().isAccepted());
	}

	@Test
	public void testInsufficientParameters_loginError() throws Exception {
		form.remove("login");
		
		MvcResult result = this.mockMvc
				.perform(post("/register").contentType(APPLICATION_JSON).accept(APPLICATION_JSON).content(form.toString()))
				.andExpect(status().isBadRequest()).andReturn();
		final String response = result.getResponse().getContentAsString();
		assertTrue("The result should contain a reference to missing parameter: login", response.contains("login"));
		assertFalse("The result should contain no reference to present parameters", response.contains("password") || response.contains("email"));
	}

	@Test
	public void testInsufficientParameters_passwordError() throws Exception {
		form.remove("password");

		MvcResult result = this.mockMvc
				.perform(post("/register").contentType(APPLICATION_JSON).accept(APPLICATION_JSON).content(form.toString()))
				.andExpect(status().isBadRequest()).andReturn();
		final String response = result.getResponse().getContentAsString();
		assertTrue("The result should contain a reference to missing parameter: password", response.contains("password"));
		assertFalse("The result should contain no reference to present parameters", response.contains("login") || response.contains("email"));
	}

	@Test
	public void testInsufficientParameters_emailError() throws Exception {
		form.remove("email");

		MvcResult result = this.mockMvc
				.perform(post("/register").contentType(APPLICATION_JSON).accept(APPLICATION_JSON).content(form.toString()))
				.andExpect(status().isBadRequest()).andReturn();
		final String response = result.getResponse().getContentAsString();
		assertTrue("The result should contain a reference to missing parameter: email", response.contains("email"));
		assertFalse("The result should contain no reference to present parameters", response.contains("login") || response.contains("password"));
	}
	
	@Test
	public void testConfirmation() throws Exception {
		String uuid = UUID.randomUUID().toString();
		doNothing().when(usersService).activateUser(eq(uuid));
		doThrow(new NoUserException(uuid)).when(usersService).activateUser(not(eq(uuid)));
		this.mockMvc.perform(post("/register/confirm").param("uuid", uuid)).andExpect(status().isOk());
	}
	
	@Test
	public void testFailedMail() throws Exception {
		doThrow(RegistrationEmailNotSentException.class).when(usersService).createUser(anyObject());
		
		this.mockMvc.perform(post("/register").content(form.toString(0)).contentType(APPLICATION_JSON)).andExpect(status().is2xxSuccessful());
	}
}
