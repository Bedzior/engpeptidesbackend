package pl.pw.rb.eng.web.servlet;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.util.Collections;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import pl.pw.rb.eng.service.DatabaseService;
import pl.pw.rb.eng.service.ScannerService;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.tests.ServletTest;
import pl.pw.rb.eng.tests.UnitTest;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Category({ UnitTest.class, ServletTest.class })
public class ScanConfigurationServletTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private DatabaseService databaseService;

	@MockBean
	private UsersService usersService;

	@MockBean
	private ScannerService scannerService;

	@Before
	public void prepare() {
		when(databaseService.getDatabases(false)).thenReturn(Collections.emptyList());
	}

	@Test
	@WithMockUser(roles = "USER")
	public void testGetAdvanced() throws Exception {
		MvcResult result = this.mockMvc.perform(get("/secured/config/advanced")).andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)).andReturn();
		JSONObject output = new JSONObject(result.getResponse().getContentAsString());
		testConfiguration(output);
	}

	@Test
	@WithMockUser(roles = "USER")
	public void testGetSimple() throws Exception {
		MvcResult result = this.mockMvc.perform(get("/secured/config/simple")).andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON)).andReturn();
		JSONObject output = new JSONObject(result.getResponse().getContentAsString());
		testConfiguration(output);
	}

	@Test
	@WithMockUser(roles = "USER")
	public void testGetWrongConfig() throws Exception {
		this.mockMvc.perform(get("/secured/config/abc")).andExpect(status().isBadRequest()).andReturn();
	}

	@Test
	public void testUnauthorizedUser() throws Exception {
		this.mockMvc.perform(get("/secured/config/simple")).andExpect(status().is4xxClientError());
		this.mockMvc.perform(get("/secured/config/advanced")).andExpect(status().is4xxClientError());
	}

	private void testConfiguration(JSONObject output) throws IOException {
		assertNotNull(output);
		assertTrue("Enzymes expected in output", output.has("enzymes"));
		assertTrue("Post-translational modifications expected in output", output.has("ptms"));
		assertTrue("Units expected in output", output.has("mmdUnits"));
		assertTrue("Instruments expected in output", output.has("instruments"));
		// assertTrue("Formats expected in output", output.has("formats"));
		assertTrue("Databases expected in output", output.has("databases"));
	}

}
