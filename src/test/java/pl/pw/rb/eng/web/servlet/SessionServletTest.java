package pl.pw.rb.eng.web.servlet;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.mockito.Mockito.when;

import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.tests.ServletTest;
import pl.pw.rb.eng.tests.UnitTest;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Category({ UnitTest.class, ServletTest.class })
public class SessionServletTest {

	private static final String USER = "user";

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	UsersService usersService;

	@Mock
	User mockUser;

	@Test
	@WithMockUser(username = USER, roles = { "USER", "ADMIN" })
	public void testLoggedIn() throws Exception {
		when(mockUser.getEmail()).thenReturn("mockEmail@mail.com");
		when(mockUser.getLogin()).thenReturn(USER);
		when(usersService.getByLogin(USER)).thenReturn(mockUser);
		MvcResult result = mockMvc.perform(get("/session")).andExpect(status().isOk()).andReturn();
		JSONObject json = new JSONObject(result.getResponse().getContentAsString());
		assertEquals(USER, json.get("login"));
	}

	@Test
	public void testAnonymous() throws Exception {
		mockMvc.perform(get("/session").accept(MediaType.APPLICATION_JSON)).andExpect(status().isUnauthorized());
	}

}
