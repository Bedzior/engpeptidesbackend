package pl.pw.rb.eng.web.servlet.admin;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.tests.ServletTest;
import pl.pw.rb.eng.tests.UnitTest;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Category({ UnitTest.class, ServletTest.class })
@ActiveProfiles("test")
public class UsersServletTest {

	private static final String ENDPOINT = "/secured/admin/users";

	@Autowired
	private MockMvc mvc;

	@MockBean
	private UsersService usersService;

	@Test
	@WithMockUser(username = "admin", roles = { "ADMIN", "USER" })
	public void testList() throws Exception {
		final ArrayList<User> list = new ArrayList<User>();
		for (int i = new Random().nextInt(10); i > 0; i--) {
			list.add(mock(User.class));
		}
		Page<User> page = new PageImpl<User>(list);
		when(usersService.listAll()).thenReturn(page);
		MvcResult result = mvc.perform(get(ENDPOINT)).andExpect(status().isOk()).andReturn();
		JSONArray response = new JSONArray(result.getResponse().getContentAsString());
		assertEquals(page.getNumberOfElements(), response.length());
	}

	@Test
	@WithMockUser(username = "user", roles = "USER")
	public void testUnauthorizedUser() throws Exception {
		mvc.perform(get(ENDPOINT)).andExpect(status().isForbidden());
	}

	@Test
	@WithMockUser(username = "admin", roles = { "ADMIN", "USER" })
	public void testUpdate() throws Exception {
		User user = mock(User.class);
		int id = new Random().nextInt(Integer.MAX_VALUE);
		when(user.getId()).thenReturn(id);
		when(usersService.getByID(eq(user.getId()))).thenReturn(user);
		when(usersService.update(anyObject())).then(returnsFirstArg());
		JSONObject form = new JSONObject();
		form.put("id", id);
		form.put("admin", true);
		form.put("email", "abc@def.com");
		
		when(user.getId()).thenReturn(form.getInt("id"));
		when(user.isAdmin()).thenReturn(form.getBoolean("admin"));
		when(user.getEmail()).thenReturn(form.getString("email"));
		MvcResult result = mvc.perform(put(ENDPOINT + "/{id}", id) //
				.content(form.toString(0)).contentType(MediaType.APPLICATION_JSON)) //
			.andExpect(status().isOk()).andReturn();
		JSONObject response = new JSONObject(result.getResponse().getContentAsString());
		assertEquals(response.get("email"), user.getEmail());

	}

	@Test
	@Ignore
	public void testDelete() {
		fail("Not yet implemented");
	}

}
