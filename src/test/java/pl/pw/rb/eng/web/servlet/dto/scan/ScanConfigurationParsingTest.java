package pl.pw.rb.eng.web.servlet.dto.scan;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StreamUtils;

@RunWith(SpringRunner.class)
@JsonTest
@AutoConfigureJsonTesters
@ActiveProfiles("test")
public class ScanConfigurationParsingTest {

	@Autowired
	private JacksonTester<ScanConfiguration> json;

	private String getConfiguration() throws IOException {
		final String configuration;
		try (InputStream is = getClass().getResourceAsStream("/test_data/real-life-example.json");) {
			configuration = StreamUtils.copyToString(is, StandardCharsets.UTF_8);
		}
		return configuration;
	}

	@Test
	public void test() throws Exception {
		final String configuration = getConfiguration();
		this.json.parse(configuration).assertThat().isNotNull();
	}

}
