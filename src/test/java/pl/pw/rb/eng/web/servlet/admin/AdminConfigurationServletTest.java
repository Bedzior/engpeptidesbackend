package pl.pw.rb.eng.web.servlet.admin;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import pl.pw.rb.eng.service.AdminConfigurationService;
import pl.pw.rb.eng.tests.ServletTest;
import pl.pw.rb.eng.tests.UnitTest;
import pl.pw.rb.eng.web.dto.Configuration;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = WebEnvironment.MOCK)
@Category({ UnitTest.class, ServletTest.class })
public class AdminConfigurationServletTest {

	private static final String USER = "user";

	private static final String USER_ROLE = "USER";

	private static final String ADMIN = "admin";

	private static final String ADMIN_ROLE = "ADMIN";

	private final String endpoint = "/secured/configuration";

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private AdminConfigurationService service;

	@Before
	public void before() throws Exception {
		when(service.update(anyString(), any())).thenAnswer(i -> new Configuration(i.getArgumentAt(0, String.class), "", i.getArgumentAt(1, Object.class)));
		Configuration conf = new Configuration("abc", "def", Integer.valueOf(1));
		when(service.get(anyString())).thenReturn(conf);
		List<Configuration> list = Arrays.asList(conf);
		when(service.list()).thenReturn(list);
	}

	@Test
	@WithMockUser(roles = { ADMIN_ROLE, USER_ROLE }, username = ADMIN)
	public void testListAsAdmin() throws Exception {
		this.testList();
	}

	@Test
	@WithMockUser(roles = USER_ROLE, username = USER)
	public void testListAsUser() throws Exception {
		this.testList();
	}

	@Test
	@WithMockUser(roles = { ADMIN_ROLE, USER_ROLE }, username = ADMIN)
	public void testGetAsAdmin() throws Exception {
		this.testGet();
	}

	@Test
	@WithMockUser(roles = USER_ROLE, username = USER)
	public void testGetAsUser() throws Exception {
		this.testGet();
	}

	@Test
	public void testGetUnauthorized() throws Exception {
		mockMvc.perform(get(endpoint).accept(MediaType.APPLICATION_JSON)).andExpect(status().isUnauthorized());
	}

	private void testList() throws Exception {
		MvcResult result = mockMvc.perform(get(endpoint).accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
		JSONArray response = new JSONArray(result.getResponse().getContentAsString());
		assertNotEquals(0, response.length());
		for (int i = 0; i < response.length(); i++) {
			JSONObject obj = response.getJSONObject(i);
			final JSONObject conf = (JSONObject) obj;
			verifyConfigurationIsComplete(conf);
		}
	}

	public void testGet() throws Exception {
		MvcResult result = mockMvc.perform(get(endpoint + "/abc").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();
		JSONObject response = new JSONObject(result.getResponse().getContentAsString());
		verifyConfigurationIsComplete(response);
	}

	@Test
	@WithMockUser(username = USER, roles = USER_ROLE)
	public void testUpdateUnauthorized() throws Exception {
		mockMvc.perform(put(endpoint)).andExpect(status().is4xxClientError());
	}

	@Test
	@WithMockUser(username = ADMIN, roles = { ADMIN_ROLE, USER_ROLE })
	public void testUpdate() throws Exception {
		mockMvc.perform(put(endpoint + "/{name}", "abc").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON).content("1234"))
				.andExpect(status().isOk());
	}

	@Test
	@WithMockUser(username = ADMIN, roles = { ADMIN_ROLE, USER_ROLE })
	public void testUpdateSeveral() throws Exception {
		JSONArray request = new JSONArray();
		request.put(new JSONObject().put("name", "abc").put("value", 1223));
		request.put(new JSONObject().put("name", "def").put("value", 1e223));
		mockMvc.perform(put("{endpoint}", endpoint).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).content(request.toString()))
				.andExpect(status().isOk());
	}

	private void verifyConfigurationIsComplete(final JSONObject conf) {
		assertTrue(isNotBlank(conf.getString("name")));
		assertTrue(isNotBlank(conf.getString("description")));
		assertTrue(isNotBlank(conf.getString("type")));
		assertNotNull(conf.get("value"));
	}

}
