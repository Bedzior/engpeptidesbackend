package pl.pw.rb.eng.web.servlet;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Random;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import pl.pw.rb.eng.db.entities.UploadFile;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.service.UploadFileService;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.tests.ServletTest;
import pl.pw.rb.eng.tests.UnitTest;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Category({ UnitTest.class, ServletTest.class })
public class UploadFileServletTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	UploadFileService uploadFileService;

	@MockBean
	UsersService usersService;

	@Mock
	UploadFile mock;

	@Test
	@WithMockUser(roles = "USER", username = "user")
	public void testUploadWithUser() throws Exception {
		final User mockUser = mock(User.class);
		when(mockUser.getId()).thenReturn(new Random().nextInt());
		when(usersService.getByLogin(eq("user"))).thenReturn(mockUser);
		when(usersService.getByID(eq(mockUser.getId()))).thenReturn(mockUser);
		when(mock.getToken()).thenReturn("aToken");
		when(uploadFileService.create(anyString(), anyObject())).thenReturn(mock);
		MockMultipartFile mockMultipart = new MockMultipartFile("file", getClass().getResourceAsStream("/config.json"));
		MvcResult result = mockMvc.perform(fileUpload("/secured/upload").file(mockMultipart)).andExpect(status().isOk()).andReturn();
		JSONObject response = new JSONObject(result.getResponse().getContentAsString());
		assertEquals(mock.getToken(), response.get("token"));
	}

	@Test
	public void testUploadAnonymous() throws Exception {
		MockMultipartFile mockMultipart = new MockMultipartFile("file", getClass().getResourceAsStream("/config.json"));
		mockMvc.perform(fileUpload("/secured/upload").file(mockMultipart)).andExpect(status().is4xxClientError());
	}

}
