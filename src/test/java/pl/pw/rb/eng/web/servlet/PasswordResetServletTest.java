package pl.pw.rb.eng.web.servlet;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;

import pl.pw.rb.eng.service.PasswordResetService;
import pl.pw.rb.eng.tests.ServletTest;
import pl.pw.rb.eng.tests.UnitTest;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Category({ UnitTest.class, ServletTest.class })
public class PasswordResetServletTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	PasswordResetService resetPasswordService;

	@Test
	public void requestResetWithUsername() throws Exception {
		JSONObject request = new JSONObject().put("username", "someUser");
		mockMvc.perform(post("/resetPassword/request").accept(APPLICATION_JSON).content(request.toString(0)).contentType(APPLICATION_JSON))
				.andExpect(status().isAccepted());
	}

	@Test
	public void requestResetWithEmail() throws Exception {
		JSONObject request = new JSONObject().put("email", "someEmail@host.com");
		mockMvc.perform(post("/resetPassword/request").accept(APPLICATION_JSON).content(request.toString(0)).contentType(APPLICATION_JSON))
				.andExpect(status().isAccepted());
	}

	@Test
	public void requestWithNothing() throws Exception {
		JSONObject request = new JSONObject().put("random", "This shouldn't be here");
		mockMvc.perform(post("/resetPassword/request").content(request.toString(0)).contentType(APPLICATION_JSON)).andExpect(status().isBadRequest());
	}

	@Test
	public void reset() throws Exception {
		String uuid = UUID.randomUUID().toString();
		when(resetPasswordService.verify(uuid)).thenReturn(true);
		mockMvc.perform(post("/resetPassword").contentType(APPLICATION_JSON).param("uuid", uuid)).andExpect(status().isOk());
	}
}
