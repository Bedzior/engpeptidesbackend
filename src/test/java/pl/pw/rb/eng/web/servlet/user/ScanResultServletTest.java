package pl.pw.rb.eng.web.servlet.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_OCTET_STREAM;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.pw.rb.eng.test.ScanTestUtils.readData;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import mscanlib.ms.msms.MsMsPeptideHit;
import mscanlib.ms.msms.MsMsProteinHit;
import mscanlib.ms.msms.dbengines.mscandb.io.MScanDbOutFileReader;
import pl.pw.rb.eng.db.entities.Scan;
import pl.pw.rb.eng.db.entities.ScanResult;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.service.ScanResultService;
import pl.pw.rb.eng.service.exception.NoSuchScanException;
import pl.pw.rb.eng.tests.ServletTest;
import pl.pw.rb.eng.tests.UnitTest;
import pl.pw.rb.eng.web.servlet.dto.scan.MsMsShortPeptideHit;
import pl.pw.rb.eng.web.servlet.dto.scan.MsMsShortProteinHit;
import pl.pw.rb.eng.web.servlet.dto.scan.MsOutputDTO;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = WebEnvironment.MOCK)
@Category({ UnitTest.class, ServletTest.class })
public class ScanResultServletTest {

	private static final String USER_ROLE = "USER";

	private static final String USER_NAME = "user";

	@Autowired
	public MockMvc mvc;

	@Autowired
	public ModelMapper mapper;

	@MockBean
	public ScanResultService service;

	@Mock
	private User mockUser;

	@Mock
	private Scan mockScan;

	@Mock
	private ScanResult mockScanResult;

	final byte[] resultContent = RandomUtils.nextBytes(21);

	final String token = RandomStringUtils.randomAlphabetic(20);

	@Before
	public void before() throws Exception {
		when(mockUser.getLogin()).thenReturn(USER_NAME);
		when(mockScan.getUser()).thenReturn(mockUser);
		when(mockScanResult.getScan()).thenReturn(mockScan);
		when(mockScanResult.getResult()).thenReturn(resultContent);
		when(mockScanResult.getName()).thenReturn("someEntryName.bin");
	}

	@Test
	@WithMockUser(username = USER_NAME, roles = { USER_ROLE })
	public void testGetArchive() throws Exception {
		when(service.getForScan(eq(token))).thenReturn(mockScanResult);
		MvcResult response = mvc.perform(get("/secured/scan/result/{token}?packed=true", token).contentType(APPLICATION_OCTET_STREAM))
				.andExpect(status().isOk()).andReturn();
		byte[] result = response.getResponse().getContentAsByteArray();
		assertNotNull(result);
		assertNotEquals(0, result.length);
		try (ArchiveInputStream ais = new TarArchiveInputStream(new GzipCompressorInputStream(new ByteArrayInputStream(result)))) {
			ArchiveEntry entry = ais.getNextEntry();
			assertEquals(resultContent.length, entry.getSize());
			byte[] buffer = new byte[(int) entry.getSize()];
			ais.read(buffer);
			assertTrue(Arrays.equals(buffer, resultContent));
		}
	}

	@Test
	@SuppressWarnings("unchecked")
	@WithMockUser(username = USER_NAME, roles = { USER_ROLE })
	public void testGetNonExistentArchive() throws Exception {
		when(service.getForScan(eq(token))).thenThrow(NoSuchScanException.class);
		mvc.perform(get("/secured/scan/result/{token}?packed=true", token).accept(APPLICATION_OCTET_STREAM)).andExpect(status().isNotFound());
	}

	@Test
	@WithMockUser(username = USER_NAME, roles = { USER_ROLE })
	public void testGetJSON() throws Exception {
		mapper.addMappings(new PropertyMap<MScanDbOutFileReader, MsOutputDTO>() {

			@Override
			protected void configure() {
				map().setFileFormatName(source.getFileFormatName());
			}
		});
		when(service.getResult(eq(token))).thenAnswer(invocation -> {
			MsOutputDTO mapped = mapper.map(readData(), MsOutputDTO.class);
			return mapped;
		});
		MvcResult result = mvc.perform(get("/secured/scan/result/{token}", token).accept(APPLICATION_JSON).contentType(APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		JSONObject response = new JSONObject(result.getResponse().getContentAsString());
		assertTrue(response.has("fileFormatName"));
//		assertTrue(response.has("header"));
		assertTrue(response.has("iTraqType"));
		assertTrue(response.has("mergedList"));
		assertTrue(response.has("phRange"));
		assertTrue(response.has("proteinsList"));
		assertTrue(response.has("queries"));
		assertTrue(response.has("warnings"));
		assertTrue(response.has("errors"));
	}

	@Test
	@SuppressWarnings("unchecked")
	@WithMockUser(username = USER_NAME, roles = { USER_ROLE })
	public void testGetNonExistent() throws Exception {
		when(service.getResult(eq(token))).thenThrow(NoSuchScanException.class);
		mvc.perform(get("/secured/scan/result/{token}", token).contentType(APPLICATION_JSON)).andExpect(status().isNotFound());
	}

	@Test
	@WithMockUser(username = USER_NAME, roles = { USER_ROLE })
	public void testGetProteins() throws Exception {
		when(service.getProteinHashes(eq(token))).thenAnswer(invocation -> {
			LinkedHashMap<String, MsMsProteinHit> proteinHits = new LinkedHashMap<>();
			readData().createHashes(proteinHits, new LinkedHashMap<>());
			Map<String, MsMsShortProteinHit> mapped = new HashMap<>(proteinHits.size());
			proteinHits.forEach((key, value) -> {
				mapped.put(key, mapper.map(value, MsMsShortProteinHit.class));
			});
			return mapped;
		});
		MvcResult result = mvc.perform(get("/secured/scan/result/{token}/proteins", token).contentType(APPLICATION_JSON)).andExpect(status().isOk())
				.andReturn();
		String responseAsString = result.getResponse().getContentAsString();
		assertNotEquals(0, responseAsString.length());
		assertNotEquals(0, new JSONObject(responseAsString).length());
	}

	@Test
	@WithMockUser(username = USER_NAME, roles = { USER_ROLE })
	public void testGetPeptides() throws Exception {
		String protein = "unassigned";
		when(service.getPeptideHashes(eq(token), eq(protein))).thenAnswer(invocation -> {
			LinkedHashMap<String, MsMsProteinHit> proteinHits = new LinkedHashMap<>();
			LinkedHashMap<String, MsMsPeptideHit> peptideHits = new LinkedHashMap<>();
			readData().createHashes(proteinHits, peptideHits);
			peptideHits.values().stream().forEach(pep -> {
				pep.setProteins(null);
			});
			//@formatter:off
			TypeToken<LinkedHashMap<String, MsMsShortPeptideHit>> token = new TypeToken<LinkedHashMap<String, MsMsShortPeptideHit>>(){};
			//@formatter:on
			return mapper.map(proteinHits.get("N/A").getPeptides(), token.getType());
		});
		MvcResult result = mvc
				.perform(get("/secured/scan/result/{token}/proteins/{protein}", token, protein).accept(APPLICATION_JSON).contentType(APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();
		String responseAsString = result.getResponse().getContentAsString();
		assertNotEquals(0, responseAsString.length());
		assertNotEquals(0, new JSONObject(responseAsString).length());
	}

}
