package pl.pw.rb.eng.web.servlet.admin;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Random;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import pl.pw.rb.eng.db.entities.ScanStatus;
import pl.pw.rb.eng.service.ScannerService;
import pl.pw.rb.eng.tests.ServletTest;
import pl.pw.rb.eng.tests.UnitTest;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Category({ UnitTest.class, ServletTest.class })
public class AdminServletTest {

	private static final String ENDPOINT = "/secured/admin/status";

	@MockBean
	private ScannerService service;

	@Autowired
	private MockMvc mvc;

	@Test
	@WithMockUser(username = "admin", roles = "ADMIN")
	public void testGetStatus() throws Exception {
		final Random r = new Random();
		final int running = r.nextInt(), waiting = r.nextInt();
		when(service.count(eq(ScanStatus.RUNNING))).thenReturn(running);
		when(service.count(eq(ScanStatus.WAITING))).thenReturn(waiting);
		MvcResult result = mvc.perform(get(ENDPOINT)).andExpect(status().isOk()).andReturn();
		JSONObject jsonObject = new JSONObject(result.getResponse().getContentAsString());
		assertTrue(jsonObject.has("scanCount"));
		assertTrue(jsonObject.has("waitingScanCount"));
	}

	@Test
	@WithMockUser(username = "user", roles = "USER")
	public void testUnauthorizedUser() throws Exception {
		mvc.perform(get(ENDPOINT)).andExpect(status().isForbidden());
	}

	@Test
	public void testAnonymousUser() throws Exception {
		mvc.perform(get(ENDPOINT)).andExpect(status().isUnauthorized());
	}

}
