package pl.pw.rb.eng.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import pl.pw.rb.eng.db.dao.TemplateDAO;
import pl.pw.rb.eng.db.entities.EmailTemplate;
import pl.pw.rb.eng.service.TemplateService;
import pl.pw.rb.eng.springconfig.PeptidesConfiguration;
import pl.pw.rb.eng.tests.UnitTest;

@RunWith(SpringRunner.class)
@Category({ UnitTest.class })
@ActiveProfiles("test")
public class TemplateServiceUnitTest {

	@TestConfiguration
	public static class Configuration extends PeptidesConfiguration {

		@Bean
		TemplateService service() {
			return new TemplateServiceImpl();
		}
	}

	@Autowired
	private TemplateService service;

	@MockBean
	private TemplateDAO dao;

	@Mock
	private EmailTemplate mock;

	@Test
	public void testGetTemplate() {
		when(dao.findByNameAndType(anyString(), eq("txt"))).thenReturn(mock);
		assertNotNull(service.getTemplate("abc", "txt"));
	}

	@Test
	public void testUpdate() {
		when(dao.save((EmailTemplate) anyObject())).thenReturn(mock);
		assertEquals(service.update(mock), mock);
	}

	@Test
	public void testPopulateFromResources() throws Exception {
		when(dao.save((EmailTemplate) anyObject())).thenReturn(mock);
		service.populateFromResources();
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		verify(dao, times(resolver.getResources("/templates/*").length)).save((EmailTemplate) anyObject());
	}

}
