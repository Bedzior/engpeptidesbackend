package pl.pw.rb.eng.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import pl.pw.rb.eng.db.dao.UploadFileDAO;
import pl.pw.rb.eng.db.entities.UploadFile;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.service.UploadFileService;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.service.exception.NoScanFileException;
import pl.pw.rb.eng.springconfig.PeptidesConfiguration;
import pl.pw.rb.eng.tests.UnitTest;

@RunWith(SpringRunner.class)
@Category({ UnitTest.class })
@ActiveProfiles("test")
public class UploadFileServiceUnitTest {

	@TestConfiguration
	public static class Configuration extends PeptidesConfiguration {

		@Bean
		UploadFileService service() {
			return new UploadFileServiceImpl();
		}
	}

	@Autowired
	UploadFileService service;

	@MockBean
	UsersService usersService;

	@MockBean
	UploadFileDAO dao;

	@Rule
	public TemporaryFolder temp = new TemporaryFolder();

	@Test
	public void testCreate() throws Exception {
		String user = "user";
		final User mockedUser = mock(User.class);
		when(usersService.getByLogin(eq(user))).thenReturn(mockedUser);
		when(dao.save((UploadFile) anyObject())).thenAnswer(AdditionalAnswers.returnsFirstArg());
		UploadFile create = service.create(user, temp.newFile());
		assertEquals(create.getUser(), mockedUser);
	}

	@Test
	public void testGet() {
		User mockedUser = mock(User.class);
		final int userID = new Random().nextInt();
		when(mockedUser.getId()).thenReturn(userID);
		String token = RandomStringUtils.random(10);
		UploadFile mockedFile = mock(UploadFile.class);
		when(dao.findByUserAndToken(eq(mockedUser), eq(token))).thenReturn(mockedFile);
		assertEquals(mockedFile, service.get(mockedUser, token));
	}

	@Test
	public void testGetFile() throws Exception {
		User mockedUser = mock(User.class);
		Integer userId = new Random().nextInt();
		when(mockedUser.getId()).thenReturn(userId);
		String token = RandomStringUtils.random(10);
		final UploadFile mock = mock(UploadFile.class);
		when(mock.getFilename()).thenReturn(temp.newFile().getAbsolutePath());
		when(dao.findByUserAndToken(mockedUser, token)).thenReturn(mock);
		assertNotNull(service.getFile(mockedUser, token));
	}

	@Test(expected = NoScanFileException.class)
	public void testGetFileException() throws Exception {
		User mockedUser = mock(User.class);
		Integer userId = new Random().nextInt();
		when(mockedUser.getId()).thenReturn(userId);
		String token = RandomStringUtils.random(10);
		when(dao.findOne(anyString())).thenReturn(null);
		service.getFile(mockedUser, token);
	}

}
