package pl.pw.rb.eng.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import pl.pw.rb.eng.db.dao.UsersDAO;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.service.MailService;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.springconfig.PeptidesConfiguration;
import pl.pw.rb.eng.tests.IntegrationTest;

@RunWith(SpringRunner.class)
@Category({ IntegrationTest.class })
@ActiveProfiles("test")
public class UsersServiceUnitTest {

	@TestConfiguration
	public static class Configuration extends PeptidesConfiguration {

		@Bean
		public UsersService usersService() {
			return new UsersServiceImpl();
		}

	}

	@Autowired
	UsersService service;

	@MockBean
	UsersDAO dao;

	@MockBean
	private PasswordEncoder encoder;

	@MockBean
	private MailService mailService;
	
	@Test
	public void testGetByLogin() {
		User mockedUser = mock(User.class);
		String login = RandomStringUtils.random(new Random().nextInt(20));
		when(mockedUser.getLogin()).thenReturn(login);
		when(dao.findByLogin(eq(login))).thenReturn(mockedUser);
		assertEquals(mockedUser, service.getByLogin(login));
	}

	@Test
	public void testGetByEmail() {
		User mockedUser = mock(User.class);
		String email = RandomStringUtils.random(new Random().nextInt(20)) + "@email.com";
		when(mockedUser.getEmail()).thenReturn(email);
		when(dao.findByEmail(eq(email))).thenReturn(mockedUser);
		assertEquals(mockedUser, service.getByEmail(email));
	}

	@Test
	public void testGetByID() {
		User mockedUser = mock(User.class);
		int id = new Random().nextInt();
		when(mockedUser.getId()).thenReturn(id);
		when(dao.findOne(eq(id))).thenReturn(mockedUser);
		assertEquals(mockedUser, service.getByID(id));
	}

	@Test
	public void testCreateUser() throws Exception {
		String login = RandomStringUtils.random(new Random().nextInt(48), true, false);
		String email = RandomStringUtils.random(new Random().nextInt(40), true, false) + "@abc.com";
		String password = RandomStringUtils.random(new Random().nextInt(40), true, true);
		when(dao.save((User) anyObject())).then(AdditionalAnswers.returnsFirstArg());
		when(encoder.encode(anyObject())).thenReturn(new StringBuilder(password).reverse().toString());
		User user = service.createActivatedUser(login, email, password);
		assertEquals(login, user.getLogin());
		assertEquals(email, user.getEmail());
		assertEquals(encoder.encode(password), user.getHash());
		assertFalse(user.isAdmin());
	}

	@Test
	public void testCreateAdmin() throws Exception {
		String login = RandomStringUtils.random(new Random().nextInt(48), true, false);
		String email = RandomStringUtils.random(new Random().nextInt(40), true, false) + "@abc.com";
		String password = RandomStringUtils.random(new Random().nextInt(40), true, true);
		when(dao.save((User) anyObject())).then(AdditionalAnswers.returnsFirstArg());
		when(encoder.encode(anyObject())).thenReturn(new StringBuilder(password).reverse().toString());
		User user = service.createActivatedAdmin(login, email, password);
		assertEquals(login, user.getLogin());
		assertEquals(email, user.getEmail());
		assertEquals(encoder.encode(password), user.getHash());
		assertTrue(user.isAdmin());
	}

	@Test
	public void testDeleteWhenNonexistent() {
		String login = RandomStringUtils.random(20, true, false);
		service.delete(login);
		Mockito.verify(dao, atMost(0)).delete(anyInt());
		Mockito.verify(dao, atMost(0)).delete((User) anyObject());
	}
}
