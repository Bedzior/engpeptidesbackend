package pl.pw.rb.eng.service.impl;

import static org.junit.Assert.assertTrue;

import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import pl.pw.rb.eng.db.dao.PasswordResetDAO;
import pl.pw.rb.eng.db.entities.PasswordReset;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.service.MailService;
import pl.pw.rb.eng.service.PasswordResetService;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.springconfig.PeptidesConfiguration;
import pl.pw.rb.eng.tests.UnitTest;

@RunWith(SpringRunner.class)
@Category({ UnitTest.class })
@ActiveProfiles("test")
public class PasswordResetServiceUnitTest {

	@TestConfiguration
	public static class Configuration extends PeptidesConfiguration {

		@Bean
		public PasswordResetService passwordResetService() {
			return new PasswordResetServiceImpl();
		}

	}

	@Autowired
	PasswordResetService service;

	@MockBean
	PasswordResetDAO dao;

	@MockBean
	UsersService usersService;

	@MockBean
	MailService mailService;

	final String user = "user";

	@Before
	public void before() {
		when(dao.save((PasswordReset) any())).then(invocation -> {
			PasswordReset passwordReset = mock(PasswordReset.class);
			when(passwordReset.getUuid()).thenReturn(UUID.randomUUID().toString());
			return passwordReset;
		});
	}

	@Test
	public void testVerify() {
		String uuid = UUID.randomUUID().toString();
		final PasswordReset mockPassReset = mock(PasswordReset.class);
		when(dao.findByUuid(eq(uuid))).thenReturn(mockPassReset);
		assertTrue(service.verify(uuid));
	}

	@Test
	public void testCreateForUser() throws Exception {
		final User mockedUser = mock(User.class);
		when(usersService.getByLogin(user)).thenReturn(mockedUser);
		when(mockedUser.getLogin()).thenReturn(user);
		doNothing().when(mailService).send(anyString(), anyString(), anyString(), any());
		service.createForUser(user);
	}

	@Test
	public void testCreateForEmail() throws Exception {
		String email = "abc@abc.com";
		final User mockedUser = mock(User.class);
		when(mockedUser.getLogin()).thenReturn(user);
		when(usersService.getByEmail(email)).thenReturn(mockedUser);
		doNothing().when(mailService).send(eq(email), anyString(), anyString(), any());
		service.createForEmail(email);
	}

}
