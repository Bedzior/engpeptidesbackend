package pl.pw.rb.eng.service.impl;

import static java.text.MessageFormat.format;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.DisableOnDebug;
import org.junit.rules.TemporaryFolder;
import org.junit.rules.TestRule;
import org.junit.rules.Timeout;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import com.fasterxml.jackson.databind.ObjectMapper;

import pl.pw.rb.eng.db.dao.ScansDAO;
import pl.pw.rb.eng.db.dao.UsersDAO;
import pl.pw.rb.eng.db.entities.Database;
import pl.pw.rb.eng.db.entities.Scan;
import pl.pw.rb.eng.db.entities.ScanStatus;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.service.DatabaseService;
import pl.pw.rb.eng.service.ScannerService;
import pl.pw.rb.eng.service.UploadFileService;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.springconfig.PeptidesConfiguration;
import pl.pw.rb.eng.tests.UnitTest;
import pl.pw.rb.eng.tests.WebSocketTest;
import pl.pw.rb.eng.web.servlet.dto.scan.ScanConfiguration;

@RunWith(SpringRunner.class)
@Category({ UnitTest.class, WebSocketTest.class })
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class ScannerProgressEndpointTest {

	@ClassRule
	public static final TemporaryFolder temp = new TemporaryFolder();

	@Rule
	public TestRule rule = new DisableOnDebug(new Timeout(1, TimeUnit.MINUTES));

	@TestConfiguration
	@Order(Ordered.HIGHEST_PRECEDENCE)
	public static class Configuration extends PeptidesConfiguration {

		@Override
		public File tempDir() {
			try {
				return temp.newFolder();
			} catch (IOException e) {
				throw new RuntimeException();
			}
		}
	}

	RestTemplate rest = new RestTemplate();

	@LocalServerPort
	private int port;

	@Autowired
	private PasswordEncoder encoder;

	@Autowired
	private ScannerService scannerService;

	@MockBean
	private ScansDAO dao;

	@MockBean
	private JavaMailSender mailSender;

	@MockBean
	private DatabaseService dbService;

	@MockBean
	private UsersDAO users;

	@MockBean
	private UsersService usersService;

	@MockBean
	private UploadFileService uploadFileService;

	private boolean initialized = false;

	private HttpHeaders headers;

	private String websocketURI;

	private WebSocketStompClient client;

	@Mock
	private Scan scan;

	@Mock
	private User user;

	@Mock
	private Database database;

	@Autowired
	private ObjectMapper mapper;

	@Before
	public void setup() throws Exception {
		if (!initialized) {
			final int userID = new Random().nextInt();
			when(dao.save((Scan) anyObject())).thenReturn(scan);
			final String scanToken = UUID.randomUUID().toString();
			when(dao.findOne(eq(scanToken))).thenReturn(scan);
			when(scan.getId()).thenReturn(scanToken);
			when(scan.getUser()).thenReturn(user);
			when(user.getId()).thenReturn(userID);
			when(user.getEmail()).thenReturn("admin@admin.com");
			when(user.getRoles()).thenCallRealMethod();
			when(user.getAuthorities()).thenCallRealMethod();
			when(user.isActivated()).thenReturn(true);
			final String login = "admin";
			final String password = "admin";
			when(user.getLogin()).thenReturn(login);
			when(user.getHash()).thenReturn(encoder.encode(password));
			when(users.findByLogin(login)).thenReturn(user);
			when(users.findOne(userID)).thenReturn(user);
			when(usersService.getByID(userID)).thenReturn(user);
			when(usersService.getByLogin(login)).thenReturn(user);
			when(dbService.getDatabase(anyString())).thenReturn(database);
			when(dbService.getDatabase(anyInt())).thenReturn(database);
			when(database.getName()).thenReturn("testDBName");
			when(database.getPath()).thenReturn(temp.newFile().getAbsolutePath());
			when(uploadFileService.getFile(anyObject(), anyString())).thenReturn(temp.newFile());
			websocketURI = format("ws://localhost:{0,number,#}/api/ws", port);
			initialized = true;
			//@formatter:off
			JSONObject form = new JSONObject()
					.put("j_username", login)
					.put("j_password", password);
			//@formatter:on
			ResponseEntity<String> postForEntity = rest.postForEntity(format("http://localhost:{0,number,#}/api/login", port), form.toString(0), String.class);
			HttpHeaders headers = postForEntity.getHeaders();
			List<String> list = headers.get("Authorization");
			this.headers = new HttpHeaders();
			this.headers.add("Authentication", list.get(0));
			client = new WebSocketStompClient(new SockJsClient(asList(new WebSocketTransport(new StandardWebSocketClient()))));
		}
	}

	@Test
	public void testAuthorized() throws Exception {
		assertNotNull(client.connect(websocketURI, new WebSocketHttpHeaders(headers), new StompSessionHandlerAdapter() {
		}).get());
	}

	@Test(expected = TimeoutException.class)
	public void testUnauthorized() throws Exception {
		StompSession session = null;
		try {
			session = client.connect(websocketURI, new WebSocketHttpHeaders(), new StompSessionHandlerAdapter() {
			}).get(5, TimeUnit.SECONDS);
		} finally {
			if (session != null)
				session.disconnect();
		}
	}

	@Test
	public void testScanInitialization() throws Exception {
		final CompletableFuture<JSONObject> future = new CompletableFuture<>();
		final ScanConfiguration configuration;
		try (BufferedReader is = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/config.json")))) {
			configuration = mapper.readValue(is, ScanConfiguration.class);
			configuration.getOther().setFiles("token");
		}
		StompSession session = null;
		try {
			session = client.connect(websocketURI, new WebSocketHttpHeaders(headers), new StompSessionHandlerAdapter() {
			}).get();
			Scan scan = scannerService.createFor(this.user, session.getSessionId(), configuration);
			when(dao.findOne(eq(scan.getId()))).thenReturn(scan);
			when(scan.getStatus()).thenReturn(ScanStatus.RUNNING);
			session.subscribe("/topic/scan/" + scan.getId(), new StompSessionHandlerAdapter() {

				@Override
				public void handleFrame(StompHeaders headers, Object payload) {
					if (payload instanceof byte[])
						future.complete(new JSONObject(new String((byte[]) payload)));
				}

				@Override
				public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {
					future.completeExceptionally(exception);
				}

				@Override
				public void handleTransportError(StompSession session, Throwable exception) {
					future.completeExceptionally(exception);
				}

				@Override
				public Type getPayloadType(StompHeaders headers) {
					return byte[].class;
				}
			});
			assertNotNull(future.get());
		} finally {
			if (session != null)
				session.disconnect();
		}
	}

	@Test
	public void testCancelCommand() throws Exception {
		final CompletableFuture<JSONObject> future = new CompletableFuture<>();
		final ScanConfiguration configuration;
		try (BufferedReader is = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/config.json")))) {
			configuration = mapper.readValue(is, ScanConfiguration.class);
			configuration.getOther().setFiles("token");
		}
		StompSession session = null;
		try {
			session = client.connect(websocketURI, new WebSocketHttpHeaders(headers), new StompSessionHandlerAdapter() {
			}).get();
			Scan scan = scannerService.createFor(this.user, session.getSessionId(), configuration);
			session.subscribe("/topic/scan/" + scan.getId(), new StompSessionHandlerAdapter() {

				@Override
				public void handleFrame(StompHeaders headers, Object payload) {
					if (payload instanceof byte[]) {
						JSONObject frame = new JSONObject(new String((byte[]) payload));
						if (frame.has("success") && frame.has("message"))
							future.complete(frame);
					}
				}

				@Override
				public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {
					future.completeExceptionally(exception);
				}

				@Override
				public void handleTransportError(StompSession session, Throwable exception) {
					future.completeExceptionally(exception);
				}

				@Override
				public Type getPayloadType(StompHeaders headers) {
					return byte[].class;
				}
			});
			session.send("/app/scan/" + scan.getId(), new JSONObject().put("command", "cancel").toString().getBytes(StandardCharsets.UTF_8));
			final JSONObject response = future.get();
			assertNotNull(response);
			assertTrue(response.getBoolean("success"));
			assertNull(scannerService.getEngine(scan.getId()));
		} finally {
			if (session != null)
				session.disconnect();
		}
	}

	// TODO test with unauthorized users

}
