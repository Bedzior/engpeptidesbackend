package pl.pw.rb.eng.service.impl;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.apache.commons.io.IOUtils.readFully;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.Writer;
import java.util.Arrays;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import mscanlib.ms.mass.MassTools;
import pl.pw.rb.eng.db.dao.ScanResultDAO;
import pl.pw.rb.eng.db.entities.Database;
import pl.pw.rb.eng.db.entities.Scan;
import pl.pw.rb.eng.db.entities.ScanResult;
import pl.pw.rb.eng.service.DatabaseService;
import pl.pw.rb.eng.service.ScanResultService;
import pl.pw.rb.eng.service.ScannerService;
import pl.pw.rb.eng.springconfig.PeptidesConfiguration;
import pl.pw.rb.eng.tests.UnitTest;
import pl.pw.rb.eng.web.servlet.dto.scan.MsMsShortPeptideHit;
import pl.pw.rb.eng.web.servlet.dto.scan.MsMsShortProteinHit;
import pl.pw.rb.eng.web.servlet.dto.scan.MsOutputDTO;

@RunWith(SpringRunner.class)
@Category({ UnitTest.class })
@ActiveProfiles("test")
public class ScanResultServiceUnitTest {

	@BeforeClass
	public static final void beforeClass() throws Exception {
		MassTools.initMaps();
	}

	@TestConfiguration
	public static class Configuration extends PeptidesConfiguration {

		@Bean
		ScanResultService service() {
			return new ScanResultServiceImpl();
		}

		@Bean
		ObjectMapper jacksonMapper() {
			Jackson2ObjectMapperBuilder b = new Jackson2ObjectMapperBuilder();
			b.autoDetectFields(true);
			b.autoDetectGettersSetters(true);
			b.indentOutput(true);
			b.timeZone(TimeZone.getDefault());
			b.featuresToEnable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, DeserializationFeature.READ_ENUMS_USING_TO_STRING);
			return b.build();
		}
	}

	@Autowired
	ScanResultService service;

	@MockBean
	DatabaseService dbService;

	@MockBean
	ScanResultDAO dao;

	@MockBean
	ScannerService scannerService;

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	@Mock
	Scan mockScan;

	@Mock
	ScanResult result;

	@Mock
	private Database mockDatabase;

	@Test
	public void testCreate() throws Exception {
		String scanID = UUID.randomUUID().toString();
		when(dao.save((ScanResult) anyObject())).thenAnswer(returnsFirstArg());
		when(scannerService.getSecure(scanID)).thenReturn(mockScan);
		when(mockScan.getId()).thenReturn(scanID);
		final File newFile = folder.newFile();
		final String content = "hello\n";
		try (Writer fw = new FileWriter(newFile, false)) {
			fw.write(content);
		}
		final byte[] buffer = new byte[(int) newFile.length()];
		try (FileInputStream reader = new FileInputStream(newFile)) {
			IOUtils.readFully(reader, buffer);
		}
		ScanResult create = service.create(scanID, newFile);
		assertEquals(create.getScan().getId(), scanID);
		assertEquals(create.getName(), newFile.getName());
		assertTrue(Arrays.equals(create.getResult(), buffer));
	}

	@Test
	public void testGetResult() throws Exception {
		byte[] data = IOUtils.toByteArray(getClass().getResourceAsStream("/test_data/scan.out"));
		String token = "someToken";
		when(mockScan.getResult()).thenReturn(data);
		when(scannerService.get(eq(token))).thenReturn(mockScan);
		when(result.getResult()).thenReturn(data);
		when(dao.findByScan(eq(mockScan))).thenReturn(result);
		MsOutputDTO result = service.getResult(token);
		assertNotNull(result);
		assertEquals(0, result.getErrors().length);
		assertNotEquals(0, result.getQueries().length);
	}

	@Test
	public void testGetProteins() throws Exception {
		String token = RandomStringUtils.randomAlphabetic(20);
		when(scannerService.get(eq(token))).thenReturn(mockScan);
		when(scannerService.getSecure(eq(token))).thenReturn(mockScan);
		when(dao.findByScan(eq(mockScan))).thenReturn(result);
		when(dbService.getDatabase(anyInt())).thenReturn(mockDatabase);
		when(result.getResult()).thenAnswer(invocation -> readFully(getClass().getResourceAsStream("/test_data/test.out"), 9812220));
		when(mockScan.getParameters()).thenAnswer(invocation -> IOUtils.toString(getClass().getResourceAsStream("/test_data/real-life-example.json"), UTF_8));
		when(mockDatabase.getRegexName()).thenReturn("$(\\w)\\s.*");

		Map<String, MsMsShortProteinHit> hashes = service.getProteinHashes(token);
		assertNotNull(hashes);
		assertNotEquals(0, hashes.values().size());
		assertTrue(hashes.values().stream().allMatch(p -> p instanceof MsMsShortProteinHit));
	}

	@Test
	public void testGetPeptides() throws Exception {
		String token = RandomStringUtils.randomAlphabetic(20);
		when(scannerService.get(eq(token))).thenReturn(mockScan);
		when(scannerService.getSecure(eq(token))).thenReturn(mockScan);
		when(dao.findByScan(eq(mockScan))).thenReturn(result);
		when(result.getResult()).thenAnswer(invocation -> readFully(getClass().getResourceAsStream("/test_data/test.out"), 9812220));
		
		Map<String, MsMsShortPeptideHit> hashes = service.getPeptideHashes(token, "Q9UPN4");
		assertNotNull(hashes);
		assertNotEquals(0, hashes.values().size());
		assertTrue(hashes.values().stream().allMatch(p -> p instanceof MsMsShortPeptideHit));
	}
}
