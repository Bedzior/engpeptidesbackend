package pl.pw.rb.eng.service.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.nio.charset.StandardCharsets;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.sleroy.fakesmtp.core.ServerConfiguration;
import com.github.sleroy.fakesmtp.model.EmailModel;
import com.github.sleroy.junit.mail.server.test.FakeSmtpRule;

import pl.pw.rb.eng.db.dao.UsersDAO;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.tests.IntegrationTest;
import pl.pw.rb.eng.web.servlet.dto.UserRegistration;

@RunWith(SpringRunner.class)
@Category({IntegrationTest.class})
@AutoConfigureDataJpa
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class UsersServiceIntegrationTest {

	@MockBean
	UsersDAO dao;

	@Autowired
	UsersService service;

	@Rule
	public FakeSmtpRule smtp = new FakeSmtpRule(ServerConfiguration.create() //
			.bind("localhost") //
			.port(25) //
			.relayDomains("def.com") //
			.charset(StandardCharsets.UTF_8.name()));

	@Before
	public void before() {
		assertTrue(smtp.isRunning());
		smtp.mailBox().clear();
	}

	@Test
	public void testSendEmailOnRegistration() throws Exception {
		when(dao.save((User)any())).then(AdditionalAnswers.returnsFirstArg());
		
		UserRegistration form = mock(UserRegistration.class);
		String email = "abc@def.com";
		String login = "login";
		String password = "password";
		when(form.getEmail()).thenReturn(email);
		when(form.getLogin()).thenReturn(login);
		when(form.getPassword()).thenReturn(password);

		service.createUser(form);
		
		assertFalse(smtp.mailBox().isEmpty());
		EmailModel emailModel = smtp.mailBox().get(0);
		assertNotNull(emailModel);
		assertEquals("Peptides Panel | Confirm account registration", emailModel.getSubject());
		assertTrue(emailModel.getEmailStr().contains("uuid="));
	}
	
}
