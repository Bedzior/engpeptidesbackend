package pl.pw.rb.eng.service.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import pl.pw.rb.eng.db.dao.DatabaseDAO;
import pl.pw.rb.eng.db.dao.UsersDAO;
import pl.pw.rb.eng.db.entities.Database;
import pl.pw.rb.eng.db.entities.UploadFile;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.scan.db.DatabaseDownloadTaskTest;
import pl.pw.rb.eng.scan.db.DatabaseDownloader;
import pl.pw.rb.eng.service.DatabaseService;
import pl.pw.rb.eng.service.UploadFileService;
import pl.pw.rb.eng.springconfig.PeptidesConfiguration;
import pl.pw.rb.eng.tests.ExternalSystemsTest;
import pl.pw.rb.eng.tests.IntegrationTest;
import pl.pw.rb.eng.web.dto.DatabaseDownloadRequest;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Category({ IntegrationTest.class })
public class DatabaseServiceIntegrationTest {

	@ClassRule
	public static TemporaryFolder temp = new TemporaryFolder();

	@TestConfiguration
	public static class Configuration extends PeptidesConfiguration {

		@Bean
		@Override
		public File tempDir() {
			try {
				return temp.newFolder();
			} catch (IOException e) {
				throw new RuntimeException("Could not create test temporary directory");
			}
		}
	}

	@Autowired
	public DatabaseService service;

	@Autowired
	DatabaseDAO dao;

	@Autowired
	UsersDAO usersDAO;

	@Autowired
	DatabaseDownloader downloader;

	@MockBean
	UploadFileService uploadFileService;

	private User user;

	@PostConstruct
	private void postConstruct() {
		if (usersDAO.count() == 0L)
			user = usersDAO.save(new User("admin123", "email@admin.com", "tesfr", true));
		else
			user = usersDAO.findAll().iterator().next();
	}

	@Before
	public void before() {
		dao.deleteAll();
	}

	@Test
	public void testGetDatabases() throws Exception {
		final Collection<Database> databases = service.getDatabases(false);
		assertNotNull(databases);
	}

	@Test
	@Category(ExternalSystemsTest.class)
	public void testJSONDownload() throws Exception {
		final String url = DatabaseDownloadTaskTest.SMALL_DOWNLOAD;
		NetUtils.checkInternetConnection(url);
		final DatabaseDownloadRequest request = mock(DatabaseDownloadRequest.class);
		when(request.getName()).thenReturn(RandomStringUtils.randomAlphabetic(10));
		when(request.getRegexId()).thenReturn(RandomStringUtils.randomAlphabetic(10));
		when(request.getRegexName()).thenReturn(RandomStringUtils.randomAlphabetic(10));
		when(request.getUrl()).thenReturn(url);
		Future<Database> future = service.create(user.getLogin(), request);
		assertNotNull(future);
		compareObjects(future.get(2, TimeUnit.MINUTES), request);
	}

	@Test
	@Category(ExternalSystemsTest.class)
	public void testDirectDownload() throws Exception {
		final String url = DatabaseDownloadTaskTest.SMALL_DOWNLOAD;
		NetUtils.checkInternetConnection(url);
		final String name = RandomStringUtils.randomAlphabetic(10);
		final String regexId = "regexId";
		final String regexName = "regexName";
		final String description = RandomStringUtils.randomAlphabetic(30000);
		Future<Database> future = service.create(user.getLogin(), name, description, url, regexId, regexName);
		assertNotNull(future);
		final Database created = future.get(2, TimeUnit.MINUTES);
		assertEquals(name, created.getName());
		assertEquals(regexId, created.getRegexId());
		assertEquals(regexName, created.getRegexName());
		assertEquals(description, created.getDescription());
		assertEquals(url, created.getUrl());
	}

	@Test
	public void testCreateFromUpload() throws Exception {
		final UploadFile mockUpload = mock(UploadFile.class);
		when(mockUpload.getToken()).thenReturn(UUID.randomUUID().toString());
		when(mockUpload.getFilename()).thenReturn(temp.newFile().getAbsolutePath());
		when(uploadFileService.get(anyObject(), anyString())).thenReturn(mockUpload);
		final DatabaseDownloadRequest request = mock(DatabaseDownloadRequest.class);
		when(request.getName()).thenReturn(RandomStringUtils.randomAlphabetic(10));
		when(request.getRegexId()).thenReturn(RandomStringUtils.randomAlphabetic(10));
		when(request.getRegexName()).thenReturn(RandomStringUtils.randomAlphabetic(10));
		Database created = service.create(user.getLogin(), request, mockUpload.getToken());
		compareObjects(created, request);
	}

	private void compareObjects(Database created, DatabaseDownloadRequest request) {
		assertNotNull(created);
		assertNotNull(request);
		assertEquals(request.getName(), created.getName());
		assertEquals(request.getRegexId(), created.getRegexId());
		assertEquals(request.getRegexName(), created.getRegexName());
		if (request.getUrl() != null)
			assertEquals(request.getUrl(), created.getUrl());

	}
}
