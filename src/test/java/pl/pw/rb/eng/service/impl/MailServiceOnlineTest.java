package pl.pw.rb.eng.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMailMessage;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.sleroy.fakesmtp.core.ServerConfiguration;
import com.github.sleroy.fakesmtp.model.EmailModel;
import com.github.sleroy.junit.mail.server.test.FakeSmtpRule;

import pl.pw.rb.eng.db.entities.ScanResult;
import pl.pw.rb.eng.service.MailService;
import pl.pw.rb.eng.service.TemplateService;
import pl.pw.rb.eng.springconfig.PeptidesConfiguration;
import pl.pw.rb.eng.tests.UnitTest;

@RunWith(SpringRunner.class)
@Category({ UnitTest.class })
@ActiveProfiles("test")
@TestPropertySource("/application-test.properties")
public class MailServiceOnlineTest {

	@Rule
	public TemporaryFolder temp = new TemporaryFolder();

	@TestConfiguration
	public static class Configuration extends PeptidesConfiguration {

		@Bean
		public MailService service() {
			return new MailServiceImpl();
		}

		@Bean
		public JavaMailSender maiLSender() {
			return new JavaMailSenderImpl();
		}
	}

	@Rule
	public FakeSmtpRule smtp = new FakeSmtpRule(ServerConfiguration.create().bind("localhost").port(25).relayDomains("gmail.com").charset(StandardCharsets.UTF_8.name()));

	@Autowired
	MailService service;

	@MockBean
	TemplateService templateService;

	@Mock
	private ScanResult mock;

	@Before
	public void before() {
		assertTrue(smtp.isRunning());
		smtp.mailBox().clear();
	}

	@Test
	public void testSend() throws Exception {
		final String subject = "Test subject";
		final String content = "<div><h1>Howdy, stranger</h1><p>Here's some content</p></div>";
		final String recipient = "someperson@gmail.com";
		service.send(recipient, subject, content);

		assertFalse(smtp.mailBox().isEmpty());
		final EmailModel message = smtp.mailBox().get(0);
		assertNotNull(message);
		assertTrue(message.getSubject().contains(subject));
		assertTrue(message.getEmailStr().contains(content));
		assertEquals(recipient, message.getTo());
		assertNull(message.getFilePath());
		MimeMailMessage mimeMessage = getMimeMessage(message);
		assertFalse(hasAttachments(mimeMessage.getMimeMessage()));
	}

	@Test
	public void testSendWithFiles() throws Exception {
		final File newFile = temp.newFile();
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		try (GzipCompressorOutputStream stream = new GzipCompressorOutputStream(out)) {
			stream.write("some data\n".getBytes(StandardCharsets.UTF_8));
		}
		final String recipient = "someuser@gmail.com";
		final String subject = "Test subject";
		final String content = "<div><h1>Howdy, stranger</h1><p>Here's some content</p></div>";
		when(mock.getName()).thenReturn(newFile.getName());
		when(mock.getResult()).thenReturn(out.toByteArray());
		service.send(recipient, subject, content, Collections.singleton(mock));
		assertFalse(smtp.mailBox().isEmpty());
		final EmailModel message = smtp.mailBox().get(0);
		assertNotNull(message);
		assertTrue(message.getSubject().contains(subject));
		assertTrue(message.getEmailStr().contains(content));
		assertEquals(recipient, message.getTo());
		MimeMailMessage mimeMessage = getMimeMessage(message);
		assertTrue(hasAttachments(mimeMessage.getMimeMessage()));
	}

	/**
	 * <a href="https://javaee.github.io/javamail/FAQ#hasattach">source</a>
	 * 
	 * @param msg {@code Message} to check for attachments
	 * @return true if the specified message contains any attachments
	 * @throws Exception
	 */
	boolean hasAttachments(Message msg) throws Exception {
		if (msg.isMimeType("multipart/mixed")) {
			Multipart mp = (Multipart) msg.getContent();
			for (int i = 0; i < mp.getCount(); i++) {
				BodyPart m = mp.getBodyPart(i);
				if (MimeMessage.ATTACHMENT.equals(m.getDisposition()))
					return true;
			}
		}
		return false;
	}

	private MimeMailMessage getMimeMessage(EmailModel model) throws MessagingException {
		return new MimeMailMessage(new MimeMessage(Session.getDefaultInstance(new Properties()), new ByteArrayInputStream(model.getEmailStr().getBytes())));
	}

	@Test(expected = AddressException.class)
	public void testError() throws Exception {
		service.send("notanaddress%$%", "fdn", "<html>");
	}
}
