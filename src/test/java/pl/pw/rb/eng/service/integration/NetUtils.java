package pl.pw.rb.eng.service.integration;

import static java.text.MessageFormat.format;
import static org.junit.Assume.assumeNoException;
import static org.junit.Assume.assumeTrue;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.http.HttpStatus;

public class NetUtils {

	private static final String DEFAULT_FTP_NO_USER = "anonymous";

	private static final String DEFAULT_FTP_NO_PASS = "guest";

	public static void checkInternetConnection(String url) {
		try {
			int responseCode;
			if (url.startsWith("ftp:")) {
				FTPClient client = new FTPClient();
				URL _url = new URL(url);

				client.enterLocalPassiveMode();
				client.connect(_url.getHost());
				client.login(DEFAULT_FTP_NO_USER, DEFAULT_FTP_NO_PASS);
				responseCode = client.getReplyCode();
				// TODO actually check file resource
				assumeTrue(format("Expected a positive response, got {0}", responseCode), FTPReply.isPositiveCompletion(responseCode));
			} else if (url.startsWith("http:")) {
				responseCode = ((HttpURLConnection) new URL(url).openConnection()).getResponseCode();
				assumeTrue(HttpStatus.valueOf(responseCode).is2xxSuccessful());
			} else if (url.startsWith("https:")) {
				responseCode = ((HttpsURLConnection) new URL(url).openConnection()).getResponseCode();
				assumeTrue(HttpStatus.valueOf(responseCode).is2xxSuccessful());
			} else {
				throw new RuntimeException("Unexpected URL scheme");
			}
		} catch (IOException e) {
			assumeNoException(e);
		}
	}

}
