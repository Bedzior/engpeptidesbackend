package pl.pw.rb.eng.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import mscanlib.ms.mass.PTMMap;
import pl.pw.rb.eng.service.ScanConfigurationService;
import pl.pw.rb.eng.springconfig.PeptidesConfiguration;
import pl.pw.rb.eng.tests.ExternalSystemsTest;
import pl.pw.rb.eng.tests.UnitTest;

@RunWith(SpringRunner.class)
@Category({ UnitTest.class, ExternalSystemsTest.class })
@ActiveProfiles("test")
public class ScanConfigurationServiceUnitTest {

	@TestConfiguration
	public static class Configuration extends PeptidesConfiguration {

		@Bean
		public ScanConfigurationService passwordResetService() {
			return new ScanConfigurationServiceImpl();
		}

	}

	@Autowired
	ScanConfigurationService service;

	@Test
	public void testNotEmpty() {
		assertFalse(service.getAvailablePTMs().isEmpty());
		assertFalse(service.getEnzymes(true).isEmpty());
		assertFalse(service.getEnzymes(false).isEmpty());
		assertFalse(service.getMMDUnits().isEmpty());
		assertFalse(service.getFragmentationRules().isEmpty());
	}

	@Test
	public void testPTMs() {
		assertEquals(service.getAvailablePTMs().size(), PTMMap.getPTMsList(true).size());
	}

	@Test
	public void testDifference() {
		assertTrue(service.getEnzymes(true).size() < service.getEnzymes(false).size());
	}

}
