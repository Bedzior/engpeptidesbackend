package pl.pw.rb.eng.service.integration;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.UUID;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;

import pl.pw.rb.eng.db.entities.Database;
import pl.pw.rb.eng.db.entities.Scan;
import pl.pw.rb.eng.db.entities.UploadFile;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.service.DatabaseService;
import pl.pw.rb.eng.service.ScannerService;
import pl.pw.rb.eng.service.UploadFileService;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.tests.IntegrationTest;
import pl.pw.rb.eng.web.servlet.dto.scan.ScanConfiguration;

@RunWith(SpringRunner.class)
@AutoConfigureDataJpa
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Category({ IntegrationTest.class })
@ActiveProfiles("test")
public class ScannerServiceTest {

	@Autowired
	ScannerService service;

	@Autowired
	UsersService usersService;

	@Autowired
	UploadFileService uploadFileService;

	@MockBean
	DatabaseService databaseService;

	@Autowired
	ObjectMapper mapper;

	@Rule
	public TemporaryFolder temp = new TemporaryFolder();

	private User user;

	@Before
	public void before() throws Exception {
		if (usersService.getByLogin("user") != null) {
			usersService.delete("user");
		}
		user = usersService.createActivatedUser("user", "email@email.com", "password");
	}

	@Test
	@WithMockUser(username = "user", roles = "USER")
	public void testStopping() throws Exception {
		when(databaseService.getDatabase(anyInt())).thenReturn(mock(Database.class));
		UploadFile uploadFile = uploadFileService.create(user.getLogin(), temp.newFile());
		final ScanConfiguration jsonConfiguration;
		try (BufferedReader is = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/config.json")))) {
			jsonConfiguration = mapper.readValue(is, ScanConfiguration.class);
			jsonConfiguration.getOther().setFiles(uploadFile.getToken());
		}
		String sessionID = UUID.randomUUID().toString();
		Scan scan = service.createFor(user, sessionID, jsonConfiguration);
		assertNotNull(scan);
		assertNotNull(service.getEngine(scan.getId()));
		service.stopTask(scan.getId());
		assertNull(service.getEngine(scan.getId()));
	}
}
