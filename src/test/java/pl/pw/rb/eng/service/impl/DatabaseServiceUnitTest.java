package pl.pw.rb.eng.service.impl;

import static org.apache.commons.lang3.RandomStringUtils.random;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.RandomUtils;
import org.hamcrest.Matchers;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.AdditionalAnswers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.thedeanda.lorem.LoremIpsum;

import pl.pw.rb.eng.db.dao.DatabaseDAO;
import pl.pw.rb.eng.db.entities.Database;
import pl.pw.rb.eng.db.entities.UploadFile;
import pl.pw.rb.eng.scan.db.DatabaseDownloader;
import pl.pw.rb.eng.service.DatabaseService;
import pl.pw.rb.eng.service.UploadFileService;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.service.exception.DatabaseAlreadyDownloadingException;
import pl.pw.rb.eng.service.exception.MissingFileUploadException;
import pl.pw.rb.eng.springconfig.PeptidesConfiguration;
import pl.pw.rb.eng.tests.UnitTest;
import pl.pw.rb.eng.web.dto.DatabaseDownloadRequest;
import pl.pw.rb.eng.web.servlet.dto.DatabaseUpdateDTO;
import pl.pw.rb.eng.web.servlet.exception.db.MissingDatabaseException;

@RunWith(SpringRunner.class)
@Category({ UnitTest.class })
@ActiveProfiles("test")
public class DatabaseServiceUnitTest {

	@ClassRule
	public static TemporaryFolder temp = new TemporaryFolder();

	@TestConfiguration
	public static class Configuration extends PeptidesConfiguration {

		@Bean
		public DatabaseService databaseService() {
			return new DatabaseServiceImpl();
		}

		@Override
		public File databaseDir() {
			try {
				return temp.newFolder();
			} catch (IOException e) {
				throw new RuntimeException();
			}
		}
	}

	@Autowired
	DatabaseService service;

	@MockBean
	SimpMessagingTemplate simp;

	@MockBean
	DatabaseDAO dao;

	@MockBean
	UsersService usersService;

	@MockBean
	UploadFileService uploadFileService;

	@MockBean
	DatabaseDownloader downloader;

	@Autowired
	public File databaseDir;

	private final LoremIpsum lorem = LoremIpsum.getInstance();

	@Mock
	private Database database;

	@Test
	public void testGetDatabases() {
		final ArrayList<Database> list = new ArrayList<>();
		list.add(mock(Database.class));
		list.add(mock(Database.class));
		list.add(mock(Database.class));
		when(dao.findDatabaseByDownloading(eq(false))).thenReturn(list);
		assertFalse(service.getDatabases(false).isEmpty());
	}

	@Test
	public void testGetDatabaseInteger() {
		Integer id = new Random().nextInt();
		when(dao.findOne(eq(id))).thenReturn(database);
		Database retrieved = service.getDatabase(id);
		assertEquals(database, retrieved);
	}

	@Test
	public void testGetDatabaseString() {
		String name = random(6);
		when(dao.findByName(eq(name))).thenReturn(database);
		Database retrieved = service.getDatabase(name);
		assertEquals(database, retrieved);
	}

	@Test
	public void testRedownloadWithId() throws Exception {
		int id = new Random().nextInt();
		final String dbName = randomAlphabetic(50);
		when(dao.findOne(id)).thenReturn(database);
		when(dao.exists(id)).thenReturn(true);
		when(database.getUrl()).thenReturn(randomAlphabetic(100));
		when(database.getName()).thenReturn(dbName);
		when(dao.save(Mockito.<Database>anyObject())).then(AdditionalAnswers.returnsFirstArg());
		final CompletableFuture<File> future = new CompletableFuture<File>();
		when(downloader.requestDownload(anyString(), anyString())).thenReturn(future);
		CompletableFuture<Database> redownloaded = service.redownload(id);
		future.complete(temp.newFile());
		final Database db = redownloaded.get();
		assertNotNull(db);
		assertEquals(database.getUrl(), db.getUrl());
		assertEquals(database.getName(), db.getName());
	}

	@Test
	public void testRedownloadWithName() throws Exception {
		String dbName = randomAlphanumeric(100);
		when(dao.findByName(eq(dbName))).thenReturn(database);
		when(database.getName()).thenReturn(dbName);
		when(database.getUrl()).thenReturn(randomAlphabetic(100));
		when(dao.save(Mockito.<Database>anyObject())).then(AdditionalAnswers.returnsFirstArg());
		final CompletableFuture<File> future = new CompletableFuture<File>();
		when(downloader.requestDownload(anyString(), anyString())).thenReturn(future);
		CompletableFuture<Database> redownloaded = service.redownload(dbName);
		future.complete(temp.newFile());
		final Database db = redownloaded.get();
		assertNotNull(db);
		assertEquals(database.getUrl(), db.getUrl());
	}

	@Test(expected = DatabaseAlreadyDownloadingException.class)
	public void testRedownloadError() throws Exception {
		String dbName = randomAlphanumeric(20);
		when(downloader.isDownloading(eq(dbName))).thenReturn(true);
		when(dao.findByName(dbName)).thenReturn(database);
		when(database.getName()).thenReturn(dbName);
		service.redownload(dbName);
	}

	@Test(expected = MissingDatabaseException.class)
	public void testRedownloadNoDatabase() throws Exception {
		String dbName = randomAlphanumeric(20);
		when(dao.findByName(eq(dbName))).thenReturn(null);
		service.redownload(dbName);
	}

	@Test
	public void testDelete() throws Exception {
		Database mock = mock(Database.class);
		when(mock.getId()).thenReturn(new Random().nextInt(Integer.MAX_VALUE));
		when(dao.findOne(eq(mock.getId()))).thenReturn(mock);
		final File tempFile = temp.newFile();
		when(mock.getPath()).thenReturn(tempFile.getAbsolutePath());
		service.delete(mock.getId());
		assertFalse(tempFile.exists());
	}

	@Test
	public void testUpdateByID() throws Exception {
		when(dao.save((Database) anyObject())).thenAnswer(AdditionalAnswers.returnsFirstArg());
		final DatabaseUpdateDTO data = createDatabaseUpdateDTO();

		Database db = spy(new Database());
		final int id = new Random().nextInt();
		when(db.getId()).thenReturn(id);
		when(db.getId()).thenReturn(id);
		when(dao.findOne(eq(id))).thenReturn(db);
		Database update = service.update(db.getId(), data);
		assertEquals(data.getRegexId(), update.getRegexId());
		assertEquals(data.getRegexName(), update.getRegexName());
		assertEquals(data.getUrl(), update.getUrl());
	}

	public DatabaseUpdateDTO createDatabaseUpdateDTO() {
		Random r = new Random();
		final String name = randomAlphanumeric(r.nextInt(100));
		final String regexID = randomAlphanumeric(r.nextInt(100));
		final String regexName = random(r.nextInt(100));
		final String url = random(r.nextInt(100));
		final String description = randomAlphanumeric(r.nextInt(1000));
		return new DatabaseUpdateDTO(name, regexID, regexName, url, description);
	}

	@Test(expected = MissingDatabaseException.class)
	public void testUpdateNoIDData() throws Exception {
		service.update(-1, mock(DatabaseUpdateDTO.class));
	}

	@Test
	public void testCreateFromPreUpload() throws Exception {
		String user = random(10);
		String token = random(10);
		final UploadFile mockFile = mock(UploadFile.class);
		when(mockFile.getFilename()).thenReturn(temp.newFile().getAbsolutePath());
		when(uploadFileService.get(anyObject(), eq(token))).thenReturn(mockFile);
		when(dao.save(Mockito.<Database>anyObject())).thenAnswer(AdditionalAnswers.returnsFirstArg());
		final DatabaseDownloadRequest object = mock(DatabaseDownloadRequest.class);
		when(object.getName()).thenReturn("name");
		Database create = service.create(user, object, token);
		assertEquals("name", create.getName());
		assertEquals(mockFile.getFilename(), create.getPath());
	}

	/**
	 * Verify that the largest file had been added as a FASTA database
	 * 
	 * @throws Exception
	 */
	@Test
	public void testImportFrom() throws Exception {
		Random r = new Random();
		long max = Long.MIN_VALUE;
		File maxSizeFile = null;
		File directory = null;
		do {
			try {
				directory = temp.newFolder("importsDir", "database1");
			} catch (final Exception e) {
				temp.create();
			}
		} while (directory == null || !directory.exists());
		for (int i = 0; i < 3; i++) {
			File file = new File(directory, "file" + i);
			try (FileWriter fw = new FileWriter(file)) {
				fw.write(random(r.nextInt(1000)));
			}
			final long length = file.length();
			if (length > max) {
				max = length;
				maxSizeFile = file;
			}
		}
		service.importFrom(directory.getParentFile());
		verify(dao).save(Mockito.argThat(Matchers.<Database>hasProperty("path", equalTo(maxSizeFile.getAbsolutePath()))));
	}

	@Test
	public void testCreateFromJSON() throws Exception {
		String dbName = random(10);
		String user = randomAlphabetic(10);
		final String url = "someURL";
		final DatabaseDownloadRequest request = mock(DatabaseDownloadRequest.class);
		final CompletableFuture<File> task = new CompletableFuture<File>();
		when(downloader.requestDownload(eq(dbName), eq(url))).thenReturn(task);
		when(dao.save((Database) anyObject())).thenAnswer(AdditionalAnswers.returnsFirstArg());
		when(request.getName()).thenReturn(dbName);
		when(request.getUrl()).thenReturn(url);
		final File dir = temp.newFolder();
		final File file = new File(dir, "someDB.txt");
		try (FileWriter fos = new FileWriter(file)) {
			fos.write(lorem.getParagraphs(20, 20));
		}
		CompletableFuture<Database> future = service.create(user, request);
		task.complete(file);
		Database db = future.get();
		assertNotNull(db);
		assertEquals(dbName, db.getName());
		assertEquals(url, db.getUrl());
	}

	@Test
	public void testCreateFromDirectFields() throws Exception {
		final String name = random(10);
		final String user = randomAlphabetic(10);
		final String url = "someURL";
		final String regexId = randomAlphabetic(10);
		final String regexName = randomAlphabetic(10);
		final String description = randomAlphabetic(20000);
		final CompletableFuture<File> task = new CompletableFuture<File>();
		when(downloader.requestDownload(eq(name), eq(url))).thenReturn(task);
		when(dao.save((Database) anyObject())).thenAnswer(AdditionalAnswers.returnsFirstArg());
		final File dir = temp.newFolder();
		final File file = new File(dir, "someDB.txt");
		try (FileWriter fos = new FileWriter(file)) {
			fos.write(lorem.getParagraphs(20, 20));
		}
		CompletableFuture<Database> future = service.create(user, name, description, url, regexId, regexName);
		task.complete(file);
		Database db = future.get();
		assertNotNull(db);
		assertEquals(name, db.getName());
		assertEquals(url, db.getUrl());
		assertEquals(new File(databaseDir, db.getName()).getAbsolutePath(), new File(db.getPath()).getParent());
		assertEquals(usersService.getByLogin(user), db.getUser());
		assertEquals(regexId, db.getRegexId());
		assertEquals(regexName, db.getRegexName());
	}

	// @Test
	// public void testCreateAllAttributes() {
	// final String name = random(10);
	// service.create(user, object, uploadedFileToken, sessionID);
	// }

	@Test(expected = MissingFileUploadException.class)
	public void testCreateNoUploadToken() throws Exception {
		final String name = random(10);
		final String user = randomAlphabetic(10);
		final String regexId = randomAlphabetic(10);
		final String regexName = randomAlphabetic(10);
		DatabaseDownloadRequest json = mock(DatabaseDownloadRequest.class);
		when(json.getName()).thenReturn(name);
		when(json.getRegexId()).thenReturn(regexId);
		when(json.getRegexName()).thenReturn(regexName);
		service.create(user, json, "");
	}

	// @Test
	// public void testCreateNoUploadedFile() {
	// service.create(user, object, uploadedFileToken, sessionID);
	// }

	@Test
	public void testWithDownloadError() throws Exception {
		String dbName = random(10);
		String user = randomAlphabetic(10);
		final String url = "someURL";
		final DatabaseDownloadRequest request = mock(DatabaseDownloadRequest.class);
		final CompletableFuture<File> task = new CompletableFuture<File>();
		when(downloader.requestDownload(eq(dbName), eq(url))).thenReturn(task);
		when(request.getName()).thenReturn(dbName);
		when(request.getUrl()).thenReturn(url);
		final File dir = temp.newFolder();
		final File file = new File(dir, "someDB.txt");
		try (FileWriter fos = new FileWriter(file)) {
			fos.write(lorem.getParagraphs(20, 20));
		}
		CompletableFuture<Database> future = service.create(user, request);
		task.completeExceptionally(mock(ExecutionException.class));
		assertTrue(future.isCompletedExceptionally());
	}

	@Test
	public void testGetLargest() throws Exception {
		final File testDir = temp.newFolder("sizeTest");
		File largest = null;
		int largestSize = Integer.MIN_VALUE;
		for (int i = 0; i < 3; i++) {
			final File tempFile = File.createTempFile("size-test-", Integer.toString(i + 1), testDir);
			final int size = RandomUtils.nextInt();
			try (OutputStream os = new FileOutputStream(tempFile)) {
				os.write(RandomUtils.nextBytes(size));
			}
			if (size > largestSize) {
				largestSize = size;
				largest = tempFile;
			}
		}
		assertEquals(largest, DatabaseService.getLargestFileFrom(testDir));
		assertEquals(largest, DatabaseService.getLargestFileFrom(largest));

	}
}
