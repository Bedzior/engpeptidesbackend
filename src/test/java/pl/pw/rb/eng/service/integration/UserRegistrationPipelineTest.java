package pl.pw.rb.eng.service.integration;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import com.github.sleroy.fakesmtp.core.ServerConfiguration;
import com.github.sleroy.fakesmtp.model.EmailModel;
import com.github.sleroy.junit.mail.server.test.FakeSmtpRule;

import pl.pw.rb.eng.tests.IntegrationTest;

@RunWith(SpringRunner.class)
@Category({ IntegrationTest.class })
@AutoConfigureDataJpa
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class UserRegistrationPipelineTest {

	private String login = "fakeLogin";

	private String password = "password";

	private String email = "fake@mail.com";

	private JSONObject form = new JSONObject() //
			.put("login", login) //
			.put("password", password) //
			.put("email", email);

	@Autowired
	MockMvc mvc;

	@Rule
	public FakeSmtpRule smtp = new FakeSmtpRule(ServerConfiguration.create() //
			.bind("localhost") //
			.port(25) //
			.relayDomains("mail.com") //
			.charset(StandardCharsets.UTF_8.name()));

	@Before
	public void before() {
		assertTrue(smtp.isRunning());
		smtp.mailBox().clear();
	}

	@Test
	@Transactional
	public void testConfirmed() throws Exception {
		mvc.perform(post("/register").content(form.toString(0)).contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isAccepted());

		assertFalse(smtp.mailBox().isEmpty());		
		EmailModel registrationConfirmEmail = smtp.mailBox().get(0);

		assertTrue(registrationConfirmEmail.getEmailStr().contains("uuid="));

		Pattern p = Pattern.compile("uuid=(?<uuid>[a-zA-Z0-9-]+)");
		Matcher matcher = p.matcher(registrationConfirmEmail.getEmailStr());
		assertTrue(matcher.find());
		mvc.perform(post("/register/confirm").param("uuid", matcher.group("uuid"))).andExpect(status().isOk());

		mvc.perform(post("/login").param("username", login).param("password", password)).andExpect(status().isOk());
	}

	@Test
	@Transactional
	public void testUnconfirmed() throws Exception {
		mvc.perform(post("/register").content(form.toString(0)).contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isAccepted());

		assertFalse(smtp.mailBox().isEmpty());
		EmailModel registrationConfirmEmail = smtp.mailBox().get(0);

		assertTrue(registrationConfirmEmail.getEmailStr().contains("uuid="));

		Pattern p = Pattern.compile("uuid=(?<uuid>[a-zA-Z0-9-]+)");
		Matcher matcher = p.matcher(registrationConfirmEmail.getEmailStr());
		assertTrue(matcher.find());
		// no e-mail confirmation

		mvc.perform(post("/login").param("username", login).param("password", password)).andExpect(status().is4xxClientError());
	}
	
	@Test
	@Transactional
	public void testDoubleConfirmation() throws Exception {
		mvc.perform(post("/register").content(form.toString(0)).contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isAccepted());

		assertFalse(smtp.mailBox().isEmpty());
		EmailModel registrationConfirmEmail = smtp.mailBox().get(0);

		assertTrue(registrationConfirmEmail.getEmailStr().contains("uuid="));

		Pattern p = Pattern.compile("uuid=(?<uuid>[a-zA-Z0-9-]+)");
		Matcher matcher = p.matcher(registrationConfirmEmail.getEmailStr());
		assertTrue(matcher.find());
		mvc.perform(post("/register/confirm").param("uuid", matcher.group("uuid"))).andExpect(status().isOk());
		mvc.perform(post("/register/confirm").param("uuid", matcher.group("uuid"))).andExpect(status().is(HttpStatus.BAD_REQUEST.value()));
	}

}
