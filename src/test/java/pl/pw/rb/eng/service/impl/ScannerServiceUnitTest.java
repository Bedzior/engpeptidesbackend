package pl.pw.rb.eng.service.impl;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.test.annotation.Repeat;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;

import mscanlib.ms.mass.MassTools;
import mscanlib.ms.msms.dbengines.DbEngine;
import pl.pw.rb.eng.db.dao.ScanResultDAO;
import pl.pw.rb.eng.db.dao.ScansDAO;
import pl.pw.rb.eng.db.entities.Database;
import pl.pw.rb.eng.db.entities.Scan;
import pl.pw.rb.eng.db.entities.ScanStatus;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.scan.ScannerTask;
import pl.pw.rb.eng.scan.ScannerTaskFactory;
import pl.pw.rb.eng.scan.exception.DbConfigurationException;
import pl.pw.rb.eng.scan.exception.NoSuchOngoingScanException;
import pl.pw.rb.eng.service.DatabaseService;
import pl.pw.rb.eng.service.ScannerService;
import pl.pw.rb.eng.service.UploadFileService;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.service.exception.NullUserException;
import pl.pw.rb.eng.springconfig.PeptidesConfiguration;
import pl.pw.rb.eng.tests.UnitTest;
import pl.pw.rb.eng.web.servlet.dto.scan.MassUnit;
import pl.pw.rb.eng.web.servlet.dto.scan.ScanConfiguration;
import pl.pw.rb.eng.web.servlet.dto.scan.ScanConfiguration.BioConfiguration;
import pl.pw.rb.eng.web.servlet.dto.scan.ScanConfiguration.DataConfiguration;
import pl.pw.rb.eng.web.servlet.dto.scan.ScanConfiguration.ModificationsConfiguration;
import pl.pw.rb.eng.web.servlet.dto.scan.ScanConfiguration.OtherConfiguration;

@RunWith(SpringRunner.class)
@Category({ UnitTest.class })
@ActiveProfiles("test")
public class ScannerServiceUnitTest {

	@Rule
	public TemporaryFolder temp = new TemporaryFolder();

	@TestConfiguration
	public static class Configuration extends PeptidesConfiguration {

		@Bean
		public ScannerService scannerService() {
			return new ScannerServiceImpl();
		}

		@Bean
		public ObjectMapper mapper() {
			return new ObjectMapper();
		}
	}

	@Autowired
	ScannerService service;

	@MockBean
	private ScansDAO dao;

	@MockBean
	private UsersService usersService;

	@MockBean
	private DatabaseService dbService;

	@MockBean
	private UploadFileService uploadFileService;

	@MockBean
	private ScanResultDAO resultFileDAO;

	@MockBean
	private SimpMessagingTemplate template;

	@MockBean
	private ScannerTaskFactory factory;

	@Mock
	private User mockUser;

	@Mock
	private Database mockDB;

	@Mock
	private Scan scan;

	@Mock
	ScanConfiguration configuration;

	@Mock
	DataConfiguration dataConfig;

	@Mock
	OtherConfiguration otherConfig;

	@Mock
	BioConfiguration bioConfig;

	@Mock
	ModificationsConfiguration modConfig;

	@Mock
	ScannerTask mockTask;

	@BeforeClass
	public static void beforeClass() throws Exception {
		MassTools.initMaps();
	}

	@Before
	public void before() throws Exception {
		when(usersService.getByID(anyInt())).thenReturn(mockUser);
		when(dbService.getDatabase(anyString())).thenReturn(mockDB);
		when(dbService.getDatabase(anyInt())).thenReturn(mockDB);
		when(dao.findOne(anyString())).thenReturn(scan);
		when(dao.save((Scan) anyObject())).thenReturn(scan);
		when(scan.getId()).thenReturn(UUID.randomUUID().toString());
		when(mockTask.getEngine()).thenReturn(mock(DbEngine.class));
		when(factory.createTask(anyObject(), anyString(), anyObject(), anyObject())).thenReturn(mockTask);
		when(configuration.getData()).thenReturn(dataConfig);
		when(configuration.getOther()).thenReturn(otherConfig);
		when(configuration.getBio()).thenReturn(bioConfig);
		when(bioConfig.getPeptideCharge()).thenReturn("1,2");
		when(configuration.getModifications()).thenReturn(modConfig);
		when(bioConfig.getPeptideToleranceUnits()).thenReturn(MassUnit.PARTS_PER_MILLION);
		when(bioConfig.getTandemMassToleranceUnits()).thenReturn(MassUnit.DALTON);
		when(otherConfig.getFiles()).thenReturn(new String[] { temp.newFile().getAbsolutePath() });
		when(otherConfig.getInstrument()).thenReturn("Default");
	}

	@Test(expected = DbConfigurationException.class)
	public void testNullConfig() throws Exception {
		service.createFor(mockUser, RandomStringUtils.random(10), null);
	}

	@Test(expected = NullUserException.class)
	public void testNullUser() throws Exception {
		service.createFor(null, RandomStringUtils.random(10), mock(ScanConfiguration.class));
	}

	@Test
	public void testCreation() throws Exception {
		final String name = RandomStringUtils.randomAlphanumeric(20);
		when(mockDB.getName()).thenReturn(name);
		when(uploadFileService.getFile(eq(mockUser), anyString())).thenReturn(temp.newFile());
		Scan scan = service.createFor(mockUser, RandomStringUtils.random(10), configuration);
		assertNotNull(scan);
		assertNotNull(service.getEngine(scan.getId()));
	}

	@Test
	@Repeat(10)
	public void testCancel() throws Exception {
		doAnswer(i -> {
			wait(100000);
			return null;
		}).when(mockTask).run();
		when(uploadFileService.getFile(eq(mockUser), anyString())).thenReturn(temp.newFile());
		Scan scan = service.createFor(mockUser, RandomStringUtils.random(10), configuration);
		assertNotNull(scan);
		assertNotNull(service.getEngine(scan.getId()));
		service.stopTask(scan.getId());
		assertNull(service.getEngine(scan.getId()));
	}

	@Test
	public void testGetNullEngine() {
		assertNull(service.getEngine(RandomStringUtils.random(10)));
	}

	@Test(expected = NoSuchOngoingScanException.class)
	public void testStopNoOngoingTask() throws Exception {
		service.stopTask(RandomStringUtils.random(10));
	}

	@Test
	public void testGetFinishedForUser() throws Exception {
		List<Scan> list = Arrays.asList(mock(Scan.class));
		when(dao.findByUserAndStatusIn(eq(mockUser), (Collection<ScanStatus>) argThat(hasItems(ScanStatus.FINISHED, ScanStatus.ABORTED, ScanStatus.FAILED))))
				.thenReturn(list);
		Collection<Scan> scans = service.getForUser(mockUser, true);
		assertNotNull(scans);
		assertEquals(list.size(), scans.size());
		assertEquals(0, service.getForUser(mockUser, false).size());
	}

	@Test
	public void testGetUnfinishedForUser() throws Exception {
		List<Scan> list = Arrays.asList(mock(Scan.class), mock(Scan.class));
		when(dao.findByUserAndStatusIn(eq(mockUser), (Collection<ScanStatus>) argThat(hasItems(ScanStatus.WAITING, ScanStatus.RUNNING)))).thenReturn(list);
		Collection<Scan> scans = service.getForUser(mockUser, false);
		assertNotNull(scans);
		assertEquals(list.size(), scans.size());
		assertEquals(0, service.getForUser(mockUser, true).size());
	}

	@Test
	public void testGet() throws Exception {
		String token = RandomStringUtils.randomAlphabetic(20);
		Scan mockScan = mock(Scan.class);
		when(dao.findOne(eq(token))).thenReturn(mockScan);
		Scan scan = service.get(token);
		assertEquals(mockScan, scan);
	}

	@Test
	public void testComparator() throws Exception {
		ScannerTask mockTask1 = mock(ScannerTask.class);
		ScannerTask mockTask2 = mock(ScannerTask.class);
		Comparator<Runnable> comparator = service.getTaskComparator();
		assertEquals(ScannerTask.EQUAL, comparator.compare(null, null));
		assertEquals(ScannerTask.FIRST, comparator.compare(mockTask1, null));
		assertEquals(ScannerTask.SECOND, comparator.compare(null, mockTask2));

		String token1 = RandomStringUtils.randomAlphabetic(10);
		String token2 = RandomStringUtils.randomAlphabetic(10);
		when(mockTask1.getToken()).thenReturn(token1);
		when(mockTask2.getToken()).thenReturn(token2);

		when(dao.findOne(eq(token1))).thenReturn(null);
		when(dao.findOne(eq(token2))).thenReturn(null);
		assertEquals(ScannerTask.EQUAL, comparator.compare(mockTask1, mockTask2));
		Scan mockScan1 = mock(Scan.class);
		Scan mockScan2 = mock(Scan.class);
		when(dao.findOne(eq(token1))).thenReturn(mockScan1);
		assertEquals(ScannerTask.FIRST, comparator.compare(mockTask1, mockTask2));
		when(dao.findOne(eq(token1))).thenReturn(null);
		when(dao.findOne(eq(token2))).thenReturn(mockScan2);
		assertEquals(ScannerTask.SECOND, comparator.compare(mockTask1, mockTask2));

		Date sameDate = new Date();
		when(mockScan1.getCreated()).thenReturn(sameDate);
		when(mockScan2.getCreated()).thenReturn(sameDate);
		when(dao.findOne(eq(token1))).thenReturn(mockScan1);
		when(dao.findOne(eq(token2))).thenReturn(mockScan2);
		assertEquals(ScannerTask.EQUAL, comparator.compare(mockTask1, mockTask2));
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(sameDate);
		calendar.add(Calendar.SECOND, -1);
		when(mockScan1.getCreated()).thenReturn(calendar.getTime());
		assertEquals(ScannerTask.SECOND, comparator.compare(mockTask1, mockTask2));
	}

}
