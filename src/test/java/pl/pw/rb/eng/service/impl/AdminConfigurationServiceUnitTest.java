package pl.pw.rb.eng.service.impl;

import static java.text.MessageFormat.format;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import org.junit.ClassRule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import pl.pw.rb.eng.service.AdminConfigurationService;
import pl.pw.rb.eng.service.exception.MissingConfigurationValueException;
import pl.pw.rb.eng.springconfig.PeptidesConfiguration;
import pl.pw.rb.eng.tests.UnitTest;
import pl.pw.rb.eng.web.dto.Configuration;
import pl.pw.rb.eng.web.servlet.admin.exception.NoSuchConfigurationException;

@RunWith(SpringRunner.class)
@Category({ UnitTest.class })
@ActiveProfiles({ "admin-config", "test" })
@TestPropertySource("/application-admin-config.properties")
public class AdminConfigurationServiceUnitTest {

	@ClassRule
	public static TemporaryFolder temp = new TemporaryFolder();

	@org.springframework.context.annotation.Configuration
	public static class TestConfiguration extends PeptidesConfiguration {

		@Bean
		public AdminConfigurationServiceImpl adminConfigService() {
			return new AdminConfigurationServiceImpl();
		}

		@Bean
		@Override
		public File appDir() {
			try {
				return temp.newFolder("peptides");
			} catch (IOException e) {
				throw new RuntimeException("Could not create folder for testing");
			}
		}

	}

	@Autowired
	File appDir;

	@Autowired
	File confDir;

	@Autowired
	AdminConfigurationService service;

	@Test
	public void testList() throws Exception {
		Collection<Configuration> configurations = service.list();
		assertFalse(configurations.isEmpty());
	}

	@Test(expected = NoSuchConfigurationException.class)
	public void testGetNull() throws Exception {
		service.get(null);
	}

	@Test(expected = NoSuchConfigurationException.class)
	public void testUpdateNullObject() throws Exception {
		service.update((Configuration) null);
	}

	@Test(expected = NoSuchConfigurationException.class)
	public void testUpdateNull() throws Exception {
		service.update(null, null);
	}

	@Test(expected = MissingConfigurationValueException.class)
	public void testUpdateNullValue() throws Exception {
		service.update("abc", null);
	}

	@Test
	public void testGet() throws Exception {
		Configuration abc = service.get("abc");
		assertEquals("abc", abc.getName());
		assertEquals(123, abc.getValue());
		assertEquals("Special integer value", abc.getDescription());
		assertThatThrownBy(() -> {
			service.get("def");
		}).isInstanceOf(NoSuchConfigurationException.class);
		assertThatThrownBy(() -> {
			service.get("unused-setting");
		}).isInstanceOf(NoSuchConfigurationException.class);
	}

	@Test
	@WithMockUser(username = "admin")
	public void testUpdateWithCollection() throws Exception {
		List<Configuration> configs = new ArrayList<>();
		for (int i = 0; i < 5; i++)
			configs.add(new Configuration(format("name{0}", i + 1), format("desc{0}", i + 1), randomAlphabetic(nextInt(0, 100))));
		configs.add(new Configuration("peptides.abc", "Full description", randomAlphabetic(nextInt(0, 100))));
		service.update(configs);
	}

	@Test
	@WithMockUser(username = "admin")
	public void testUpdateConfiguration() throws Exception {
		Configuration config = service.get("abc");
		String newValue = "434";
		config.setValue(newValue);
		service.update(config);
		Properties p = loadProperties();
		assertEquals(newValue, p.getProperty("peptides.abc"));
	}

	private Properties loadProperties() throws IOException, FileNotFoundException {
		File[] configFiles = confDir.listFiles((FilenameFilter) (dir, name) -> name.endsWith(".properties"));
		assertNotEquals(0, configFiles.length);
		Properties p = new Properties();
		try (FileInputStream fis = new FileInputStream(configFiles[0])) {
			p.load(fis);
		}
		return p;
	}

	@Test
	@WithMockUser(username = "admin")
	public void testUpdateStringObject() throws Exception {
		String name = "abc";
		Object newValue = "453";
		Configuration update = service.update(name, newValue);
		assertEquals(name, update.getName());
		assertEquals(newValue, update.getValue());
		Properties p = loadProperties();
		assertEquals(newValue, p.getProperty("peptides.abc"));
	}

}
