package pl.pw.rb.eng.service.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Map;
import java.util.UUID;

import javax.mail.MessagingException;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StreamUtils;

import com.google.common.collect.ImmutableMap;

import pl.pw.rb.eng.db.entities.EmailTemplate;
import pl.pw.rb.eng.db.entities.ScanResult;
import pl.pw.rb.eng.service.MailService;
import pl.pw.rb.eng.service.TemplateService;
import pl.pw.rb.eng.service.exception.MailTemplateException;
import pl.pw.rb.eng.springconfig.PeptidesConfiguration;
import pl.pw.rb.eng.tests.UnitTest;

@RunWith(SpringRunner.class)
@Category({ UnitTest.class })
public class MailServiceOfflineTest {

	@ClassRule
	public static TemporaryFolder temp = new TemporaryFolder();

	@TestConfiguration
	public static class Configuration extends PeptidesConfiguration {

		@Bean
		public MailService service() {
			return new MailServiceImpl();
		}
	}

	@Autowired
	MailService service;

	@MockBean
	JavaMailSender sender;

	@MockBean
	TemplateService templateService;

	@Test(expected = MailTemplateException.class)
	public void testPrepareNoTemplate() {
		service.prepareTemplate(RandomStringUtils.random(10, true, true), "abc", ImmutableMap.of());
	}

	@Test
	public void testPrepareTemplate() throws Exception {
		final PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		when(templateService.getTemplate(anyString(), anyString())).thenAnswer(invocation -> {
			String name = invocation.getArgumentAt(0, String.class);
			String type = invocation.getArgumentAt(1, String.class);
			EmailTemplate template = new EmailTemplate(name, type);

			try (InputStream is = resolver.getResource("/templates/" + name + "." + type).getInputStream()) {
				template.setBody(StreamUtils.copyToString(is, StandardCharsets.UTF_8));
			}
			return template;
		});
		for (Resource resource : resolver.getResources("/templates/*")) {
			final String[] split = resource.getFilename().split("\\.");
			String name = split[0], type = split[1];
			Map<String, String> values = ImmutableMap.<String, String>of("uuid", UUID.randomUUID().toString());
			String preparedTemplate = service.prepareTemplate(name, type, values);
			assertTrue(preparedTemplate.contains(values.get("uuid")));
		}
	}

	@Test(expected = MessagingException.class)
	public void testSendInvalidRecipient() throws Exception {
		service.send("aabc.com", "someSubject", "content");
	}

	@Test
	public void testSendWithoutFiles() throws Exception {
		service.send("a@abc.com", "someSubject", "content");
		verify(sender).send((MimeMessagePreparator) anyObject());
	}

	@Test
	public void testSendWithFiles() throws Exception {
		service.send("a@abc.com", "someSubject", "content", Collections.singletonList(mock(ScanResult.class)));
		verify(sender).send((MimeMessagePreparator) anyObject());
	}

}
