package pl.pw.rb.eng.tests;

import org.junit.experimental.categories.Category;

/**
 * Used for marking tests employing external systems (JUnit's
 * <code>{@link Category}</code>)
 * 
 * @author ravcik@gmail.com
 */
public interface ExternalSystemsTest {

}
