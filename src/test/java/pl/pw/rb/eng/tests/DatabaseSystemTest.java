package pl.pw.rb.eng.tests;

/**
 * Used for grouping tests <br/>
 * <br/>
 * Indicates the test is run with the intent of testing a database system.
 * 
 * @author Rafał Będźkowski
 *
 */
public interface DatabaseSystemTest {

}
