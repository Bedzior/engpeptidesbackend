package pl.pw.rb.eng.db.dao;

import java.io.Serializable;
import java.util.Collection;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import pl.pw.rb.eng.tests.DatabaseSystemTest;
import pl.pw.rb.eng.tests.UnitTest;

@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles(profiles = { "test", "sqlite" })
@ContextConfiguration
@Category({ DatabaseSystemTest.class, UnitTest.class })
public class SQLiteTest {

	@Autowired
	Collection<CrudRepository<?, ? extends Serializable>> repositories;

	@Test
	public void test() {
		for (CrudRepository<?, ? extends Serializable> repo : repositories) {
			repo.findAll();
			repo.count();
		}
	}

}
