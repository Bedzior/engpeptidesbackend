package pl.pw.rb.eng.scan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import pl.pw.rb.eng.db.dao.ScansDAO;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.service.MailService;
import pl.pw.rb.eng.service.ScanResultService;
import pl.pw.rb.eng.service.ScannerService;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.web.servlet.dto.scan.JSONDbEngineScanConfig;

/**
 * Factory component responsible for constructing peptide scan tasks and
 * supplying them with other components' references.<br/>
 * <br/>
 * 
 * @author Rafał Będźkowski
 *
 */
@Component
public class ScannerTaskFactory {

	private ScannerTaskFactory() {
		super();
	}

	@Autowired
	private MailService mailService;

	@Autowired
	private ScanResultService resultFileService;

	@Autowired
	private UsersService userService;

	@Autowired
	private ScannerService scanService;

	@Autowired
	private ScansDAO dao;

	/**
	 * Factory method for returning a default ScannerTask. <br/>
	 * <b>NOTE</b> this way testing tasks' interplay with the scheduling service is
	 * much easier
	 * 
	 * @param user     {@link User} starting the task
	 * @param token    task's token
	 * @param config
	 * @param template
	 * @return
	 */
	public ScannerTask createTask(User user, String token, JSONDbEngineScanConfig config, SimpMessagingTemplate template) {
		ScannerTask task = new ScannerTask(user, token, config, template);
		task.addListener(new ScanTaskStatusListener(dao, token));
		task.addListener(new ScannerProgressEndpointListener(token, template));
		task.addListener(
				new ScanUserCommunicationHandler(dao, resultFileService, mailService, userService, scanService, config.getOutputFiles(), token, user.getId()));
		return task;
	}

}
