package pl.pw.rb.eng.scan;

import static java.text.MessageFormat.format;

import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;

import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.context.SecurityContextHolder;

import apps.mscandb.MScanDb;
import mscanlib.ms.msms.dbengines.DbEngine;
import mscanlib.ms.msms.dbengines.DbEngineListener;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.scan.status.ScanStatusMessage;
import pl.pw.rb.eng.web.servlet.dto.scan.JSONDbEngineScanConfig;

/**
 * This <code>class</code> represents a single instance of a once-configured
 * peptide scan task. <br/>
 * <br/>
 * Effectively both a runnable and a completable future to maintain proper
 * back-propagation and compatibility with task-based approach.
 * 
 * @author Rafał Będźkowski
 *
 */
public class ScannerTask extends CompletableFuture<Void> implements Runnable {

	/**
	 * priorities equal
	 */
	public static final int EQUAL = 0;

	/**
	 * second with higher priority
	 */
	public static final int SECOND = -1;

	/**
	 * first with higher priority
	 */
	public static final int FIRST = 1;

	private final Logger logger;

	private final DbEngine engine;

	private final String token;

	private final String username;

	private Thread thread;

	public ScannerTask(@NotNull User user, String scanID, JSONDbEngineScanConfig configuration, SimpMessagingTemplate template) {
		this.engine = new MScanDb(configuration.getInputFilenames(), configuration);
		this.token = scanID;
		this.username = user.getLogin();
		this.logger = Logger.getLogger(format("ScannerTask#{0}", token));
	}

	@Override
	public void run() {
		this.thread = Thread.currentThread();
		try {
			logger.info(format("Starting task {0} by {1}", token, username));
			engine.notifyInitialized();
			engine.start();
			complete(null);
		} catch (Exception e) {
			logger.fatal(format("Unexpected fatal exception during peptide scan {0}", token), e);
			completeExceptionally(e);
		} catch (Throwable t) {
			logger.fatal("Something went completely wrong");
			completeExceptionally(t);
		}
	}

	@Override
	@SuppressWarnings("deprecation")
	public boolean cancel(boolean mayInterruptIfRunning) {
		synchronized (thread) {
			thread.stop(); // sorry
		}
		String cancellingUser = null;
		try {
			cancellingUser = SecurityContextHolder.getContext().getAuthentication().getName();
		} catch (final Exception e) { // any exception occurring with security context
			cancellingUser = "unknown";
		}
		engine.notifyUpdated(new ScanStatusMessage(format("User {0} canceled", cancellingUser), true));
		this.completeExceptionally(new CancellationException(format("User {0} canceled scan {1}", cancellingUser, token)));
		engine.notifyFinished();
		return super.cancel(mayInterruptIfRunning);
	}

	@Override
	public CompletableFuture<Void> exceptionally(Function<Throwable, ? extends Void> fn) {
		return super.exceptionally(fn);
	}

	public DbEngine getEngine() {
		return this.engine;
	}

	public void addListener(DbEngineListener listener) {
		this.engine.addDbEngineListener(listener);
	}

	public String getToken() {
		return token;
	}
}
