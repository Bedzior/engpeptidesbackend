package pl.pw.rb.eng.scan.db;

import java.io.File;
import java.io.FileFilter;

import org.apache.commons.io.monitor.FileAlterationObserver;

public class DatabaseFileAlterationObserver extends FileAlterationObserver {

	private static final long serialVersionUID = -1961409839296834400L;

	public DatabaseFileAlterationObserver(File dl) {
		super(dl.getParentFile(), (FileFilter) pathname -> pathname.equals(dl));
	}

}
