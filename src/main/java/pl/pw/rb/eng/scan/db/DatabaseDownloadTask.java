package pl.pw.rb.eng.scan.db;

import static java.text.MessageFormat.format;
import static org.apache.commons.io.FileUtils.byteCountToDisplaySize;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.apache.log4j.Logger;
import org.rauschig.jarchivelib.Archiver;
import org.rauschig.jarchivelib.ArchiverFactory;
import org.rauschig.jarchivelib.Compressor;
import org.rauschig.jarchivelib.CompressorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import pl.pw.rb.eng.db.entities.Database;
import pl.pw.rb.eng.service.DatabaseService;
import pl.pw.rb.eng.web.servlet.exception.db.DatabaseCancelledException;
import pl.pw.rb.eng.web.servlet.exception.db.DatabaseDownloadException;

public class DatabaseDownloadTask extends CompletableFuture<File> implements Runnable {

	private final Logger log4j;

	private final DatabaseDownloadEndpointListener listener;

	private final String url;

	private final File tempDir;

	private FileAlterationMonitor monitor;

	public DatabaseDownloadTask(File tempDir, SimpMessagingTemplate template, Database config) {
		this(tempDir, template, config.getName(), config.getUrl());
	}

	public DatabaseDownloadTask(File tempDir, SimpMessagingTemplate template, String name, String url) {
		this.listener = new DatabaseDownloadEndpointListener(name, template);
		this.tempDir = tempDir;
		this.url = url;
		this.log4j = Logger.getLogger(format("DatabaseDownload#{0}", Integer.toHexString(url.hashCode()).toUpperCase()));
	}

	@Autowired
	public void setMonitor(FileAlterationMonitor monitor) {
		this.monitor = monitor;
	}

	@Override
	public void run() {
		FileAlterationObserver observer = null;
		try {
			final String resourceName = url.substring(url.lastIndexOf('/') + 1);
			File dl = new File(tempDir, System.currentTimeMillis() + "_" + resourceName);
			dl.createNewFile();
			final URL urlObject = new URL(url);
			long size = -1;
			URLConnection connection = urlObject.openConnection();
			connection.setReadTimeout((int) TimeUnit.MILLISECONDS.convert(5, TimeUnit.SECONDS));
			connection.setConnectTimeout((int) TimeUnit.MILLISECONDS.convert(5, TimeUnit.SECONDS));
			try {
				if (connection instanceof HttpURLConnection) {
					((HttpURLConnection) connection).setRequestMethod("HEAD");
					try (InputStream is = connection.getInputStream()) {
						size = connection.getHeaderFieldInt("Content-Length", -1);
					}
				} else {
					size = connection.getContentLengthLong();
				}
			} catch (final IOException e) {
				log4j.warn(format("Could not retrieve file size from {0}", url));
			} finally {
				if (size > 0)
					log4j.info(format("Downloading database of size {0} from {1}", FileUtils.byteCountToDisplaySize(size), url));
				else
					log4j.info(format("Downloading database from {0} (no size data could be retrieved from remote host)", url));
				log4j.info(format("Download destination: {0}", dl.getAbsolutePath()));
			}
			observer = new DatabaseFileAlterationObserver(dl);
			listener.setSize(size);
			listener.onProgress(0);
			observer.addListener((FileAlterationListener) listener);
			try {
				monitor.addObserver(observer);
			} catch (Exception e) {
				log4j.error("Error starting the file alteration monitor", e);
			}
			long transferred;
			try (ReadableByteChannel channel = Channels.newChannel(connection.getInputStream()); FileOutputStream fos = new FileOutputStream(dl)) {
				transferred = fos.getChannel().transferFrom(channel, 0, size == -1L ? Long.MAX_VALUE : size);
			}
			if (isCancelled()) {
				int progress = (int) Math.floor(100 * transferred / size);
				throw new DatabaseCancelledException(format("{0} download cancelled at {1,number,#}% ({2} out of {3})", resourceName, progress,
						byteCountToDisplaySize(transferred), byteCountToDisplaySize(size)));
			} else if (size > 0 && transferred != size) {
				int progress = (int) Math.floor(100 * transferred / size);
				throw new DatabaseDownloadException(format("{0} failed to download at {1,number,#}% ({2} out of {3})", resourceName, progress,
						byteCountToDisplaySize(transferred), byteCountToDisplaySize(size)));
			}
			listener.setSize(transferred);
			listener.onProgress(100);
			String fileName = dl.getName();
			try {
				if (fileName.endsWith(".gz") && !fileName.endsWith(".tar.gz")) {
					Compressor c = CompressorFactory.createCompressor(dl);
					c.decompress(dl, tempDir);
				} else {
					final Archiver a = ArchiverFactory.createArchiver(dl);
					a.extract(dl, tempDir);
				}
				dl.delete();
			} catch (final IOException e) {
				log4j.error("Registering downloaded file as an explicit database", e);
				listener.onError(e.getMessage(), null);
				FileUtils.moveFile(dl, new File(tempDir, resourceName));
			}
			// Retrieve the largest file in the directory now (presumably that's
			// our protein database)
			this.complete(DatabaseService.getLargestFileFrom(tempDir));
		} catch (final MalformedURLException e) {
			onException(e, "ERR_BAD_URL");
		} catch (final SocketTimeoutException e) {
			onException(e, "Error connecting to specified endpoint", "ERR_INACCESSIBLE");
		} catch (final IOException e) {
			onException(e, "Error saving downloaded database", "ERR_IO");
		} catch (final Exception e) {
			onException(e, "Unexpected exception during database download", "ERR_UNEXPECTED");
		} finally {
			if (observer != null)
				monitor.removeObserver(observer);
		}

	}

	private void onException(Exception e, String token) {
		listener.onError(e.getMessage(), token);
		this.completeExceptionally(e);
	}

	private void onException(Exception e, String message, String token) {
		listener.onError(message, token);
		this.completeExceptionally(e);
	}

	public DatabaseDownloadEndpointListener getListener() {
		return this.listener;
	}

}
