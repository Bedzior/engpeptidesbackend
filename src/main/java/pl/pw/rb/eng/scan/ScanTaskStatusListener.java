package pl.pw.rb.eng.scan;

import mscanlib.ms.msms.dbengines.DbEngine;
import pl.pw.rb.eng.db.dao.ScansDAO;
import pl.pw.rb.eng.db.entities.Scan;
import pl.pw.rb.eng.db.entities.ScanStatus;
import pl.pw.rb.eng.scan.status.ScanStatusMessage;

/**
 * This listener exists to update an on-going scan's database status
 * 
 * @category <i>separation of concerns</i>, <i>statefulness</i>
 * 
 * @author Rafał Będźkowski
 *
 */
final class ScanTaskStatusListener extends AbstractDbEngineListener {

	private final String token;

	private final ScansDAO dao;

	public ScanTaskStatusListener(final ScansDAO dao, final String token) {
		this.dao = dao;
		this.token = token;
	}

	@Override
	public void onError(DbEngine engine, String message) {
		Scan scan = dao.findOne(token);
		if (scan != null) {
			scan.setStatus(ScanStatus.FAILED);
			dao.save(scan);
		} else
			throw new RuntimeException("Missing scan");
	}

	@Override
	public void onFinished(DbEngine arg0) {
		Scan scan = dao.findOne(token);
		scan.setStatus(ScanStatus.FINISHED);
		dao.save(scan);
	}

	@Override
	public void onInitialized(DbEngine engine) {
		Scan scan = dao.findOne(token);
		scan.setStatus(ScanStatus.RUNNING);
		dao.save(scan);
	}

	@Override
	protected void onCustomUpdated(DbEngine engine, ScanStatusMessage message) {
		if (message.aborted) {
			Scan scan = dao.findOne(token);
			if (scan != null) {
				scan.setStatus(ScanStatus.ABORTED);
				dao.save(scan);
			}
		}
	}
}
