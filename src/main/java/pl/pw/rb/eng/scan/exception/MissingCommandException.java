package pl.pw.rb.eng.scan.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "No such ongoing scan")
public class MissingCommandException extends Exception {

	private static final long serialVersionUID = -8622021951727889721L;

	public MissingCommandException() {
		super();
	}

}
