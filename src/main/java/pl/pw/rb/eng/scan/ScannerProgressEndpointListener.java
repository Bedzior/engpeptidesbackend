package pl.pw.rb.eng.scan;

import static java.text.MessageFormat.format;

import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import mscanlib.ms.msms.dbengines.DbEngine;
import pl.pw.rb.eng.db.dto.Message;
import pl.pw.rb.eng.scan.status.ScanStatusMessage;

/**
 * This listener exists to supply frontend users (or anybody listening to
 * websocket messages to that matter) with updates from an ongoing
 * {@link DbEngine} scan. <br/>
 * <br/>
 * 
 * @category <i>separation of concerns</i>, <i>websocket</i>
 * @author Rafał Będźkowski
 *
 */
public final class ScannerProgressEndpointListener extends AbstractDbEngineListener {

	public static final String TOPIC_FORMAT = "/topic/scan/{0}";

	private static final Logger log4j = Logger.getLogger(ScannerProgressEndpointListener.class);

	private SimpMessagingTemplate template;

	private boolean initialised = false;

	private final String scanID;

	private final String destination;

	public ScannerProgressEndpointListener(@NotNull String scanID, @NotNull SimpMessagingTemplate template) {
		this.scanID = scanID;
		this.destination = format(TOPIC_FORMAT, scanID);
		this.template = template;
		this.init();
	}

	private void init() {
		this.template.convertAndSend(destination, new Message("Initializing scan"));
		this.initialised = true;
	}

	private void proxyToSocket(boolean init, boolean finish, Object status) {
		this.proxyToSocket(init, finish, false, status);
	}

	private void proxyToSocket(boolean init, boolean finish, boolean abort, Object status) {
		if (status == null)
			this.template.convertAndSend(destination, new ScanProgressStatus(init, finish, abort, null));
		else if (status instanceof String)
			this.template.convertAndSend(destination, new ScanProgressStatus(init, finish, abort, ((String) status).trim()));
		else
			this.template.convertAndSend(destination, new ScanProgressStatus(init, finish, abort, status));
	}

	@Override
	protected void onUpdated(DbEngine engine, String status) {
		if (!this.initialised)
			this.init();
		if (status != null && status.matches("User (\\w*) canceled")) {
			this.proxyToSocket(false, false, true, status);
		} else {
			this.proxyToSocket(false, false, status);
		}
	}

	@Override
	protected void onCustomUpdated(DbEngine engine, ScanStatusMessage message) {
		this.proxyToSocket(false, false, message);
	}

	@Override
	protected void onInitialized(DbEngine engine) {
		if (!this.initialised)
			this.init();
		log4j.info(format("Engine search initialized for {0}", scanID));
		this.proxyToSocket(true, false, null);
	}

	@Override
	protected void onFinished(DbEngine engine) {
		if (!this.initialised)
			this.init();
		this.proxyToSocket(false, true, null);
	}

	@Override
	protected void onError(DbEngine engine, String status) {
		if (!this.initialised)
			this.init();
		this.proxyToSocket(false, true, status);

	}
}
