package pl.pw.rb.eng.scan.exception;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Erroneous scan configuration: database")
@TranslationToken("ERR_SCAN_DB")
public class DbConfigurationException extends ServletException {

	private static final long serialVersionUID = 5235349767135454121L;

	private final String[] fields;

	public DbConfigurationException(String message) {
		super(message);
		fields = null;
	}

	/**
	 * @return names of fields that were in error
	 */
	public String[] getFields() {
		return fields;
	}
}
