package pl.pw.rb.eng.scan.db;

import java.io.File;
import java.util.concurrent.CompletableFuture;

import pl.pw.rb.eng.service.exception.DatabaseAlreadyDownloadingException;
import pl.pw.rb.eng.service.exception.NoOngoingDatabaseDownload;

public interface DatabaseDownloader {

	/**
	 * schedules a download task
	 * 
	 * @param dbName name of the requested database to download, further used as an
	 *               identifier
	 * @param url    link to the database to download to server's local system
	 * @return a {@code CompletableFuture} of the result {@code File}
	 */
	CompletableFuture<File> requestDownload(String dbName, String url) throws DatabaseAlreadyDownloadingException;

	/**
	 * checks if there is an ongoing scan for specified database
	 * 
	 * @param dbName name of the database to check against
	 * @return whether the specified database is currently being downloaded
	 */
	boolean isDownloading(String dbName);

	/**
	 * cancels the downloading process of a specified database
	 * 
	 * @param dbName name of the database being cancelled
	 * @throws NoOngoingDatabaseDownload in case there is no such download in
	 *                                   progress
	 */
	void cancelDownloading(String dbName) throws NoOngoingDatabaseDownload;

	/**
	 * 
	 * @return <code>true</code> if any database is currently being downloaded
	 */
	boolean isDownloading();

}
