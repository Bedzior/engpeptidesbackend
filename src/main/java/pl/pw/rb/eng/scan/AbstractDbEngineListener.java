package pl.pw.rb.eng.scan;

import org.apache.log4j.Logger;

import mscanlib.ms.msms.dbengines.DbEngine;
import mscanlib.ms.msms.dbengines.DbEngineListener;
import pl.pw.rb.eng.scan.status.ScanStatusMessage;

/**
 * This default abstract listener class serves the following purposes:
 * <ul>
 * <li>limits the need to implement all listener functions by providing default
 * (empty) implementations;</li>
 * <li>spares the need to implement separate error handlers for unexpected
 * errors during listeners' execution;</li>
 * <li>rebrands some of the functions, in terms of spelling and parameter
 * typing.</li>
 * </ul>
 * 
 * @category abstract implementation
 * 
 * @author Rafał Będźkowski
 *
 */
public class AbstractDbEngineListener implements DbEngineListener {

	private final Logger logger = Logger.getLogger(getClass());

	@Override
	public final void notifyInitalized(DbEngine engine) {
		try {
			this.onInitialized(engine);
		} catch (Throwable t) {
			logger.error("Exception in notifyInitialized", t);
		}
	}

	@Override
	public final void notifyUpdated(DbEngine engine, Object message) {
		try {
			if (message instanceof String)
				this.onUpdated(engine, (String) message);
			else
				this.onCustomUpdated(engine, (ScanStatusMessage) message);
		} catch (Throwable t) {
			logger.error("Exception in notifyUpdated", t);
		}
	}

	@Override
	public final void notifyError(DbEngine engine, Object message) {
		try {
			if (message instanceof String)
				this.onError(engine, (String) message);
			else
				this.onCustomError(engine, (ScanStatusMessage) message);
		} catch (Throwable t) {
			logger.error("Exception in notifyError", t);
		}
	}

	protected void onCustomUpdated(DbEngine engine, ScanStatusMessage message) {
	}

	protected void onCustomError(DbEngine engine, ScanStatusMessage message) {
	}

	@Override
	public final void notifyFinished(DbEngine engine) {
		try {
			this.onFinished(engine);
		} catch (Throwable t) {
			logger.error("Exception in notifyFinished", t);
		}
	}

	protected void onInitialized(DbEngine engine) {
	}

	protected void onUpdated(DbEngine engine, String message) {
	}

	protected void onFinished(DbEngine engine) {
	}

	protected void onError(DbEngine engine, String message) {
	}
}
