package pl.pw.rb.eng.scan;

import static java.text.MessageFormat.format;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;

import pl.pw.rb.eng.scan.exception.MissingCommandException;
import pl.pw.rb.eng.scan.exception.NoSuchOngoingScanException;
import pl.pw.rb.eng.service.ScannerService;
import pl.pw.rb.eng.service.exception.NoSuchScanException;
import pl.pw.rb.eng.web.controller.dto.CommandMessage;
import pl.pw.rb.eng.web.controller.dto.StatusMessage;
import pl.pw.rb.eng.web.controller.dto.SuccessStatusMessage;
import pl.pw.rb.eng.web.controller.dto.TranslatableErrorMessage;

@Controller
public class ScannerProgressEndpointController {

	@Autowired
	private ScannerService service;

	@SubscribeMapping("/scan/{scanID}")
	void subscribed(@DestinationVariable String scanID) throws NoSuchOngoingScanException, NoSuchScanException {
		if (!service.isRunning(scanID))
			throw new NoSuchOngoingScanException(scanID);
	}

	@MessageMapping("/scan/{scanID}")
	@SendTo("/topic/scan/{scanID}")
	StatusMessage command(@Payload CommandMessage payload, @DestinationVariable String scanID) throws MissingCommandException {
		if (payload.getCommand() == null)
			throw new MissingCommandException();
		switch (payload.getCommand()) {
		case CANCEL:
			try {
				service.stopTask(scanID);
				return new SuccessStatusMessage(format("Scan {0} successfully stopped", scanID));
			} catch (NoSuchOngoingScanException e) {
				return new TranslatableErrorMessage(format("No such scan currently in progress: {0}", scanID), "ERR_NO_SCAN");
			} catch (SecurityException e) {
				return new TranslatableErrorMessage("User has no permission to this task", "ERR_NO_PERMISSION");
			}
		default:
			return new TranslatableErrorMessage(format("Unrecognized command: {0}", payload.getCommand()), "ERR_NO_COMMAND");
		}
	}

}
