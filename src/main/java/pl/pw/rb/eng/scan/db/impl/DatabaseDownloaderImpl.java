package pl.pw.rb.eng.scan.db.impl;

import static java.text.MessageFormat.format;

import java.io.File;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import pl.pw.rb.eng.scan.db.DatabaseDownloadEndpointListener;
import pl.pw.rb.eng.scan.db.DatabaseDownloadTask;
import pl.pw.rb.eng.scan.db.DatabaseDownloader;
import pl.pw.rb.eng.service.exception.DatabaseAlreadyDownloadingException;
import pl.pw.rb.eng.service.exception.NoOngoingDatabaseDownload;
import pl.pw.rb.eng.web.controller.dto.StatusMessage;
import pl.pw.rb.eng.web.controller.dto.SuccessStatusMessage;

/**
 * Handles FASTA databased download from external endpoints.
 * 
 * @author Rafał Będźkowski
 *
 */
@Service
class DatabaseDownloaderImpl implements DatabaseDownloader {

	private Logger log4j = Logger.getLogger(getClass());

	private final FileAlterationMonitor monitor;

	private final ThreadPoolExecutor executor;

	private Map<String, DatabaseDownloadEndpointListener> listeners = new ConcurrentHashMap<>(0);

	private Map<String, Future<?>> ongoingDownloads = new ConcurrentHashMap<>(0);

	private final Object synchLock = new Object();

	@Autowired
	private SimpMessagingTemplate template;

	@Autowired
	private File tempDir;

	private boolean monitorStopped = true;

	@Autowired
	DatabaseDownloaderImpl(FileAlterationMonitor monitor) {
		this.monitor = monitor;
		executor = new ThreadPoolExecutor(1, 5, 30, TimeUnit.SECONDS, new ArrayBlockingQueue<>(5),
				new ThreadFactoryBuilder().setDaemon(true).setNameFormat("DatabaseDownload_%d").setUncaughtExceptionHandler((t, e) -> {
					log4j.fatal("Uncaught exception from database download task", e);
				}).build());
	}

	@Override
	/**
	 * 
	 * @param dbName
	 * @throws NoOngoingDatabaseDownload
	 */
	public void cancelDownloading(String dbName) throws NoOngoingDatabaseDownload {
		if (ongoingDownloads.containsKey(dbName)) {
			Future<?> submitted = ongoingDownloads.get(dbName);
			synchronized (submitted) {
				submitted.cancel(true);
				StatusMessage payload = new SuccessStatusMessage(format("Download {0} successfully stopped", dbName));
				template.convertAndSend("/topic/db_upload/" + dbName, payload);
				listeners.remove(dbName);
				ongoingDownloads.remove(dbName);
				checkAndStopMonitor();
			}
		} else
			throw new NoOngoingDatabaseDownload(dbName);
	}

	private void checkAndStopMonitor() {
		synchronized (synchLock) {
			try {
				if (executor.getActiveCount() == 0) {
					monitor.stop();
					this.monitorStopped = true;
				}
			} catch (Exception e) {
				log4j.error("Error stopping the file alteration monitor", e);
			}
		}
	}

	private void checkAndStart() {
		if (monitorStopped)
			synchronized (synchLock) {
				try {
					monitor.start();
					monitorStopped = false;
				} catch (Exception e) {
					log4j.error("Monitor could not start", e);
				}
			}
	}

	@Override
	public CompletableFuture<File> requestDownload(String name, String url) throws DatabaseAlreadyDownloadingException {
		if (this.isDownloading(name))
			throw new DatabaseAlreadyDownloadingException(name);
		final DatabaseDownloadTask task = new DatabaseDownloadTask(tempDir, template, name, url);
		task.handle((t, u) -> {
			checkAndStopMonitor();
			ongoingDownloads.remove(name);
			listeners.remove(name);
			return t;
		});
		listeners.put(name, task.getListener());
		task.setMonitor(monitor);
		ongoingDownloads.put(name, executor.submit(task));
		checkAndStart();
		return task;
	}

	@Override
	public boolean isDownloading(String name) {
		return listeners.containsKey(name);
	}

	@Override
	public boolean isDownloading() {
		return !ongoingDownloads.isEmpty();
	}
}
