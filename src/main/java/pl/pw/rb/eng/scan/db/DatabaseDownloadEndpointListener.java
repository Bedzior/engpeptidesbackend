package pl.pw.rb.eng.scan.db;

import static java.text.MessageFormat.format;

import java.io.File;

import javax.validation.constraints.NotNull;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.apache.log4j.Logger;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import pl.pw.rb.eng.db.dto.DownloadProgress;
import pl.pw.rb.eng.web.controller.dto.TranslatableErrorMessage;

public class DatabaseDownloadEndpointListener implements DatabaseDownloadListener, FileAlterationListener {

	private final Logger log4j = Logger.getLogger(DatabaseDownloadEndpointListener.class);

	private final SimpMessagingTemplate messagingTemplate;

	private final String name;

	private long size;

	public DatabaseDownloadEndpointListener(@NotNull String name, SimpMessagingTemplate messagingTemplate) {
		this.name = name;
		this.messagingTemplate = messagingTemplate;
	}

	public void setSize(long size) {
		this.size = size;
	}

	@Override
	public void onProgress(int progress) {
		messagingTemplate.convertAndSend("/topic/db_upload/" + name, new DownloadProgress(this.size, progress));
	}

	@Override
	public void onFileChange(File file) {
		final long currentSize = FileUtils.sizeOf(file);
		int progress = (int) (currentSize * 100 / size);
		log4j.debug(format("Progress on {0}: {1,number,#} ({2}/{3})", file.getAbsolutePath(), progress,
				FileUtils.byteCountToDisplaySize(currentSize), FileUtils.byteCountToDisplaySize(size)));
		if (size > 0)
			this.onProgress(progress);
	}

	public boolean isFor(@NotNull String name) {
		return this.name.equals(name);
	}

	@Override
	public void onError(String message, String translationToken) {
		messagingTemplate.convertAndSend("/topic/db_upload/" + name, new TranslatableErrorMessage(message, translationToken));
	}
	// Things in this section won't be used

	@Override
	public void onStart(FileAlterationObserver observer) {
	}

	@Override
	public void onDirectoryCreate(File directory) {
	}

	@Override
	public void onDirectoryChange(File directory) {
	}

	@Override
	public void onDirectoryDelete(File directory) {
	}

	@Override
	public void onFileCreate(File file) {
	}

	@Override
	public void onFileDelete(File file) {
	}

	@Override
	public void onStop(FileAlterationObserver observer) {
	}

}
