package pl.pw.rb.eng.scan.db;

public interface DatabaseDownloadListener {

	void onProgress(int progress);

	void onError(String message, String translationToken);

	void setSize(long size);

}
