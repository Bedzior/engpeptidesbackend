package pl.pw.rb.eng.scan;

class ScanProgressStatus {

	private final Boolean init;

	private final Boolean finish;

	private final Boolean abort;

	private final Object status;

	public ScanProgressStatus(Boolean init, Boolean finish, Boolean abort, Object status) {
		this.init = init;
		this.finish = finish;
		this.abort = abort;
		this.status = status;
	}

	public ScanProgressStatus(boolean init, boolean finish, boolean abort, String status) {
		this.init = init;
		this.finish = finish;
		this.abort = abort;
		this.status = status;
	}

	public Boolean isInit() {
		return init;
	}

	public Boolean isFinish() {
		return finish;
	}

	public Boolean isAbort() {
		return this.abort;
	}

	public Object getStatus() {
		return status;
	}
}