package pl.pw.rb.eng.scan;

import static java.text.MessageFormat.format;
import static pl.pw.rb.eng.springconfig.PeptidesWebMvcConfiguration.DATE_STRING_FORMAT;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import org.apache.commons.compress.compressors.CompressorException;
import org.apache.log4j.Logger;

import mscanlib.ms.msms.dbengines.DbEngine;
import pl.pw.rb.eng.db.dao.ScansDAO;
import pl.pw.rb.eng.db.entities.Scan;
import pl.pw.rb.eng.db.entities.ScanResult;
import pl.pw.rb.eng.service.MailService;
import pl.pw.rb.eng.service.ScanResultService;
import pl.pw.rb.eng.service.ScannerService;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.service.exception.NoSuchScanException;

/**
 * This listener is responsible for the end communication to the user starting
 * the task.<br/>
 * <br/>
 * The main goal is to communicate to a user that their scan had been completed,
 * successfully or not, attaching the end files if applicable. The resulting
 * files are packed (gzip) to avoid exceeding the SMTP server's transfer limits.
 * 
 * @category <i>separation of concerns</i>, <i>e-mail</i>
 * @author Rafał Będźkowski
 *
 */
public class ScanUserCommunicationHandler extends AbstractDbEngineListener {

	private Logger logger = Logger.getLogger(ScanUserCommunicationHandler.class);

	private final ScanResultService resultFileService;

	private final MailService mailService;

	private final UsersService usersService;

	private final ScannerService scanService;

	private final ScansDAO scans;
	
	private final File[] outputFiles;

	private final String token;

	private final int clientID;

	public ScanUserCommunicationHandler(ScansDAO scans, ScanResultService resultFileService, MailService mailService, UsersService usersService, ScannerService scanService,
			File[] outputFiles, String token, int clientID) {
		this.scans = scans;
		this.resultFileService = resultFileService;
		this.mailService = mailService;
		this.usersService = usersService;
		this.scanService = scanService;
		this.outputFiles = outputFiles;
		this.token = token;
		this.clientID = clientID;
	}

	@Override
	public void onError(DbEngine engine, String message) {
		notifyEndUser();
	}

	@Override
	public void onFinished(DbEngine arg0) {
		notifyEndUser();
	}

	private final void notifyEndUser() {
		try {
			List<ScanResult> resultFiles = new ArrayList<>();
			final Scan scan = scans.findOne(token);
			Map<String, String> map = new Hashtable<>();
			map.put("uuid", token);
			map.put("created", DATE_STRING_FORMAT.format(scan.getCreated()));
			map.put("started", DATE_STRING_FORMAT.format(scan.getStarted()));
			map.put("finished", DATE_STRING_FORMAT.format(scan.getFinished()));
			map.put("parameters", scan.getParameters());
			map.put("count", Integer.toString(outputFiles.length));
			for (File file : outputFiles) {
				try {
					final ScanResult resultFile = resultFileService.create(token, file);
					resultFiles.add(resultFile);
					scanService.getSecure(token).setResult(resultFile.getResult());
				} catch (IOException e) {
					logger.error(format("Error finding scan''s result file: {0}", file.getAbsolutePath()), e);
				} catch (CompressorException e) {
					logger.error(format("Error zipping results"), e);
				} catch (NoSuchScanException e) {
					logger.error(format("The result corresponds to a non-existing scan"), e);
				}
			}
			mailService.send(usersService.getByID(clientID).getEmail(), "Scan finished", mailService.prepareTemplate("scan-finished", "html", map), resultFiles);
		} catch (MessagingException e) {
			logger.error("Could not send email to the scan initiator", e);
		} finally {
			for (File file : outputFiles) {
				if (!file.delete())
					file.deleteOnExit();
			}
		}
	}

}
