package pl.pw.rb.eng.scan.status;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ScanStatusMessage {

	private String message;

	public boolean aborted;

	public ScanStatusMessage(String message, boolean abort) {
		super();
		this.message = message;
		this.aborted = abort;
	}

	@JsonProperty
	public String getMessage() {
		return message;
	}

	@JsonProperty
	public boolean isAborted() {
		return this.aborted;
	}

}
