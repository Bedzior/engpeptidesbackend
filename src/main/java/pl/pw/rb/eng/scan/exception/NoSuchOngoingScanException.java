package pl.pw.rb.eng.scan.exception;

import static java.text.MessageFormat.format;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "No such ongoing scan")
@TranslationToken("ERR_NO_ONGOING_SCAN")
public class NoSuchOngoingScanException extends ServletException {

	private static final long serialVersionUID = 2492809199560790686L;

	public NoSuchOngoingScanException(String scanID) {
		super(format("No such ongoing peptide search: {0}", scanID));
	}

}
