package pl.pw.rb.eng.scan.db;

import static java.text.MessageFormat.format;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Controller;

import pl.pw.rb.eng.scan.exception.MissingCommandException;
import pl.pw.rb.eng.service.exception.NoOngoingDatabaseDownload;
import pl.pw.rb.eng.web.controller.dto.CommandMessage;
import pl.pw.rb.eng.web.controller.dto.StatusMessage;
import pl.pw.rb.eng.web.controller.dto.SuccessStatusMessage;
import pl.pw.rb.eng.web.controller.dto.TranslatableErrorMessage;

@Controller
class DatabaseDownloadEndpointController {

	private final Logger logger = Logger.getLogger(getClass());

	@Autowired
	private DatabaseDownloader downloader;

	@MessageMapping("/db_upload/{dbName}")
	StatusMessage command(@Payload CommandMessage payload, @DestinationVariable("dbName") String dbName) throws MissingCommandException {
		if (payload.getCommand() == null)
			throw new MissingCommandException();
		switch (payload.getCommand()) {
		case CANCEL:
			try {
				downloader.cancelDownloading(dbName);
				return new SuccessStatusMessage(format("{0} database download cancelled", dbName));
			} catch (NoOngoingDatabaseDownload e) {
				logger.warn("User ordered to cancel a non-running database download");
				return new TranslatableErrorMessage(format("No such database download is ongoing: {0}", dbName), "ERR_MISSING");
			}
		default:
			return new TranslatableErrorMessage(format("Unrecognized command: {0}", payload.getCommand()), "ERR_NO_COMMAND");
		}
	}
}
