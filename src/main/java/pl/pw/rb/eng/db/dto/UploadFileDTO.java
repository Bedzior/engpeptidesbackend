package pl.pw.rb.eng.db.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect
public class UploadFileDTO {

	public UploadFileDTO() {
	}

	protected UploadFileDTO(String token, Date uploadTime) {
		super();
		this.token = token;
		this.uploadTime = uploadTime;
	}

	private String token;

	private Date uploadTime;

	@JsonProperty
	public String getToken() {
		return token;
	}

	@JsonProperty
	public Date getUploadTime() {
		return uploadTime;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public void setUploadTime(Date uploadTime) {
		this.uploadTime = uploadTime;
	}
}
