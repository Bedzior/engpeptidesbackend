package pl.pw.rb.eng.db.dao;

import java.util.Collection;

import org.springframework.context.annotation.DependsOn;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import pl.pw.rb.eng.db.entities.Scan;
import pl.pw.rb.eng.db.entities.ScanStatus;
import pl.pw.rb.eng.db.entities.User;

@DependsOn({ "userDAO" })
@Transactional
public interface ScansDAO extends CrudRepository<Scan, String>, ScansDAOExtension {

	Collection<Scan> findByUser(User user);

	Collection<Scan> findByUserAndStatusIn(User user, Collection<ScanStatus> status);

	Collection<Scan> findByStatus(ScanStatus status);

	@Query("SELECT COUNT(s.id) FROM Scan s WHERE s.status = ?1")
	int countByStatus(ScanStatus status);

}
