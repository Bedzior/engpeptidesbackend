package pl.pw.rb.eng.db.dao;

import org.springframework.context.annotation.DependsOn;
import org.springframework.data.repository.CrudRepository;

import pl.pw.rb.eng.db.entities.PasswordReset;

@DependsOn({ "userDAO" })
public interface PasswordResetDAO extends CrudRepository<PasswordReset, Integer> {

	PasswordReset findByUuid(String uuid);

}
