package pl.pw.rb.eng.db.entities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.io.IOUtils;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "SCAN_RESULT", schema = "peptides")
@NamedQuery(name = "ScanResult.findAll", query = "SELECT r FROM ScanResult r")
@JsonAutoDetect(isGetterVisibility = Visibility.NONE, getterVisibility = Visibility.NONE)
public class ScanResult implements Serializable {

	private static final long serialVersionUID = 4713712254306896205L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String name;

	@ManyToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "scanID", referencedColumnName = "ID", foreignKey = @ForeignKey(name = "FK_SCANRESULT_SCAN", value = ConstraintMode.CONSTRAINT))
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Scan scan;

	@Lob
	private byte[] result;

	public ScanResult() {
	}

	public ScanResult(Scan scan, File file) throws FileNotFoundException, IOException, CompressorException {
		super();
		this.scan = scan;
		this.name = file.getName();
		try (FileInputStream inputStream = new FileInputStream(file)) {
			this.result = IOUtils.readFully(inputStream, (int) file.length());
		}
	}

	@JsonProperty
	public int getId() {
		return this.id;
	}

	@JsonProperty
	public String getName() {
		return this.name;
	}

	public byte[] getResult() {
		return this.result;
	}

	public Scan getScan() {
		return this.scan;
	}

}