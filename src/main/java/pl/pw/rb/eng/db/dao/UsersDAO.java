package pl.pw.rb.eng.db.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import pl.pw.rb.eng.db.entities.User;

public interface UsersDAO extends PagingAndSortingRepository<User, Integer> {

	User findByEmail(String email);

	User findByLogin(String login);

	Page<User> findAllForAdminDisplay(Pageable pageable);

	User findByToken(String token);

}
