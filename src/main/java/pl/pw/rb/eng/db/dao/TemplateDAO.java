package pl.pw.rb.eng.db.dao;

import org.springframework.data.repository.CrudRepository;

import pl.pw.rb.eng.db.entities.EmailTemplate;

public interface TemplateDAO extends CrudRepository<EmailTemplate, Integer> {

	EmailTemplate findByNameAndType(String name, String type);
}
