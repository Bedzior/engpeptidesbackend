package pl.pw.rb.eng.db.dao;

import javax.persistence.EntityManagerFactory;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

final class DatabaseDAOImpl implements DatabaseDAOExtension {

	private static final String UPDATE_DOWNLOADING = "update Database d set d.downloading= 'false' where d.downloading = 'true'";

	private SessionFactory sessionFactory;

	@Autowired
	private DatabaseDAOImpl(EntityManagerFactory factory) {
		this.sessionFactory = factory.unwrap(SessionFactory.class);
	}

	@Override
	public int resetAllDownloading() {
		try (Session session = sessionFactory.openSession()) {
			Transaction tx = session.beginTransaction();

			int updatedEntities = session.createQuery(UPDATE_DOWNLOADING).executeUpdate();
			tx.commit();
			return updatedEntities;
		}
	}

}
