package pl.pw.rb.eng.db.dao;

import org.springframework.context.annotation.DependsOn;
import org.springframework.data.repository.CrudRepository;

import pl.pw.rb.eng.db.entities.Scan;
import pl.pw.rb.eng.db.entities.ScanResult;

@DependsOn({ "userDAO" })
public interface ScanResultDAO extends CrudRepository<ScanResult, Integer> {

	ScanResult findByScan(Scan scan);

}
