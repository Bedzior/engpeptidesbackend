package pl.pw.rb.eng.db.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum ScanStatus {
	WAITING(1, false),
	RUNNING(2, false),
	FINISHED(50, true),
	ABORTED(98, true),
	FAILED(99, true);

	private final int code;

	private final boolean finished;

	private ScanStatus(int code, boolean finished) {
		this.code = code;
		this.finished = finished;
	}

	public boolean isRunning() {
		return this == ScanStatus.RUNNING;
	}

	public boolean isFinished() {
		return finished;
	}

	@JsonProperty
	public int getCode() {
		return code;
	}
}
