package pl.pw.rb.eng.db.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "database", schema = "peptides", indexes = { @Index(name = "DATABASE_UNQ_NAME", columnList = "NAME", unique = true) })
@NamedQuery(name = "Database.findAll", query = "SELECT d FROM Database d")
@JsonAutoDetect(getterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE)
public class Database implements Serializable {

	private static final long serialVersionUID = 9154412419316390895L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(updatable = false, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	private Date added;

	@Column
	@Temporal(TemporalType.TIMESTAMP)
	@UpdateTimestamp
	private Date updated;

	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date downloaded;

	@Column(name = "NAME", updatable = true)
	private String name;

	@Column(name = "DESCRIPTION", updatable = true, length = 32672)
	private String description;

	@Column(name = "PATH")
	private String path;

	@Column(name = "REGEX_ID")
	private String regexId;

	@Column(name = "REGEX_NAME")
	private String regexName;

	@Column(name = "URL")
	private String url;

	// bi-directional many-to-one association to User
	@ManyToOne(optional = true, cascade = CascadeType.DETACH)
	@JoinColumn(name = "USERID", foreignKey = @ForeignKey(name = "FK_DATABASE_USER"))
	private User user;

	@Column(name = "downloading")
	private boolean downloading;

	@Column(name = "SIZE")
	private long size;

	public Database() {
	}

	public int getId() {
		return this.id;
	}

	@JsonProperty
	public Date getAdded() {
		return this.added;
	}

	@JsonProperty
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty
	public String getPath() {
		return this.path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@JsonProperty
	public String getRegexId() {
		return this.regexId;
	}

	public void setRegexId(String regexId) {
		this.regexId = regexId;
	}

	@JsonProperty
	public String getRegexName() {
		return this.regexName;
	}

	public void setRegexName(String regexName) {
		this.regexName = regexName;
	}

	@JsonProperty
	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@JsonProperty
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@JsonProperty
	public Date getDownloaded() {
		return downloaded;
	}

	@JsonProperty
	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public void setDownloaded() {
		this.downloaded = new Date();
	}

	@PrePersist
	protected void onCreate() {
		added = new Date();
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty
	public String getDescription() {
		return description;
	}

	public void setDownloading(boolean downloading) {
		this.downloading = downloading;
	}

	@JsonProperty
	public boolean isDownloading() {
		return downloading;
	}

	@JsonProperty
	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

}