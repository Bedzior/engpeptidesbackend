package pl.pw.rb.eng.db.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "PASSWORD_RESET", schema = "peptides", indexes = {
		@Index(name = "PASSWORD_RESET_UNQ_UUID", columnList = "uuid", unique = true) })
@NamedQuery(name = "PasswordReset.findAll", query = "SELECT p FROM PasswordReset p")
public class PasswordReset implements Serializable {

	private static final long serialVersionUID = -1927563316645914153L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Basic(optional = false)
	@Column(insertable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	// bi-directional many-to-one association to User
	@ManyToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "userID", foreignKey = @ForeignKey(name = "FK_PASSWORDRESET_USER"))
	@OnDelete(action = OnDeleteAction.CASCADE)
	private User user;

	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	private String uuid;

	private Timestamp completed;

	public PasswordReset() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCompleted() {
		return this.completed;
	}

	public void setCompleted(Timestamp completed) {
		this.completed = completed;
	}

	public Date getCreated() {
		return this.created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public String getUuid() {
		return this.uuid;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}