package pl.pw.rb.eng.db.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "SCAN", schema = "peptides")
@NamedQuery(name = "Scan.findAll", query = "SELECT s FROM Scan s")
@JsonAutoDetect(getterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE)
public class Scan implements Serializable {

	private static final long serialVersionUID = 9138812525880456828L;

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	private String id;

	@ManyToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(updatable = false, name = "userID", foreignKey = @ForeignKey(name = "FK_SCAN_USER"))
	@OnDelete(action = OnDeleteAction.CASCADE)
	private User user;

	/**
	 * JSON object the scan request was created with
	 */
	@Column(length = 32672, updatable = false)
	private String parameters;

	@Lob
	private byte[] result;

	private String sessionID;

	private ScanStatus status;

	@Column(updatable = false)
	private Date created;

	@Column(updatable = true)
	private Date started;

	@Column(nullable = true)
	private Date finished;

	public Scan() {
		super();
	}

	public Scan(User user, String sessionID, String parameters) {
		super();
		this.user = user;
		this.parameters = parameters;
		this.sessionID = sessionID;
		this.status = ScanStatus.WAITING;
		this.created = new Date();
	}

	@JsonProperty
	public String getId() {
		return id;
	}

	@JsonProperty
	public User getUser() {
		return this.user;
	}

	@JsonProperty
	public String getParameters() {
		return this.parameters;
	}

	public byte[] getResult() {
		return this.result;
	}

	public void setResult(byte[] result) {
		this.result = result;
	}

	@JsonProperty
	public boolean isRunning() {
		return this.status.isRunning();
	}

	@JsonProperty
	public boolean isCanceled() {
		return status == ScanStatus.ABORTED;
	}

	public String getSessionID() {
		return sessionID;
	}

	@JsonProperty
	public ScanStatus getStatus() {
		return status;
	}

	public void setStatus(ScanStatus status) {
		if (status.isFinished()) {
			this.finished = new Date();
		} else if (status == ScanStatus.RUNNING) {
			this.started = new Date();
		}
		this.status = status;
	}

	@JsonProperty
	public Date getCreated() {
		return this.created;
	}

	@JsonProperty
	public Date getStarted() {
		return this.started;
	}

	@JsonProperty
	public Date getFinished() {
		return finished;
	}
}
