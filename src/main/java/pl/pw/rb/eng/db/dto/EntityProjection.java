package pl.pw.rb.eng.db.dto;

import org.springframework.data.rest.core.config.Projection;

/**
 * Internal type to explicitly mark {@link Projection}s that need stripping
 * before sending to client.
 * 
 * @author Rafał Będźkowski
 *
 */
public interface EntityProjection {

}
