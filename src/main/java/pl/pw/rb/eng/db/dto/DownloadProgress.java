package pl.pw.rb.eng.db.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * DTO informing the end-user about database download progress
 * 
 * @author Rafał Będźkowski
 *
 */
@JsonAutoDetect
public class DownloadProgress {

	private final long total;

	private final int progress;

	public DownloadProgress(long total, int progress) {
		this.total = total;
		this.progress = progress;
	}

	@JsonProperty
	public int getProgress() {
		return progress;
	}

	@JsonProperty
	public long getTotal() {
		return total;
	}

}
