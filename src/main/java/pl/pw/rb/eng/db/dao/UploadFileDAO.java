package pl.pw.rb.eng.db.dao;

import org.springframework.context.annotation.DependsOn;
import org.springframework.data.repository.CrudRepository;

import pl.pw.rb.eng.db.entities.UploadFile;
import pl.pw.rb.eng.db.entities.User;

@DependsOn({ "userDAO" })
public interface UploadFileDAO extends CrudRepository<UploadFile, String> {

	UploadFile findByUserAndToken(User user, String token);

}
