package pl.pw.rb.eng.db.dao;

import java.util.Collection;

import org.springframework.context.annotation.DependsOn;
import org.springframework.data.repository.CrudRepository;

import pl.pw.rb.eng.db.dto.SimpleDatabaseDTO;
import pl.pw.rb.eng.db.entities.Database;

@DependsOn({ "userDAO" })
public interface DatabaseDAO extends CrudRepository<Database, Integer>, DatabaseDAOExtension {

	Database findByName(String name);

	Collection<SimpleDatabaseDTO> findAllProjectedBy();

	Collection<Database> findDatabaseByDownloading(boolean downloading);

}
