package pl.pw.rb.eng.db.dao;

import static java.text.MessageFormat.format;

import javax.persistence.EntityManagerFactory;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import pl.pw.rb.eng.db.entities.ScanStatus;

final class ScansDAOImpl implements ScansDAOExtension {

	private static final String UPDATE_RUNNING = format("update Scan s set s.status = {0} where s.status = {1}", ScanStatus.WAITING.ordinal(),
			ScanStatus.RUNNING.ordinal());

	private SessionFactory sessionFactory;

	@Autowired
	private ScansDAOImpl(EntityManagerFactory factory) {
		this.sessionFactory = factory.unwrap(SessionFactory.class);
	}

	@Override
	public int resetAllRunning() {
		try (Session session = sessionFactory.openSession()) {
			Transaction tx = session.beginTransaction();

			int updatedEntities = session.createQuery(UPDATE_RUNNING).executeUpdate();
			tx.commit();
			return updatedEntities;
		}
	}

}
