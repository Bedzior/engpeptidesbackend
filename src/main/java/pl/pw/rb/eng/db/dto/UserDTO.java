package pl.pw.rb.eng.db.dto;

import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class UserDTO {

	public UserDTO() {
		super();
	}

	protected UserDTO(Integer id, boolean admin, String email, String login) {
		super();
		this.id = id;
		this.admin = admin;
		this.email = email;
		this.login = login;
	}

	private Integer id;

	private boolean admin;

	private String email;

	private String login;

	private Collection<String> roles;

	public Integer getId() {
		return id;
	}

	public boolean isAdmin() {
		return admin;
	}

	public String getEmail() {
		return email;
	}

	public String getLogin() {
		return login;
	}

	public Collection<String> getRoles() {
		return roles;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setRoles(Collection<String> roles) {
		this.roles = roles;
	}
}
