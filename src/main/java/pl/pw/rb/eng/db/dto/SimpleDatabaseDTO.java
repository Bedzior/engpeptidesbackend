package pl.pw.rb.eng.db.dto;

import org.springframework.data.rest.core.config.Projection;
import org.springframework.data.web.ProjectedPayload;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;

import pl.pw.rb.eng.db.entities.Database;

@Projection(name = "databaseDTO", types = { Database.class })
@ProjectedPayload
@JsonAutoDetect(getterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE)
public interface SimpleDatabaseDTO extends EntityProjection {

	@JsonProperty("value")
	public int getId();

	@JsonProperty("short")
	public String getName();

	@JsonProperty("name")
	public String getDescription();
}
