package pl.pw.rb.eng.db.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "EMAIL_TEMPLATE", schema = "peptides", indexes = { @Index(name = "EMAIL_TEMPLATE_UNQ_NAME_AND_TYPE", unique = true, columnList = "name,type") })
@NamedQuery(name = "EmailTemplate.findAll", query = "SELECT t FROM EmailTemplate t")
@JsonAutoDetect(isGetterVisibility = Visibility.NONE, getterVisibility = Visibility.NONE)
public class EmailTemplate implements Serializable {

	private static final long serialVersionUID = -784290599844873598L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Basic(optional = false)
	private String name;

	@Basic(optional = false)
	@ColumnDefault("'txt'")
	private String type;

	@Column(length = 32000)
	private String body;

	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModified;

	public EmailTemplate() {
		super();
	}

	public EmailTemplate(String name, String type) {
		this.name = name;
		this.type = type;
	}

	@PrePersist
	private void preStore() {
		this.lastModified = new Date();
	}

	@JsonProperty
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@JsonProperty
	public String getBody() {
		return this.body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	@JsonProperty
	public Date getLastModified() {
		return this.lastModified;
	}

	@JsonProperty
	public String getName() {
		return this.name;
	}

	@JsonProperty
	public String getType() {
		return type;
	}

}
