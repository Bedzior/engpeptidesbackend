package pl.pw.rb.eng.db.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Object sent to end-user on websocket status update
 * 
 * @author Rafał Będźkowski
 *
 */
@JsonAutoDetect
public class Message {

	private final String message;

	public Message(String message) {
		this.message = message;
	}

	@JsonProperty
	public String getMessage() {
		return message;
	}
}
