package pl.pw.rb.eng.db.entities;

import java.io.Serializable;
import java.util.Arrays;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "users", schema = "peptides", indexes = { @Index(columnList = "login", name = "USER_UNQ_NAME", unique = true),
		@Index(columnList = "email", name = "USER_UNQ_EMAIL", unique = true),
		@Index(columnList = "token", name = "USER_UNQ_TOKEN", unique = true)})
@NamedQueries({ @NamedQuery(name = "User.findAllForAdminDisplay", query = "SELECT u FROM User u") })
@JsonAutoDetect(getterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE)
public class User implements Serializable {

	private static final long serialVersionUID = -5604409097700127622L;

	private static final GrantedAuthority ROLE_USER = new SimpleGrantedAuthority("ROLE_USER");

	private static final GrantedAuthority ROLE_ADMIN = new SimpleGrantedAuthority("ROLE_ADMIN");

	private static final GrantedAuthority[] ADMIN_AUTHORITIES = new GrantedAuthority[] { ROLE_USER, ROLE_ADMIN };

	private static final GrantedAuthority[] USER_AUTHORITIES = new GrantedAuthority[] { ROLE_USER };

	public User() {
	}

	public User(String login, String email, String hash) {
		this(login, email, hash, false);
	}

	public User(String login, String email, String hash, boolean admin) {
		this.login = login;
		this.hash = hash;
		this.email = email;
		this.admin = admin;
		this.activated = false;
		this.token = UUID.randomUUID().toString();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(updatable = false, nullable = false)
	private String login;

	@Column(nullable = false)
	private String email;

	@Column(nullable = false)
	private String hash;

	@Column(nullable = false)
	private boolean admin;

	@Column(nullable = false)
	private String token;
	
	@Column(nullable = false)
	private boolean activated;

	@JsonProperty
	public boolean isAdmin() {
		return this.admin;
	}

	@JsonIgnore
	public String getHash() {
		return hash;
	}

	@JsonProperty
	public String getLogin() {
		return login;
	}

	@JsonProperty
	public String getEmail() {
		return email;
	}

	@JsonProperty
	public Integer getId() {
		return id;
	}

	@JsonIgnore
	public String getToken() {
		return token;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public GrantedAuthority[] getAuthorities() {
		if (isAdmin())
			return ADMIN_AUTHORITIES;
		else
			return USER_AUTHORITIES;
	}

	public String[] getRoles() {
		final GrantedAuthority[] authorities = this.getAuthorities();
		return Arrays.asList(authorities).stream().map(authority -> authority.getAuthority().replace("ROLE_", ""))
				.toArray(value -> new String[authorities.length]);
	}

	@JsonProperty
	public boolean isActivated() {
		return activated;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

}
