package pl.pw.rb.eng.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.apache.commons.compress.compressors.CompressorException;

import pl.pw.rb.eng.db.entities.ScanResult;
import pl.pw.rb.eng.service.exception.NoProteinFoundException;
import pl.pw.rb.eng.service.exception.NoSuchScanException;
import pl.pw.rb.eng.service.exception.NoSuchScanResultException;
import pl.pw.rb.eng.web.servlet.dto.scan.MsMsShortPeptideHit;
import pl.pw.rb.eng.web.servlet.dto.scan.MsMsShortProteinHit;
import pl.pw.rb.eng.web.servlet.dto.scan.MsOutputDTO;
import pl.pw.rb.eng.web.servlet.exception.ScanResultReadException;
import pl.pw.rb.eng.web.servlet.scan.exception.OngoingScanException;

/**
 * Manages creation and retrieval of peptide scans' results.
 * 
 * @author Rafał Będźkowski
 *
 */
public interface ScanResultService {

	public static final String CACHE_NAME = "scanResults";

	/**
	 * 
	 * @param token
	 * @param file
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws CompressorException
	 * @throws NoSuchScanException
	 */
	ScanResult create(String token, File file) throws FileNotFoundException, IOException, CompressorException, NoSuchScanException;

	/**
	 * Retrieves scan result, if applicable
	 * 
	 * @param token peptide scan identifier
	 * @return {@link ScanResult} identified by the token, if finished and result is
	 *         available
	 * @throws NoSuchScanException       in case the requested scan does not exist
	 *                                   or the user has no access to it as a
	 *                                   resource, so it neither:
	 *                                   <ul>
	 *                                   <li>the starting user, nor</li>
	 *                                   <li>an admin</li>
	 *                                   </ul>
	 * @throws OngoingScanException      in case the requested scan is still
	 *                                   ongoing, thus - no result is available yet
	 * @throws NoSuchScanResultException
	 */
	@NotNull
	ScanResult getForScan(@NotNull String token) throws NoSuchScanException, OngoingScanException, NoSuchScanResultException;

	@NotNull
	MsOutputDTO getResult(@NotNull String token) throws NoSuchScanException, OngoingScanException, ScanResultReadException, NoSuchScanResultException;

	@NotNull
	Map<String, MsMsShortProteinHit> getProteinHashes(String token)
			throws NoSuchScanException, OngoingScanException, NoSuchScanResultException, ScanResultReadException;

	@NotNull
	Map<String, MsMsShortPeptideHit> getPeptideHashes(String token, String protein)
			throws NoSuchScanException, OngoingScanException, NoSuchScanResultException, ScanResultReadException, NoProteinFoundException;
}
