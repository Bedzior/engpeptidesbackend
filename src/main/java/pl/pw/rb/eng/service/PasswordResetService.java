package pl.pw.rb.eng.service;

import javax.mail.MessagingException;

public interface PasswordResetService {

	boolean verify(String uuid);

	void createForUser(String username) throws MessagingException;

	void createForEmail(String email) throws MessagingException;
}
