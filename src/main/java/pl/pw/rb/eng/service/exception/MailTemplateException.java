package pl.pw.rb.eng.service.exception;

import static java.text.MessageFormat.format;

import java.util.MissingResourceException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Missing e-mail template")
public class MailTemplateException extends MissingResourceException {

	private static final long serialVersionUID = -7676973994774348213L;

	public MailTemplateException(String name, Class<?> caller) {
		super(format("No mail template \"{0}\" found in resources", name), caller.getName(), name);
	}

}
