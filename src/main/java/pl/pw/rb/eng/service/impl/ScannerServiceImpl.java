package pl.pw.rb.eng.service.impl;

import static java.text.MessageFormat.format;
import static java.util.Arrays.asList;
import static pl.pw.rb.eng.db.entities.ScanStatus.ABORTED;
import static pl.pw.rb.eng.db.entities.ScanStatus.FAILED;
import static pl.pw.rb.eng.db.entities.ScanStatus.FINISHED;
import static pl.pw.rb.eng.db.entities.ScanStatus.RUNNING;
import static pl.pw.rb.eng.db.entities.ScanStatus.WAITING;

import java.io.File;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.util.Pair;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import mscanlib.ms.msms.dbengines.DbEngine;
import pl.pw.rb.eng.db.dao.ScansDAO;
import pl.pw.rb.eng.db.entities.Database;
import pl.pw.rb.eng.db.entities.Scan;
import pl.pw.rb.eng.db.entities.ScanStatus;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.scan.AbstractDbEngineListener;
import pl.pw.rb.eng.scan.ScannerTask;
import pl.pw.rb.eng.scan.ScannerTaskFactory;
import pl.pw.rb.eng.scan.exception.DbConfigurationException;
import pl.pw.rb.eng.scan.exception.NoSuchOngoingScanException;
import pl.pw.rb.eng.service.DatabaseService;
import pl.pw.rb.eng.service.ScannerService;
import pl.pw.rb.eng.service.UploadFileService;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.service.exception.NoScanFileException;
import pl.pw.rb.eng.service.exception.NoSuchScanException;
import pl.pw.rb.eng.service.exception.NullUserException;
import pl.pw.rb.eng.web.servlet.dto.scan.JSONDbEngineScanConfig;
import pl.pw.rb.eng.web.servlet.dto.scan.ScanConfiguration;
import pl.pw.rb.eng.web.servlet.dto.scan.ScanConfiguration.DataConfiguration;
import pl.pw.rb.eng.web.servlet.dto.scan.ScanConfiguration.OtherConfiguration;

@Service
class ScannerServiceImpl implements ScannerService {

	private Logger log4j = Logger.getLogger(getClass());

	/**
	 * TODO move to @Bean construction and use Spring's capabilities
	 * {@link ThreadPoolTaskExecutor}
	 */
	private ThreadPoolExecutor threads;

	@Autowired
	private SimpMessagingTemplate messagingService;

	@Autowired
	private UsersService userService;

	@Autowired
	private UploadFileService uploadFileService;

	@Autowired
	private DatabaseService dbService;

	@Autowired
	private ScansDAO dao;

	@Autowired
	private ScannerTaskFactory taskFactory;

	@Autowired
	private ObjectMapper mapper;

	private Map<String, Pair<ScannerTask, Future<?>>> mapping = new ConcurrentHashMap<>(0);

	@Value("${peptides.max-concurrent-scans}")
	private int maxPool;

	@Value("${peptides.max-concurrent-scans-per-user}")
	private int maxScansPerUser;

	/**
	 * The comparator will prioritize the inserted tasks as follows:
	 * <ul>
	 * <li>typical check for null value happens first (non-null preferred);
	 * next</li>
	 * <li>tasks exceeding the total open slots for a single user receive a smaller
	 * priority; if this does not apply, then</li>
	 * <li>tasks started later receive a smaller priority.</li>
	 * </ul>
	 */
	private Comparator<Runnable> comparator = (Runnable t1, Runnable t2) -> {
		if (!(t1 instanceof ScannerTask) || !(t2 instanceof ScannerTask)) {
			if (!(t1 instanceof ScannerTask) && !(t2 instanceof ScannerTask))
				return ScannerTask.EQUAL;
			else if (t1 instanceof ScannerTask)
				return ScannerTask.FIRST;
			else if (t2 instanceof ScannerTask)
				return ScannerTask.SECOND;
		}
		Scan s1 = dao.findOne(((ScannerTask) t1).getToken());
		Scan s2 = dao.findOne(((ScannerTask) t2).getToken());
		if (s1 == null && s2 == null)
			return ScannerTask.EQUAL;
		else if (s1 == null)
			return ScannerTask.SECOND;
		else if (s2 == null)
			return ScannerTask.FIRST;
		Collection<Scan> u1 = dao.findByUser(s1.getUser());
		Collection<Scan> u2 = dao.findByUser(s2.getUser());
		if (u1.size() > maxScansPerUser && u2.size() > maxScansPerUser)
			return ScannerTask.EQUAL;
		else if (u1.size() > maxScansPerUser)
			return ScannerTask.SECOND;
		else if (u2.size() > maxScansPerUser)
			return ScannerTask.FIRST;
		return Long.compare(s1.getCreated().getTime(), s2.getCreated().getTime());
	};

	/**
	 * This class is simply here to keep the task map cleaning-after logic in one
	 * place.
	 * 
	 * @category <i>separation of concerns</i>
	 * @author Rafał Będźkowski
	 *
	 */
	private class ScannerServiceSanitizer extends AbstractDbEngineListener {

		private final String token;

		ScannerServiceSanitizer(String token) {
			this.token = token;
		}

		@Override
		public void onError(DbEngine engine, String message) {
			clearTaskReferences(token);
		}

		@Override
		public void onFinished(DbEngine engine) {
			clearTaskReferences(token);
		}

	}

	@PostConstruct
	public void postConstruct() {
		if (maxPool == -1)
			maxPool = Integer.MAX_VALUE;
		if (maxScansPerUser == -1)
			maxScansPerUser = Integer.MAX_VALUE;
		final BlockingQueue<Runnable> queue = new PriorityBlockingQueue<Runnable>(15, comparator);
		threads = new ThreadPoolExecutor(2, maxPool, 10, TimeUnit.SECONDS, queue);
		threads.prestartCoreThread();
	}

	@Override
	public Scan createFor(User user, String sessionID, ScanConfiguration jsonConfiguration)
			throws DbConfigurationException, NullUserException, NoScanFileException {
		if (user == null)
			throw new NullUserException();
		if (jsonConfiguration == null)
			throw new DbConfigurationException("Missing scan configuration");
		log4j.trace(format("Starting scan for user: {0}", user.getLogin()));
		return createScan(user, sessionID, jsonConfiguration);
	}

	public Scan createScan(User user, String sessionID, ScanConfiguration config) throws DbConfigurationException, NoScanFileException {
		final Scan scan;
		JSONDbEngineScanConfig engineConfig = createEngineConfiguration(sessionID, user, config);
		try {
			String jsonParameters = mapper.writeValueAsString(config);
			scan = dao.save(new Scan(user, sessionID, jsonParameters));
		} catch (JsonProcessingException e) {
			throw new RuntimeException("Unexpected JSON marshalling failure: " + config.toString());
		}
		final String token = scan.getId();
		final ScannerTask task = taskFactory.createTask(user, token, engineConfig, messagingService);
		task.addListener(new ScannerServiceSanitizer(token));
		final Future<?> submitted = threads.submit(task);
		mapping.put(token, Pair.of(task, submitted));
		return scan;
	}

	private JSONDbEngineScanConfig createEngineConfiguration(String sessionID, User user, ScanConfiguration config)
			throws DbConfigurationException, NoScanFileException {
		final String[] files = extractFilesData(config.getOther(), sessionID, user);
		final Database database = extractDatabaseData(config.getData());
		JSONDbEngineScanConfig engineConfig = new JSONDbEngineScanConfig(user, database, files, config.getBio(), config.getModifications(), config.getOther());
		return engineConfig;
	}

	protected Database extractDatabaseData(DataConfiguration dataConfiguration) {
		return dbService.getDatabase(dataConfiguration.getDatabase());
	}

	public String[] extractFilesData(OtherConfiguration otherConfiguration, String sessionID, final User user)
			throws DbConfigurationException, NoScanFileException {
		final String[] tokens = otherConfiguration.getFiles();
		if (ArrayUtils.isEmpty(tokens))
			throw new DbConfigurationException("At least one file should have been uploaded");
		List<String> files = new ArrayList<String>(tokens.length);
		for (Object token : Arrays.asList(tokens)) {
			if (token != null) {
				final File file = uploadFileService.getFile(user, (String) token);
				files.add(file.getAbsolutePath());
			}
		}
		return files.toArray(new String[files.size()]);
	}

	@Override
	public DbEngine getEngine(@NotNull String token) {
		final ScannerTask scannerTask = getScannerTask(token);
		if (scannerTask == null)
			return null;
		return scannerTask.getEngine();
	}

	@PreDestroy
	protected void destroy() throws Throwable {
		threads.shutdownNow();
	}

	private ScannerTask getScannerTask(@NotNull String token) {
		final Pair<ScannerTask, Future<?>> pair = mapping.get(token);
		if (pair == null)
			return null;
		final ScannerTask scannerTask = pair.getFirst();
		if (scannerTask == null) // $COVERAGE-IGNORE$
			throw new IllegalStateException("ScannerTask is null in a pair, which should not have happened"); // $COVERAGE-IGNORE$
		return scannerTask;
	}

	@Override
	public void stopTask(@NotNull String token) throws NoSuchOngoingScanException {
		final Pair<ScannerTask, Future<?>> pair = mapping.get(token);
		if (pair == null)
			throw new NoSuchOngoingScanException(token);
		final Future<?> submitted = pair.getSecond();
		if (submitted == null) // $COVERAGE-IGNORE$
			throw new IllegalStateException("Future is null in a pair, which should not have happened"); // $COVERAGE-IGNORE$
		if (!(submitted.cancel(true) && pair.getFirst().cancel(true))) {
			log4j.info(format("Stopping scan {0}, which has not yet started", token));
		}
		clearTaskReferences(token);
	}

	private void clearTaskReferences(String token) {
		mapping.remove(token);
	}

	@Override
	public boolean isRunning(String token) throws NoSuchScanException {
		return this.get(token).isRunning();
	}

	@Override
	public Collection<Scan> getForUser(User user) {
		return dao.findByUser(user);
	}

	@Override
	public Collection<Scan> getForUser(Principal user) {
		return this.getForUser(userService.getByLogin(user.getName()));
	}

	@Override
	public Scan get(String token) throws NoSuchScanException {
		final Scan scan = dao.findOne(token);
		if (scan == null)
			throw new NoSuchScanException(token);
		return scan;
	}

	@Override
	public Scan getSecure(String token) throws NoSuchScanException {
		final Scan scan = dao.findOne(token);
		if (scan == null)
			throw new NoSuchScanException(token);
		return scan;
	}

	@Override
	public Comparator<Runnable> getTaskComparator() {
		return comparator;
	}

	@Override
	public Collection<Scan> getForUser(Principal user, boolean finished) {
		return this.getForUser(userService.getByLogin(user.getName()), finished);
	}

	@Override
	public Collection<Scan> getForUser(User user, boolean finished) {
		if (finished)
			return dao.findByUserAndStatusIn(user, asList(ABORTED, FAILED, FINISHED));
		else
			return dao.findByUserAndStatusIn(user, asList(RUNNING, WAITING));
	}

	@Override
	public void resetRunning() {
		dao.resetAllRunning();
		Collection<Scan> rerunnable = dao.findByStatus(WAITING);
		rerunnable.forEach(scan -> {
			// XXX can't do, uploaded files are temporary
		});
	}

	@Override
	public int count(ScanStatus status) {
		return dao.countByStatus(status);
	}
}
