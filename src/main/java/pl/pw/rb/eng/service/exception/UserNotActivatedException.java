package pl.pw.rb.eng.service.exception;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "User's e-mail has not been activated")
@TranslationToken("ERR_USER_NOT_ACTIVATED")
public class UserNotActivatedException extends ServletException {

	private static final long serialVersionUID = 874541274643819513L;

}
