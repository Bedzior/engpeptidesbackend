package pl.pw.rb.eng.service.exception;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.ACCEPTED, reason = "User was registered but e-mail was not sent")
@TranslationToken("ERR_EMAIL_NOT_SENT")
public class RegistrationEmailNotSentException extends ServletException {

	private static final long serialVersionUID = 4013130126930544118L;

	public RegistrationEmailNotSentException() {
		super();
	}
}
