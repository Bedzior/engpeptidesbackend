package pl.pw.rb.eng.service.exception;

import static java.text.MessageFormat.format;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.db.entities.Database;
import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "This database is currently being downloaded")
@TranslationToken("ERR_IN_PROGRESS")
public class DatabaseAlreadyDownloadingException extends ServletException {

	private static final long serialVersionUID = 7671582589232885999L;

	public DatabaseAlreadyDownloadingException(String name) {
		super(format("Database {0} is already being downloaded", name));
	}

	public DatabaseAlreadyDownloadingException(Database db) {
		this(db.getName());
	}
}
