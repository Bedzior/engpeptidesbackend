package pl.pw.rb.eng.service;

import java.util.Collection;

import javax.validation.constraints.NotNull;

import pl.pw.rb.eng.service.exception.MissingConfigurationValueException;
import pl.pw.rb.eng.web.dto.Configuration;
import pl.pw.rb.eng.web.servlet.admin.exception.ConfigurationReadException;
import pl.pw.rb.eng.web.servlet.admin.exception.ConfigurationUpdateException;
import pl.pw.rb.eng.web.servlet.admin.exception.NoSuchConfigurationException;

/**
 * Provides configuration interface, for retrieving (authenticated, any access)
 * or updating (admin-only) configuration values in the application's external
 * <code>application.properties</code>.
 * 
 * @author Rafał Będźkowski
 *
 */
public interface AdminConfigurationService {

	/**
	 * Retrieves a full list of configurations visible to the user
	 * 
	 * @return configurations' list
	 * @throws ConfigurationReadException 
	 */
	Collection<Configuration> list() throws ConfigurationReadException;

	/**
	 * Retrieves a single configuration value w/metadata
	 * 
	 * @param name identifier for the desired configuration value
	 * @return
	 * @throws NoSuchConfigurationException
	 * @throws ConfigurationReadException 
	 */
	Configuration get(@NotNull String name) throws ConfigurationReadException, NoSuchConfigurationException;

	/**
	 * Updates configurations by reference
	 * 
	 * @param configs
	 * @return the updated configurations
	 */
	Collection<Configuration> update(@NotNull Collection<Configuration> configs) throws ConfigurationUpdateException;

	/**
	 * Updates configuration by reference
	 * 
	 * @param config
	 * @return the updated configuration
	 * @throws NoSuchConfigurationException
	 * @throws MissingConfigurationValueException
	 */
	Configuration update(@NotNull Configuration config) throws NoSuchConfigurationException, MissingConfigurationValueException, ConfigurationUpdateException;

	/**
	 * Updates configuration by name
	 * 
	 * @param name
	 * @param value
	 * @return the updated configuration
	 * @throws NoSuchConfigurationException
	 * @throws MissingConfigurationValueException
	 */
	Configuration update(@NotNull String name, @NotNull Object value)
			throws NoSuchConfigurationException, MissingConfigurationValueException, ConfigurationUpdateException;

}
