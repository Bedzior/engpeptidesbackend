package pl.pw.rb.eng.service;

import org.springframework.data.domain.Page;

import pl.pw.rb.eng.db.dto.UserDTO;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.service.exception.RegistrationEmailNotSentException;
import pl.pw.rb.eng.web.servlet.dto.UserRegistration;
import pl.pw.rb.eng.web.servlet.exception.user.EmailAlreadyRegisteredException;
import pl.pw.rb.eng.web.servlet.exception.user.NoUserException;
import pl.pw.rb.eng.web.servlet.exception.user.UserAlreadyActivatedException;
import pl.pw.rb.eng.web.servlet.exception.user.UserAlreadyExistsException;

/**
 * Service used for communication with the users' database backend (registering,
 * deleting, modifying).
 * 
 * @author Rafał Będźkowski
 *
 */
public interface UsersService {

	User getByID(Integer clientID);

	User getByLogin(String login);

	User getByEmail(String email);

	void delete(String login);

	User createActivatedAdmin(String login, String email, String password) throws EmailAlreadyRegisteredException, UserAlreadyExistsException;

	User createActivatedUser(String login, String email, String password) throws EmailAlreadyRegisteredException, UserAlreadyExistsException;

	User createUser(UserRegistration form) throws EmailAlreadyRegisteredException, UserAlreadyExistsException, RegistrationEmailNotSentException;

	Page<User> listAll();

	User update(User user) throws NoUserException;

	UserDTO getByLoginDTO(String name);

	void activateUser(String token) throws NoUserException, UserAlreadyActivatedException;

}
