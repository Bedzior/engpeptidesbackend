package pl.pw.rb.eng.service.exception;

import static java.text.MessageFormat.format;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
@TranslationToken("ERR_NO_CONFIG_VALUE")
public class MissingConfigurationValueException extends ServletException {

	private static final long serialVersionUID = -6048680838509809727L;

	public MissingConfigurationValueException(String name) {
		super(format("Missing configuration value for {0}", name));
	}

}
