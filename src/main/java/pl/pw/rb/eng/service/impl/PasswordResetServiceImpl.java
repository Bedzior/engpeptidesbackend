package pl.pw.rb.eng.service.impl;

import static java.text.MessageFormat.format;

import java.util.Hashtable;
import java.util.Map;

import javax.mail.MessagingException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.pw.rb.eng.db.dao.PasswordResetDAO;
import pl.pw.rb.eng.db.entities.PasswordReset;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.service.MailService;
import pl.pw.rb.eng.service.PasswordResetService;
import pl.pw.rb.eng.service.UsersService;

@Service
class PasswordResetServiceImpl implements PasswordResetService {

	private Logger logger = Logger.getLogger(getClass());

	@Autowired
	PasswordResetDAO resetDAO;

	@Autowired
	UsersService userService;

	@Autowired
	private MailService mailService;

	@Override
	public boolean verify(String uuid) {
		return resetDAO.findByUuid(uuid) != null;
	}

	@Override
	public void createForUser(String username) throws MessagingException {
		final User user = userService.getByLogin(username);
		if (user != null) {
			saveForUserAndSend(user);
		} else {
			logger.warn(format("Password reset link requested for non-existent user: {0}", username));
		}

	}

	@Override
	public void createForEmail(String email) throws MessagingException {
		final User user = userService.getByEmail(email);
		if (user != null) {
			saveForUserAndSend(user);
		} else {
			logger.warn(format("Password reset link requested for non-existent user: {0}", email));
		}
	}

	public PasswordReset saveForUserAndSend(User user) throws MessagingException {
		PasswordReset dto = new PasswordReset();
		dto.setUser(user);
		PasswordReset saved = resetDAO.save(dto);
		Map<String, String> map = new Hashtable<>();
		map.put("uuid", saved.getUuid());
		map.put("username", user.getLogin());
		mailService.send(user.getEmail(), "Password reset", mailService.prepareTemplate("password-reset", "html", map));
		return saved;
	}

}
