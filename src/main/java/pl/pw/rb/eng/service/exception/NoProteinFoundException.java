package pl.pw.rb.eng.service.exception;

import static java.text.MessageFormat.format;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Requested protein not found in the scan result")
@TranslationToken("ERR_PROTEIN_NOT_FOUND")
public class NoProteinFoundException extends ServletException {

	private static final long serialVersionUID = 6346442064209877173L;

	public NoProteinFoundException(String token, String protein) {
		super(format("No protein {1} found in the result of scan {0}", token, protein));
	}

}
