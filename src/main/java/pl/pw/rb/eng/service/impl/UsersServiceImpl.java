package pl.pw.rb.eng.service.impl;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Hashtable;
import java.util.Map;

import javax.mail.MessagingException;

import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import pl.pw.rb.eng.db.dao.UsersDAO;
import pl.pw.rb.eng.db.dto.UserDTO;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.service.MailService;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.service.exception.RegistrationEmailNotSentException;
import pl.pw.rb.eng.web.servlet.dto.UserRegistration;
import pl.pw.rb.eng.web.servlet.exception.user.EmailAlreadyRegisteredException;
import pl.pw.rb.eng.web.servlet.exception.user.NoUserException;
import pl.pw.rb.eng.web.servlet.exception.user.UserAlreadyActivatedException;
import pl.pw.rb.eng.web.servlet.exception.user.UserAlreadyExistsException;

@Service
class UsersServiceImpl implements UsersService {

	private static final String HTML_TEMPLATE = "registration";

	private Logger log4j = Logger.getLogger(getClass());

	@Autowired
	UsersDAO userDAO;

	@Autowired
	private PasswordEncoder encoder;

	@Autowired
	private ModelMapper mapper;

	@Autowired
	private MailService mailService;

	@Override
	public User getByLogin(String login) {
		return userDAO.findByLogin(login);
	}

	@Override
	public User createActivatedUser(String login, String email, String password) throws EmailAlreadyRegisteredException, UserAlreadyExistsException {
		checkExistence(login, email);
		User user = new User(login, email, encoder.encode(password));
		user.setActivated(true);
		return this.userDAO.save(user);
	}

	@Override
	public User createActivatedAdmin(String login, String email, String password) throws EmailAlreadyRegisteredException, UserAlreadyExistsException {
		checkExistence(login, email);
		User admin = new User(login, email, encoder.encode(password), true);
		admin.setActivated(true);
		return this.userDAO.save(admin);
	}

	public void checkExistence(String login, String email) throws EmailAlreadyRegisteredException, UserAlreadyExistsException {
		if (this.userDAO.findByEmail(email) != null)
			throw new EmailAlreadyRegisteredException(email);
		if (this.userDAO.findByLogin(login) != null)
			throw new UserAlreadyExistsException();
	}

	@Override
	public void delete(String login) {
		final User user = userDAO.findByLogin(login);
		if (user != null)
			userDAO.delete(user);
	}

	@Override
	public User getByEmail(String email) {
		return userDAO.findByEmail(email);
	}

	@Override
	public User getByID(Integer clientID) {
		return userDAO.findOne(clientID);
	}

	@Override
	public Page<User> listAll() {
		return userDAO.findAllForAdminDisplay(new PageRequest(0, Integer.MAX_VALUE));
	}

	@Override
	public User update(User user) throws NoUserException {
		if (!this.userDAO.exists(user.getId()))
			throw new NoUserException(user.getLogin());
		User original = this.userDAO.findOne(user.getId());
		if (user.isActivated() && !original.isActivated()) {
			original.setActivated(true);
		}
		if (user.isAdmin() != original.isAdmin()) {
			original.setAdmin(user.isAdmin());
		}
		if (!original.getEmail().equals(user.getEmail())) {
			original.setEmail(user.getEmail());
		}
		return this.userDAO.save(original);
	}

	@Override
	public User createUser(UserRegistration form) throws EmailAlreadyRegisteredException, UserAlreadyExistsException, RegistrationEmailNotSentException {
		if (getByLogin(form.getLogin()) != null)
			throw new UserAlreadyExistsException();
		if (getByEmail(form.getEmail()) != null)
			throw new EmailAlreadyRegisteredException(form.getEmail());
		User newUser = this.userDAO.save(new User(form.getLogin(), form.getEmail(), encoder.encode(form.getPassword())));
		Map<String, String> map = new Hashtable<String, String>();
		map.put("uuid", newUser.getToken());
		try {
			map.put("host", InetAddress.getLocalHost().getHostAddress());
		} catch (UnknownHostException e1) {
			log4j.error("Unable to retrieve localhost address");
		}
		try {
			mailService.send(newUser.getEmail(), "Confirm account registration", mailService.prepareTemplate(HTML_TEMPLATE, "html", map));
		} catch (MessagingException e) {
			log4j.error("Could not send message to registering user");
			throw new RegistrationEmailNotSentException();
		}
		return newUser;
	}

	@Override
	public void activateUser(String token) throws NoUserException, UserAlreadyActivatedException {
		User user = userDAO.findByToken(token);
		if (user == null)
			throw new NoUserException(token);
		if (user.isActivated())
			throw new UserAlreadyActivatedException();
		user.setActivated(true);
		userDAO.save(user);
	}

	@Override
	public UserDTO getByLoginDTO(String name) {
		return mapper.map(this.getByLogin(name), UserDTO.class);
	}

}
