package pl.pw.rb.eng.service.impl;

import static java.text.MessageFormat.format;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

import pl.pw.rb.eng.db.dao.DatabaseDAO;
import pl.pw.rb.eng.db.dto.SimpleDatabaseDTO;
import pl.pw.rb.eng.db.entities.Database;
import pl.pw.rb.eng.db.entities.UploadFile;
import pl.pw.rb.eng.scan.db.DatabaseDownloader;
import pl.pw.rb.eng.service.DatabaseService;
import pl.pw.rb.eng.service.UploadFileService;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.service.exception.DatabaseAlreadyDownloadingException;
import pl.pw.rb.eng.service.exception.DatabaseAlreadyExistsException;
import pl.pw.rb.eng.service.exception.MissingFileUploadException;
import pl.pw.rb.eng.web.dto.DatabaseDownloadRequest;
import pl.pw.rb.eng.web.servlet.dto.DatabaseDTO;
import pl.pw.rb.eng.web.servlet.dto.DatabaseUpdateDTO;
import pl.pw.rb.eng.web.servlet.exception.db.DatabaseDownloadException;
import pl.pw.rb.eng.web.servlet.exception.db.MissingDatabaseException;

@Service
class DatabaseServiceImpl implements DatabaseService {

	private Logger log4j = Logger.getLogger(DatabaseServiceImpl.class);

	@Autowired
	ModelMapper mapper;

	@Autowired
	private DatabaseDAO databaseDAO;

	@Autowired
	private SimpMessagingTemplate template;

	@Autowired
	private UsersService usersService;

	@Autowired
	private UploadFileService uploadFileService;

	@Autowired
	private DatabaseDownloader downloader;

	private final Map<String, Future<Database>> ongoing = new ConcurrentHashMap<>(0);

	@Autowired
	private File databaseDir;

	@Override
	public Collection<Database> getDatabases(boolean downloading) {
		if (!downloading)
			return Lists.newArrayList(databaseDAO.findDatabaseByDownloading(downloading));
		return Lists.newArrayList(databaseDAO.findAll());
	}

	public Database getDatabase(Integer id) {
		return databaseDAO.findOne(id);
	}

	public Database getDatabase(String name) {
		if (downloader.isDownloading(name)) {
			try {
				return ongoing.get(name).get();
			} catch (InterruptedException | ExecutionException e) {
				return null;
			}
		}
		return databaseDAO.findByName(name);
	}

	@Override
	public void delete(Integer id) throws MissingDatabaseException {
		Database db = databaseDAO.findOne(id);
		if (db != null) {
			databaseDAO.delete(db);
			final File dbDir = new File(db.getPath()).getParentFile();
			try {
				FileUtils.deleteDirectory(dbDir);
			} catch (IOException e) {
				log4j.warn("An error occurred while deleting peptides' database directory; postponing the action");
				Arrays.stream(dbDir.listFiles()).forEach(f -> f.deleteOnExit());
				dbDir.deleteOnExit();
			}
		} else {
			throw new MissingDatabaseException(id);
		}
	}

	@Override
	public Database update(int id, DatabaseUpdateDTO request) throws MissingDatabaseException {
		final Database db = databaseDAO.findOne(id);
		if (db == null)
			throw new MissingDatabaseException(id);
		mapper.map(request, db);
		return databaseDAO.save(db);
	}

	@Override
	public CompletableFuture<Database> redownload(final int databaseID) throws MissingDatabaseException, DatabaseAlreadyDownloadingException  {
		Database database = databaseDAO.findOne(databaseID);
		if (database == null)
			throw new MissingDatabaseException(databaseID);
		return this.redownload(database);
	}

	@Override
	public CompletableFuture<Database> redownload(final String databaseName) throws MissingDatabaseException, DatabaseAlreadyDownloadingException  {
		Database db = databaseDAO.findByName(databaseName);
		if (db == null)
			throw new MissingDatabaseException(databaseName);
		return this.redownload(db);
	}

	private CompletableFuture<Database> redownload(Database db) throws DatabaseAlreadyDownloadingException {
		if (downloader.isDownloading(db.getName()))
			throw new DatabaseAlreadyDownloadingException(db);
		final CompletableFuture<File> task = downloader.requestDownload(db.getName(), db.getUrl());
		final CompletableFuture<Database> future = new CompletableFuture<Database>();
		this.ongoing.put(db.getName(), future);
		db.setDownloading(true);
		final Database temporal = databaseDAO.save(db);
		task.handleAsync((t, u) -> {
			try {
				final File destination = new File(databaseDir, temporal.getName());
				FileUtils.moveFileToDirectory(t, destination, true);
				temporal.setPath(new File(destination, t.getName()).getAbsolutePath());
				temporal.setDownloaded();
				temporal.setSize(t.length());
				final Database finished = databaseDAO.save(temporal);
				future.complete(finished);
				return finished;
			} catch (IOException e) {
				log4j.error("Couldn't move database file to peptides' directory", e);
			}
			return temporal;
		});
		task.exceptionally(t -> {
			log4j.error("External peptides' database could not be redownloaded", t);
			future.completeExceptionally(t);
			return null;
		});
		future.whenComplete((database, exception) -> {
			ongoing.remove(db.getName());
		});
		return future;
	}

	@Override
	public CompletableFuture<Database> create(String user, final DatabaseDownloadRequest object)
			throws DatabaseDownloadException, DatabaseAlreadyExistsException, DatabaseAlreadyDownloadingException {
		String name = object.getName();
		String url = object.getUrl();
		String regexId = object.getRegexId();
		String regexName = object.getRegexName();
		String description = object.getDescription();
		return this.create(user, name, description, url, regexId, regexName);
	}

	@Override
	public CompletableFuture<Database> create(String user, String name, String description, String url)
			throws DatabaseDownloadException, DatabaseAlreadyExistsException, DatabaseAlreadyDownloadingException {
		return this.create(user, name, description, url, null, null);
	}

	@Override
	public CompletableFuture<Database> create(String user, String name, String description, String url, String regexId, String regexName)
			throws DatabaseDownloadException, DatabaseAlreadyExistsException, DatabaseAlreadyDownloadingException {
		if (this.databaseDAO.findByName(name) != null)
			throw new DatabaseAlreadyExistsException(name);
		final CompletableFuture<File> task = downloader.requestDownload(name, url);
		final CompletableFuture<Database> future = new CompletableFuture<Database>();
		ongoing.put(name, future);
		final Database db = new Database();
		db.setUser(usersService.getByLogin(user));
		db.setName(name);
		db.setRegexId(regexId);
		db.setRegexName(regexName);
		db.setDescription(description);
		db.setUrl(url);
		db.setDownloading(true);
		final Database temporal = databaseDAO.save(db);
		task.thenApplyAsync(t -> {
			try {
				final File destination = new File(databaseDir, temporal.getName());
				FileUtils.moveFileToDirectory(t, destination, true);
				final File dbFile = new File(destination, t.getName());
				temporal.setPath(dbFile.getAbsolutePath());
				temporal.setSize(dbFile.length());
				temporal.setDownloaded();
				temporal.setDownloading(false);
				final Database finished = databaseDAO.save(temporal);
				future.complete(finished);
				template.convertAndSend("/topic/db_upload/" + finished.getName(), mapper.map(finished, DatabaseDTO.class));
				return finished;
			} catch (IOException e) {
				log4j.error("Couldn't move database file to peptides' directory", e);
			}
			return temporal;
		});
		task.exceptionally((Throwable t) -> {
			log4j.error("Error downloading database in asynchronous task", t);
			future.completeExceptionally(t);
			return null;
		});
		future.whenComplete((database, exception) -> {
			ongoing.remove(name);
		});
		return future;
	}

	@Override
	public void importFrom(File dbDirectory) {
		final File[] databaseDirectories = dbDirectory.listFiles();
		if (!ArrayUtils.isEmpty(databaseDirectories))
			Arrays.asList(databaseDirectories).stream().filter(f -> f.isDirectory()).forEach(d -> {
				Database db = databaseDAO.findByName(d.getName());
				if (db == null) {
					final Database entity = new Database();
					entity.setName(d.getName());
					try {
						entity.setPath(DatabaseService.getLargestFileFrom(d).getAbsolutePath());
						entity.setUser(usersService.getByLogin("admin"));
						databaseDAO.save(entity);
					} catch (final NoSuchElementException e) {
						// ignore this error
						log4j.info(format("No file found in {0}", d.getAbsolutePath()));
					}
				}
			});

	}

	@Override
	public Database create(String user, DatabaseDownloadRequest object, String uploadedFileToken) throws MissingFileUploadException  {
		if (StringUtils.isBlank(uploadedFileToken))
			throw new MissingFileUploadException();
		final UploadFile uploadFile = uploadFileService.get(usersService.getByLogin(user), uploadedFileToken);
		if (uploadFile == null)
			throw new MissingFileUploadException();
		final Database database = new Database();
		mapper.map(object, database);
		database.setDownloading(false);
		database.setPath(uploadFile.getFilename());
		database.setSize(new File(uploadFile.getFilename()).length());
		return this.databaseDAO.save(database);
	}

	@Override
	public Collection<SimpleDatabaseDTO> getSimpleDatabasesInfo() {
		return this.databaseDAO.findAllProjectedBy();
	}

	@Override
	public void resetDownloading() {
		this.databaseDAO.resetAllDownloading();

	}
}
