package pl.pw.rb.eng.service.exception;

import static java.text.MessageFormat.format;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "No ongoing download with specified id")
@TranslationToken("ERR_NO_DOWNLOAD")
public class NoOngoingDatabaseDownload extends ServletException {

	private static final long serialVersionUID = -6369778008031609524L;

	public NoOngoingDatabaseDownload(String name) {
		super(format("No observer for {0} database download registered", name));
	}
}
