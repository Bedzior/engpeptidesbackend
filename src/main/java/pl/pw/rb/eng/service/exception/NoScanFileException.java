package pl.pw.rb.eng.service.exception;

import static java.text.MessageFormat.format;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "No file with given token had been uploaded")
@TranslationToken("ERR_FILE_NOT_FOUND")
public class NoScanFileException extends ServletException {

	private static final long serialVersionUID = 7459417749308468537L;

	public NoScanFileException(String token) {
		super(format("No file uploaded with specified token: {0}", token));
	}

}
