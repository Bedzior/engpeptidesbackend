package pl.pw.rb.eng.service.exception;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Requested upload file not found")
@TranslationToken("ERR_NO_FILE")
public class MissingFileUploadException extends ServletException {

	private static final long serialVersionUID = 5723827810721231080L;

}
