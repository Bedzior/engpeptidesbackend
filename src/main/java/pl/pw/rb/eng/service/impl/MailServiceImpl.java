package pl.pw.rb.eng.service.impl;

import static java.text.MessageFormat.format;

import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.text.StringSubstitutor;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import pl.pw.rb.eng.db.entities.EmailTemplate;
import pl.pw.rb.eng.db.entities.ScanResult;
import pl.pw.rb.eng.service.MailService;
import pl.pw.rb.eng.service.TemplateService;
import pl.pw.rb.eng.service.exception.MailTemplateException;

@Service
class MailServiceImpl implements MailService {

	private final EmailValidator validator = EmailValidator.getInstance();

	private final String TITLE_FORMAT = "Peptides Panel | {0}";

	@Autowired
	private TemplateService templates;

	@Autowired
	private JavaMailSender mailSender;

	@Override
	public String prepareTemplate(String name, String type, Map<String, String> values) throws MailTemplateException {
		final EmailTemplate template = templates.getTemplate(name, type);
		if (template == null)
			throw new MailTemplateException(format("{0}.{1}", name, type), this.getClass());
		String source = template.getBody();
		return StringSubstitutor.replace(source, values);
	}

	@Override
	public void send(String receiver, String subject, String content, Collection<ScanResult> files)
			throws MessagingException {
		if (!validator.isValid(receiver)) {
			throw new AddressException(format("{0} is an invalid e-mail address", receiver));
		}
		this.mailSender.send((MimeMessagePreparator) message -> {
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(receiver));
			message.setSubject(format(TITLE_FORMAT, subject));
			message.setSentDate(new Date());
			Multipart mime = new MimeMultipart();
			MimeBodyPart bodyPlain = new MimeBodyPart();
			bodyPlain.setText(content, StandardCharsets.UTF_8.name());
			MimeBodyPart bodyHtml = new MimeBodyPart();
			bodyHtml.setText(content, StandardCharsets.UTF_8.name(), "html");
			mime.addBodyPart(bodyPlain);
			mime.addBodyPart(bodyHtml);
			message.setContent(mime);
			message.getHeader("X-mailer");
			if (files != null && files.size() > 0) {
				for (ScanResult f : files) {
					MimeBodyPart attachment = new MimeBodyPart();
					DataSource source = new ByteArrayDataSource(f.getResult(), "application/gzip");
					attachment.setDataHandler(new DataHandler(source));
					attachment.setFileName(f.getName());
					mime.addBodyPart(attachment);
				}
			}
			message.setRecipients(RecipientType.TO, message.getAllRecipients());
		});

	}

	@Override
	public void send(String receiver, String subject, String content) throws MessagingException {
		this.send(receiver, subject, content, null);
	}

}
