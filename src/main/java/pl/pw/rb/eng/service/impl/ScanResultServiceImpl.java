package pl.pw.rb.eng.service.impl;

import static java.text.MessageFormat.format;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;

import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import mscanlib.common.MScanException;
import mscanlib.ms.mass.MassTools;
import mscanlib.ms.msms.MsMsPeptideHit;
import mscanlib.ms.msms.MsMsProteinHit;
import mscanlib.ms.msms.dbengines.mscandb.io.MScanDbOutFileReader;
import mscanlib.ms.msms.io.MsMsScanConfig;
import pl.pw.rb.eng.db.dao.ScanResultDAO;
import pl.pw.rb.eng.db.entities.Scan;
import pl.pw.rb.eng.db.entities.ScanResult;
import pl.pw.rb.eng.service.DatabaseService;
import pl.pw.rb.eng.service.ScanResultService;
import pl.pw.rb.eng.service.ScannerService;
import pl.pw.rb.eng.service.exception.NoProteinFoundException;
import pl.pw.rb.eng.service.exception.NoSuchScanException;
import pl.pw.rb.eng.service.exception.NoSuchScanResultException;
import pl.pw.rb.eng.web.servlet.dto.scan.MsMsShortPeptideHit;
import pl.pw.rb.eng.web.servlet.dto.scan.MsMsShortProteinHit;
import pl.pw.rb.eng.web.servlet.dto.scan.MsOutputDTO;
import pl.pw.rb.eng.web.servlet.dto.scan.ScanConfiguration;
import pl.pw.rb.eng.web.servlet.exception.ScanResultReadException;
import pl.pw.rb.eng.web.servlet.scan.exception.OngoingScanException;

@Service
class ScanResultServiceImpl implements ScanResultService {

	private final Logger logger = LoggerFactory.logger(ScanResultServiceImpl.class);;

	@Autowired
	ScanResultDAO dao;

	@Autowired
	ScannerService scannerService;

	@Autowired
	DatabaseService databaseService;

	@Autowired
	File tempDir;

	@Autowired
	ModelMapper mapper;

	@Autowired
	ObjectMapper jacksonMapper;

	@PostConstruct
	void postConstruct() {
		mapper.addMappings(new PropertyMap<MScanDbOutFileReader, MsOutputDTO>() {

			@Override
			protected void configure() {
				map().setFileFormatName(source.getFileFormatName());
			}
		});
	}

	@Override
	public ScanResult create(String token, @NotNull File file) throws FileNotFoundException, IOException, CompressorException, NoSuchScanException {
		if (file == null) {
			throw new NullPointerException("\"file\" parameter should not be null");
		}
		if (!file.exists()) {
			throw new FileNotFoundException(format("No scan result file {0} found", file.getAbsolutePath()));
		}
		return dao.save(new ScanResult(scannerService.getSecure(token), file));
	}

	@Override
	public ScanResult getForScan(@NotNull String token) throws NoSuchScanException, OngoingScanException, NoSuchScanResultException {
		Scan scan = scannerService.get(token);
		if (scan == null)
			throw new NoSuchScanException(token);
		if (scan.isRunning())
			throw new OngoingScanException(token);
		ScanResult result = dao.findByScan(scan);
		if (result == null)
			throw new NoSuchScanResultException(token);
		return result;
	}

	@Override
	@Cacheable(ScanResultService.CACHE_NAME)
	public MsOutputDTO getResult(@NotNull String token) throws NoSuchScanException, OngoingScanException, ScanResultReadException, NoSuchScanResultException {
		try {
			File tempFile = getFileForScan(token);
			MScanDbOutFileReader reader = new MScanDbOutFileReader(tempFile.getAbsolutePath(), new MsMsScanConfig());
			reader.readFile();
			reader.closeFile();
			return mapper.map(reader, MsOutputDTO.class);
		} catch (MScanException | IOException e) {
			throw new ScanResultReadException(e);
		}
	}

	private File getFileForScan(String token) throws NoSuchScanException, OngoingScanException, NoSuchScanResultException, IOException, FileNotFoundException {
		File tempFile = null;
		try {
			final ScanResult result = this.getForScan(token);
			if (result == null)
				throw new NoSuchScanException(token);
			tempFile = new File(tempDir, format("tmp_{0}_{1}.txt", result.getName(), token));
			if (!tempFile.exists())
				try (FileOutputStream fos = new FileOutputStream(tempFile)) {
					IOUtils.write(result.getResult(), fos);
				}
			return tempFile;
		} finally {
			if (tempFile != null)
				tempFile.deleteOnExit();
		}
	}

	@Cacheable(ScanResultService.CACHE_NAME)
	private Map<String, MsMsProteinHit> _getProteinHashes(String token)
			throws NoSuchScanException, OngoingScanException, NoSuchScanResultException, ScanResultReadException {
		try {
			File tempFile = getFileForScan(token);
			MScanDbOutFileReader reader = new MScanDbOutFileReader(tempFile.getAbsolutePath(), new MsMsScanConfig());
			LinkedHashMap<String, MsMsProteinHit> proteinHashes = new LinkedHashMap<>();
			reader.readFile();
			reader.closeFile();
			reader.createHashes(proteinHashes, new LinkedHashMap<>());
			return proteinHashes;
		} catch (MScanException | IOException e) {
			throw new ScanResultReadException(e);
		}
	}

	@Override
	public Map<String, MsMsShortProteinHit> getProteinHashes(String token)
			throws NoSuchScanException, OngoingScanException, NoSuchScanResultException, ScanResultReadException {
		Map<String, MsMsProteinHit> proteinHashes = _getProteinHashes(token);
		Map<String, MsMsShortProteinHit> map = new HashMap<String, MsMsShortProteinHit>(proteinHashes.size());
		proteinHashes.forEach((key, value) -> map.put(key, mapper.map(value, MsMsShortProteinHit.class)));
		try {
			Scan scan = scannerService.getSecure(token);
			ScanConfiguration config = jacksonMapper.readValue(scan.getParameters(), ScanConfiguration.class);
			if (config.getData() != null) {
				String regexName = databaseService.getDatabase(config.getData().getDatabase()).getRegexName();
				if (!StringUtils.isBlank(regexName) && regexName.contains("(") && regexName.contains(")")) {
					Pattern pattern = Pattern.compile(regexName);
					map.values().stream().forEach(p -> {
						Matcher matcher = pattern.matcher(p.getName());
						if (matcher.matches())
							p.setShortName(matcher.group(1));
					});
				}
			}
		} catch (IOException e) {
			// suppress, we are able to recover, no data has changed
			logger.warn(format("Unable to retrieve regular expression for protein name: error parsing scan {0} parameters", token));
		}
		return map;
	}

	@Override
	public Map<String, MsMsShortPeptideHit> getPeptideHashes(@NotNull String token, @NotNull String protein)
			throws NoSuchScanException, OngoingScanException, NoSuchScanResultException, ScanResultReadException, NoProteinFoundException {
		MsMsProteinHit proteinHit = this._getProteinHashes(token).get(protein.equals("unassigned") ? "N/A" : protein);
		if (proteinHit != null) {
			LinkedHashMap<String, MsMsPeptideHit> _peptides = proteinHit.getPeptides();
			Map<String, MsMsShortPeptideHit> map = new HashMap<String, MsMsShortPeptideHit>(_peptides.size());
			_peptides.forEach((key, value) -> {
				MsMsShortPeptideHit newValue = mapper.map(value, MsMsShortPeptideHit.class);
				newValue.getQueriesList().forEach(q -> {
					q.setDeltaPPM(MassTools.getDeltaPPM(q.getMass(), value.getCalcMass()));
				});
				map.put(key, newValue);
			});
			return map;
		} else {
			throw new NoProteinFoundException(token, protein);
		}
	}
}
