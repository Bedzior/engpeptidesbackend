package pl.pw.rb.eng.service.exception;

import static java.text.MessageFormat.format;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
@TranslationToken("ERR_RESULT_NOT_FOUND")
public class NoSuchScanResultException extends ServletException {

	private static final long serialVersionUID = 7354962432676270874L;

	public NoSuchScanResultException(String token) {
		super(format("No scan result found for scan {0}", token));
	}

}
