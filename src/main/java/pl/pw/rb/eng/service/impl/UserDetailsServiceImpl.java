package pl.pw.rb.eng.service.impl;

import static java.text.MessageFormat.format;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import pl.pw.rb.eng.service.UsersService;

/**
 * This class fulfills Spring Security's need for a user repository, effectively
 * providing user details to the container.
 * 
 * @author Rafał Będźkowski
 */
@Service
class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	UsersService usersService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		pl.pw.rb.eng.db.entities.User user = usersService.getByLogin(username);
		if (user != null) {
			if (!user.isActivated()) {
				throw new UsernameNotFoundException(format("User {0} has not been activated", username));
			}
			// @formatter:off
			final UserDetails userDetails = 
					User.withUsername(username)
						.password(user.getHash())
						.roles(user.getRoles())
						.authorities(user.getAuthorities())
						.build();
			// @formatter:on
			return userDetails;
		}
		throw new UsernameNotFoundException(format("User {0} doesn''t exist", username));
	}

}
