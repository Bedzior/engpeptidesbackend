package pl.pw.rb.eng.service.exception;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.UNAUTHORIZED, reason = "Missing user info")
@TranslationToken("ERR_NO_USER")
public class NullUserException extends ServletException {

	private static final long serialVersionUID = 1L;

	public NullUserException() {
		super();
	}

}
