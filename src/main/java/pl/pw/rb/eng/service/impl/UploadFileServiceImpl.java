package pl.pw.rb.eng.service.impl;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.pw.rb.eng.db.dao.UploadFileDAO;
import pl.pw.rb.eng.db.entities.UploadFile;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.service.UploadFileService;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.service.exception.NoScanFileException;

@Service
class UploadFileServiceImpl implements UploadFileService {

	@Autowired
	UploadFileDAO dao;

	@Autowired
	UsersService usersService;

	@Override
	public UploadFile create(String user, File file) {
		final UploadFile entity = new UploadFile();
		entity.setUser(usersService.getByLogin(user));
		entity.setFilename(file.getAbsolutePath());
		return dao.save(entity);
	}

	@Override
	public UploadFile get(User user, String token) {
		return dao.findByUserAndToken(user, token);
	}

	@Override
	public File getFile(User user, String token) throws NoScanFileException {
		final UploadFile uploadFile = this.get(user, token);
		if (uploadFile == null)
			throw new NoScanFileException(token);
		return new File(uploadFile.getFilename());
	}

}
