package pl.pw.rb.eng.service;

import java.io.File;

import org.springframework.security.access.prepost.PreAuthorize;

import pl.pw.rb.eng.db.entities.UploadFile;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.service.exception.NoScanFileException;

/**
 * 
 * @author Rafał Będźkowski
 *
 */
public interface UploadFileService {

	/**
	 * Creates a database entry with an uploaded file, to be used with peptide scan
	 * request.
	 * 
	 * @param user the uploading user
	 * @param file the uploaded file to read content from
	 * @return {@link UploadFile} entity created in database
	 */
	UploadFile create(String user, File file);

	/**
	 * Retrieves an uploaded file by the uploader, his session and a unique token.
	 * All of this is to decline unauthorized uploaded file access.
	 * 
	 * @param user  the uploading/requesting user
	 * @param token unique token of the particular file
	 * @return
	 */
	@PreAuthorize("principal.name == #user.login")
	UploadFile get(User user, String token);

	/**
	 * Retrieves the uploaded file as saved on the file system
	 * 
	 * @param user  the requesting user
	 * @param token the file identifier
	 * @return
	 * @throws NoUploadedFileException in case the requested file does not exist or
	 *                                 the
	 */
	File getFile(User user, String token) throws NoScanFileException;

}
