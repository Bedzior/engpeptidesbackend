package pl.pw.rb.eng.service;

import java.util.Collection;

import pl.pw.rb.eng.web.servlet.dto.scan.MMDUnit;
import pl.pw.rb.eng.web.servlet.dto.scan.PTM;

public interface ScanConfigurationService {

	/**
	 * @return the available molecular mass distributions units
	 */
	Collection<MMDUnit> getMMDUnits();

	/**
	 * @return a full list of post-translational modifications
	 */
	Collection<PTM> getAvailablePTMs();

	/***
	 * return a JSON array of enzymes available for configuration
	 * 
	 * @param specificOnly
	 * @return
	 */
	Collection<String> getEnzymes(boolean specificOnly);

	Collection<String> getFragmentationRules();

}
