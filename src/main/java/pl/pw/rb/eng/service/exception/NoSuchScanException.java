package pl.pw.rb.eng.service.exception;

import static java.text.MessageFormat.format;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Scan with the specified token not found or the user is not authorized to view the requested resource")
@TranslationToken("ERR_SCAN_NOT_FOUND_OR_UNAUTHORIZED")
public class NoSuchScanException extends ServletException {

	private static final long serialVersionUID = 534473795171479604L;

	public NoSuchScanException(String token) {
		super(format("Scan {0} does not exist", token));
	}
}
