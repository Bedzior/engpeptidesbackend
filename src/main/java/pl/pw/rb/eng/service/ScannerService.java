package pl.pw.rb.eng.service;

import java.security.Principal;
import java.util.Collection;
import java.util.Comparator;

import javax.validation.constraints.NotNull;

import org.springframework.security.access.prepost.PreAuthorize;

import mscanlib.ms.msms.dbengines.DbEngine;
import pl.pw.rb.eng.db.entities.Scan;
import pl.pw.rb.eng.db.entities.ScanStatus;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.scan.ScannerProgressEndpointController;
import pl.pw.rb.eng.scan.exception.DbConfigurationException;
import pl.pw.rb.eng.scan.exception.NoSuchOngoingScanException;
import pl.pw.rb.eng.service.exception.NoScanFileException;
import pl.pw.rb.eng.service.exception.NoSuchScanException;
import pl.pw.rb.eng.service.exception.NullUserException;
import pl.pw.rb.eng.web.servlet.dto.scan.ScanConfiguration;

/**
 * Represents the facade for configuring, getting the status and controlling the
 * execution of a {@link DbEngine} peptide scan.
 * 
 * Please refer to specific methods for further documentation.
 * 
 * @author Rafał Będźkowski
 *
 */
public interface ScannerService {

	/**
	 * Instantiates and schedules a peptide scan. Further communication with a web
	 * user happens over WebSocket, through a dedicated controller (see
	 * {@link ScannerProgressEndpointController}).
	 * 
	 * @param user              ID of the client requesting the scan
	 * @param sessionID         client's session ID for referencing local resources
	 *                          (namely, uploaded files)
	 * @param jsonConfiguration scan configuration stemming from a web-based editor
	 * @return scan's identification string
	 * @throws DbConfigurationException
	 * @throws NullUserException 
	 * @throws NoScanFileException 
	 * @throws NoUploadedFileException  in case when, during the current session, a
	 *                                  file specified in the
	 *                                  {@code jsonConfiguration}'s
	 *                                  {@code other.file(s)} had not been actually
	 *                                  uploaded
	 */
	public Scan createFor(User user, String sessionID, ScanConfiguration configuration) throws DbConfigurationException, NullUserException, NoScanFileException;

	/**
	 * Retrieves the {@link DbEngine} associated with the specified scan
	 * 
	 * @param token the scan's ID string
	 * @return engine associated with the token
	 */
	public DbEngine getEngine(@NotNull String token);

	/**
	 * Orders a specific scan to asynchronously finish its execution, interrupting
	 * if necessary.
	 * 
	 * @param token the scan's ID string
	 * @throws NoSuchOngoingScanException in case there is no such scan in progress
	 *                                    or the specified scan had finished already
	 */
	@PreAuthorize("hasRole('ADMIN') or @scansDAO.findOne(#token).user.login == authentication.name")
	public void stopTask(@NotNull String token) throws NoSuchOngoingScanException;

	/**
	 * Informs the user if a task identified with the token provided is running.
	 * 
	 * @throws NoSuchScanException
	 */
	@PreAuthorize("hasRole('ADMIN') or @scansDAO.findOne(#token).user.login == authentication.name")
	public boolean isRunning(@NotNull String token) throws NoSuchScanException;

	/**
	 * Retrieves a full list of scanning tasks run by the provided {@link User}
	 * 
	 * @param user
	 * @return
	 */
	public Collection<Scan> getForUser(@NotNull User user);

	/**
	 * Retrieves a full list of peptide scan tasks run by the {@link Principal}
	 * 
	 * @param user
	 * @return a list of all user's scans
	 */
	public Collection<Scan> getForUser(@NotNull Principal user);

	/**
	 * Retrieves a list of either running or finished peptide scan tasks initiated
	 * by the {@link Principal}
	 * 
	 * @param user
	 * @param finished if <code>true</code>, returns only the already finished tasks
	 * @return a list of either waiting/running or finished scans
	 */
	public Collection<Scan> getForUser(Principal user, boolean finished);

	public Collection<Scan> getForUser(User user, boolean finished);

	/**
	 * Retrieves a single Scan, given that it exists and the {@link Principal} is
	 * eligible
	 * 
	 * @param token identifier of the requested {@link Scan} object
	 * @return the requested scan, if existent and the user is eligible
	 * @throws NoSuchScanException if the requested scan doesn't exist or the user
	 *                             is not eligible to retrieve it
	 */
	@PreAuthorize("hasRole('ADMIN') or @scansDAO.findOne(#token).user.login == authentication.name")
	public Scan get(@NotNull String token) throws NoSuchScanException;

	/**
	 * Retrieves a single Scan, given that it exists
	 * 
	 * @param token identifier of the requested {@link Scan} object
	 * @return the requested scan, if existent
	 * @throws NoSuchScanException if the requested scan doesn't exist
	 */
	public Scan getSecure(@NotNull String token) throws NoSuchScanException;

	/**
	 * Retrieves the tasks' priority {@link Comparator} used by the service
	 * implementation.
	 */
	public Comparator<Runnable> getTaskComparator();

	/**
	 * Sets the status of unfinished jobs to {@link ScanStatus#WAITING} and repopulates the scan task scheduler.<br/>
	 * <br/>
	 * Used after server start
	 */
	public void resetRunning();

	/**
	 * 
	 * @param status
	 * @return count of scans in the requested status
	 */
	public int count(ScanStatus status);

}
