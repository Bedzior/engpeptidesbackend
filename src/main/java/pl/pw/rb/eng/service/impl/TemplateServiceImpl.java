package pl.pw.rb.eng.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

import com.google.common.collect.Lists;

import pl.pw.rb.eng.db.dao.TemplateDAO;
import pl.pw.rb.eng.db.entities.EmailTemplate;
import pl.pw.rb.eng.service.TemplateService;

@Service
class TemplateServiceImpl implements TemplateService {

	@Autowired
	private TemplateDAO dao;

	@Override
	public void populateFromResources() throws IOException {
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		for (Resource resource : resolver.getResources("/templates/*")) {
			final String[] filenameParts = resource.getFilename().split("\\.");
			final String name = filenameParts[0];
			final String type = filenameParts[1];
			EmailTemplate template = new EmailTemplate(name, type);
			try (InputStream is = resource.getInputStream()) {
				template.setBody(StreamUtils.copyToString(is, StandardCharsets.UTF_8));
			}
			dao.save(template);
		}
	}

	@Override
	public EmailTemplate update(EmailTemplate template) {
		return dao.save(template);
	}

	@Override
	public EmailTemplate getTemplate(String name, String type) {
		return dao.findByNameAndType(name, type);
	}

	@Override
	public EmailTemplate getTemplate(int id) {
		return dao.findOne(id);
	}

	@Override
	public Collection<EmailTemplate> list() {
		try {
			final Iterable<EmailTemplate> all = dao.findAll();
			return Lists.newArrayList(all);
		} catch (Exception e) {
			return null;
		}
	}

}
