package pl.pw.rb.eng.service.exception;

import static java.text.MessageFormat.format;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Database with this name already exists")
@TranslationToken("ERR_EXISTS")
public class DatabaseAlreadyExistsException extends ServletException {

	public DatabaseAlreadyExistsException(String name) {
		super(format("Database with this name already exists: {0}", name));
	}

	private static final long serialVersionUID = 5256252081856238428L;

}
