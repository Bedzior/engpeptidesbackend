package pl.pw.rb.eng.service.impl;

import static java.text.MessageFormat.format;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.configurationmetadata.ConfigurationMetadataProperty;
import org.springframework.boot.configurationmetadata.ConfigurationMetadataRepositoryJsonBuilder;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import pl.pw.rb.eng.service.AdminConfigurationService;
import pl.pw.rb.eng.service.exception.MissingConfigurationValueException;
import pl.pw.rb.eng.web.dto.Configuration;
import pl.pw.rb.eng.web.servlet.admin.exception.ConfigurationReadException;
import pl.pw.rb.eng.web.servlet.admin.exception.ConfigurationUpdateException;
import pl.pw.rb.eng.web.servlet.admin.exception.NoSuchConfigurationException;

@Service
class AdminConfigurationServiceImpl implements AdminConfigurationService {

	@Value("${peptides.application-name}.")
	private String propertyPrefix;

	@Autowired
	private Environment env;

	@Autowired
	private File confDir;

	@Override
	@Cacheable(key = "configurations")
	public List<Configuration> list() throws ConfigurationReadException {
		Map<String, ConfigurationMetadataProperty> allProperties;
		try {
			allProperties = getAdditionalPropertiesMetadata();
		} catch (IOException e) {
			throw new ConfigurationReadException();
		}
		List<Configuration> configurations = allProperties.keySet().stream().filter(p -> {
			return p.startsWith(propertyPrefix);
		}).map(c -> {
			return createConfigurationPropertyObject(allProperties, c);
		}).collect(Collectors.toList());
		configurations.removeIf(t -> t == null);
		return configurations;
	}

	@Override
	public Configuration get(@NotNull String name) throws NoSuchConfigurationException, ConfigurationReadException {
		if (name == null)
			throw new NoSuchConfigurationException();
		if (!env.containsProperty(propertyPrefix + name))
			throw new NoSuchConfigurationException(name);
		try {
			return list().stream().filter(c -> c.getName().endsWith(name)).findFirst().get();
		} catch (final NoSuchElementException e) {
			throw new NoSuchConfigurationException(name);
		}
	}

	@Override
	public Configuration update(@NotNull Configuration config)
			throws NoSuchConfigurationException, MissingConfigurationValueException, ConfigurationUpdateException {
		if (config == null)
			throw new NoSuchConfigurationException();
		return this.update(config.getName(), config.getValue());
	}

	@Override
	public Configuration update(@NotNull String name, @NotNull Object value)
			throws NoSuchConfigurationException, MissingConfigurationValueException, ConfigurationUpdateException {
		if (name == null)
			throw new NoSuchConfigurationException();
		if (value == null)
			throw new MissingConfigurationValueException(name);
		final String propertyName = propertyPrefix + name;
		if (!env.containsProperty(propertyName))
			throw new NoSuchConfigurationException(name);
		final File props = new File(confDir, "application.properties");
		try {
			if (!props.exists() || !props.isFile()) {
				props.getParentFile().mkdirs();
				props.createNewFile();
			}
			final Properties properties = new Properties();
			try (final FileInputStream fis = new FileInputStream(props)) {
				properties.load(fis);
			}
			properties.put(propertyName, value);
			try (final FileOutputStream fis = new FileOutputStream(props, false)) {
				String username = SecurityContextHolder.getContext().getAuthentication().getName();
				properties.store(fis, format("Last update by {0}, {1}", username, new Date()));
			}
			Configuration property = this.createConfigurationPropertyObject(getAdditionalPropertiesMetadata(), propertyName);
			property.setValue(value);
			return property;
		} catch (final IOException e) {
			throw new ConfigurationUpdateException();
		}
	}

	// TODO add cache
	@Cacheable(key = "properties-metadata")
	private final Map<String, ConfigurationMetadataProperty> getAdditionalPropertiesMetadata() throws IOException {
		ConfigurationMetadataRepositoryJsonBuilder builder = ConfigurationMetadataRepositoryJsonBuilder.create();
		for (Resource resource : new PathMatchingResourcePatternResolver().getResources("classpath*:META-INF/additional-spring-configuration-metadata.json")) {
			try (InputStream in = resource.getInputStream()) {
				builder.withJsonResource(in);
			}
		}
		return builder.build().getAllProperties();
	}

	private final Configuration createConfigurationPropertyObject(Map<String, ConfigurationMetadataProperty> allProperties, String name) {
		final ConfigurationMetadataProperty propertyMetadata = allProperties.get(name);
		final String type = propertyMetadata.getType();
		try {
			Object value = env.getProperty(name, Class.forName(type));
			final String shortName = name.substring(propertyPrefix.length());
			return new Configuration(shortName, propertyMetadata.getDescription(), value);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(format("Missing type declared for property type: {0}", type), e);
		}
	}

	@Override
	public Collection<Configuration> update(Collection<Configuration> configs) throws ConfigurationUpdateException {
		try {
			final File props;
			File currentDirProps = new File("application.properties");
			if (currentDirProps.exists() && currentDirProps.isFile()) {
				props = currentDirProps;
			} else {
				File configDirProps = new File("config", "application.properties");
				if (!configDirProps.exists() || !configDirProps.isFile()) {
					configDirProps.getParentFile().mkdirs();
					configDirProps.createNewFile();
				}
				props = configDirProps;
			}
			final Properties properties = new Properties();
			try (final FileInputStream fis = new FileInputStream(props)) {
				properties.load(fis);
			}
			for (Iterator<Configuration> it = configs.iterator(); it.hasNext();) {
				Configuration config = it.next();
				final String propertyName = propertyPrefix + config.getName();
				if (env.containsProperty(propertyName))
					properties.put(propertyName, config.getValue().toString());
				else
					it.remove();
			}
			try (final FileOutputStream fos = new FileOutputStream(props, false)) {
				String username = SecurityContextHolder.getContext().getAuthentication().getName();
			//@formatter:off
			properties.store(fos, format("Last update by {0}: {1}", username, 
					properties.keySet().stream().map(t -> t.toString()).collect(Collectors.joining(", "))
			));
			//@formatter:on
			}
			return configs;
		} catch (IOException e) {
			throw new ConfigurationUpdateException();
		}
	}

}
