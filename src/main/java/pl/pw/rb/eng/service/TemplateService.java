package pl.pw.rb.eng.service;

import java.io.IOException;
import java.util.Collection;

import pl.pw.rb.eng.db.entities.EmailTemplate;

public interface TemplateService {

	/**
	 * Retrieves e-mail template by name
	 * 
	 * @param name name of the desired e-mail message template
	 * @param type
	 * @return the requested template
	 */
	public EmailTemplate getTemplate(String name, String type);

	/**
	 * Retrieves e-mail template by ID
	 * 
	 * @param id the template's ID
	 * @return the requested template
	 */
	public EmailTemplate getTemplate(int id);

	/**
	 * Updates an existing e-mail message template
	 * 
	 * @param template the template to update, with its fields edited
	 * @return the template provided, as returned by the database backend (a new
	 *         instance)
	 */
	public EmailTemplate update(EmailTemplate template);

	public void populateFromResources() throws IOException;

	public Collection<EmailTemplate> list();

}
