package pl.pw.rb.eng.service;

import java.util.Collection;
import java.util.Map;

import javax.mail.MessagingException;

import pl.pw.rb.eng.db.entities.ScanResult;
import pl.pw.rb.eng.service.exception.MailTemplateException;

/**
 * Simple mail service for handling all the mail sending logic.
 * 
 * @author Rafał Będźkowski
 *
 */
public interface MailService {

	/**
	 * 
	 * @param name   name of the resource
	 * @param type   file extension for the resource
	 * @param values values to be put into the parsed template's placeholders
	 * @return a prepared e-mail message to be sent by the client
	 * @throws MailTemplateException in case the specified template has not been
	 *                               found
	 */
	public String prepareTemplate(String name, String type, Map<String, String> values) throws MailTemplateException;

	/**
	 * 
	 * @param receiver    e-mail address of the recipient
	 * @param subject     title of the message
	 * @param content
	 * @param resultFiles
	 * @throws MessagingException
	 */
	public void send(String receiver, String subject, String content, Collection<ScanResult> resultFiles)
			throws MessagingException;

	/**
	 * 
	 * @param receiver e-mail address of the recipient
	 * @param subject  title of the message
	 * @param content
	 * @throws MessagingException
	 */
	public void send(String receiver, String subject, String content) throws MessagingException;

}
