package pl.pw.rb.eng.service;

import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

import javax.validation.constraints.NotNull;

import pl.pw.rb.eng.db.dto.SimpleDatabaseDTO;
import pl.pw.rb.eng.db.entities.Database;
import pl.pw.rb.eng.service.exception.DatabaseAlreadyDownloadingException;
import pl.pw.rb.eng.service.exception.DatabaseAlreadyExistsException;
import pl.pw.rb.eng.service.exception.MissingFileUploadException;
import pl.pw.rb.eng.web.dto.DatabaseDownloadRequest;
import pl.pw.rb.eng.web.servlet.dto.DatabaseUpdateDTO;
import pl.pw.rb.eng.web.servlet.exception.db.DatabaseDownloadException;
import pl.pw.rb.eng.web.servlet.exception.db.MissingDatabaseException;

public interface DatabaseService {

	/**
	 * Lists all databases
	 * 
	 * @param getDownloading if set to <code>true</code>, returns a complete list of
	 *                       peptide databases within application's database; if set
	 *                       to <code>false</code>, returns a list of databases that
	 *                       can be used
	 * 
	 * @return a full list of available databases (unpaged)
	 */
	Collection<Database> getDatabases(boolean getDownloading);

	/**
	 * Retrieves database by ID
	 * 
	 * @param id database's identification number
	 * @return the desired database
	 */
	Database getDatabase(Integer id);

	/**
	 * Retrieves database by name. In case the database is currently being
	 * downloaded, an associated {@link Future} will be used to evaluate the final
	 * value.
	 * 
	 * @param name database's unique name
	 * @return the desired database
	 */
	Database getDatabase(String name);

	/**
	 * Permanently deletes a peptide database row from application's database,
	 * alongside its files
	 * 
	 * @param id the identifier of the database to be deleted
	 * @throws MissingDatabaseException 
	 */
	void delete(Integer id) throws MissingDatabaseException;

	/**
	 * Creates a deferred, future {@link Database}, whose real creation will be
	 * announced by this {@link Future}'s result
	 * 
	 * @param user    the creating user's name
	 * @param request extended database's data
	 * @return future {@link Database} object
	 * @throws DatabaseDownloadException
	 * @throws DatabaseAlreadyDownloadingException
	 */
	CompletableFuture<Database> create(String user, DatabaseDownloadRequest request)
			throws DatabaseDownloadException, DatabaseAlreadyExistsException, DatabaseAlreadyDownloadingException;

	CompletableFuture<Database> create(String user, String name, String description, String url)
			throws DatabaseDownloadException, DatabaseAlreadyExistsException, DatabaseAlreadyDownloadingException;

	CompletableFuture<Database> create(String user, String name, String description, String url, String regexId, String regexName)
			throws DatabaseDownloadException, DatabaseAlreadyExistsException, DatabaseAlreadyDownloadingException;

	/**
	 * Creates a peptides' database from an uploaded file directly
	 * 
	 * @param user              the creating user's name
	 * @param json              extended database's data
	 * @param uploadedFileToken the uploaded file's database token
	 * @return newly created {@link Database} object populating the internal
	 *         database
	 * @throws MissingFileUploadException 
	 * @ in case of any issues finding the uploaded file or
	 *                          adding the {@link Database} whatsoever
	 */
	Database create(String user, DatabaseDownloadRequest request, String uploadedFileToken) throws MissingFileUploadException ;

	/**
	 * Orders a redownload of a previously specified FASTA database.
	 * 
	 * @param databaseName
	 * @throws MissingDatabaseException 
	 * @throws DatabaseAlreadyDownloadingException 
	 * @
	 */
	public CompletableFuture<Database> redownload(String databaseName) throws MissingDatabaseException, DatabaseAlreadyDownloadingException ;

	/**
	 * Orders a redownload of a previously specified FASTA database.
	 * 
	 * @param databaseID
	 * @throws MissingDatabaseException 
	 * @throws DatabaseAlreadyDownloadingException 
	 * @
	 */
	public CompletableFuture<Database> redownload(int databaseID) throws MissingDatabaseException, DatabaseAlreadyDownloadingException ;

	/**
	 * Retrieves simple database data required by configuration servlet
	 * 
	 * @return list of available databases
	 */
	Collection<SimpleDatabaseDTO> getSimpleDatabasesInfo();

	Database update(int id, DatabaseUpdateDTO request) throws MissingDatabaseException;

	void importFrom(File dbDirectory);

	/**
	 * Resets all databases in downloading state
	 */
	void resetDownloading();

	/**
	 * Retrieves the largest file in given directory. If the input is a file, it's
	 * returned immediately.
	 * 
	 * @param directory
	 * @return
	 */
	static File getLargestFileFrom(@NotNull File directory) {
		if (directory.isDirectory())
			return Arrays.asList(directory.listFiles()).stream().max((f1, f2) -> Long.compare(f1.length(), f2.length())).get();
		else
			return directory;
	}

}
