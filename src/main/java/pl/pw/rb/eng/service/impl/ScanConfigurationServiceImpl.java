package pl.pw.rb.eng.service.impl;

import static org.apache.commons.lang3.StringUtils.compare;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import mscanlib.common.MScanException;
import mscanlib.ms.mass.EnzymeMap;
import mscanlib.ms.mass.MassTools;
import mscanlib.ms.mass.PTMMap;
import mscanlib.ms.msms.MsMsFragmentationRulesMap;
import pl.pw.rb.eng.service.ScanConfigurationService;
import pl.pw.rb.eng.web.servlet.dto.scan.MMDUnit;
import pl.pw.rb.eng.web.servlet.dto.scan.PTM;

@Service
class ScanConfigurationServiceImpl implements ScanConfigurationService {

	@PostConstruct
	private final void init() {
		try {
			MassTools.initMaps();
		} catch (MScanException e) {
			throw new RuntimeException("Unable to initialize servlet: could not initialize mass tools maps", e);
		}
	}

	@Override
	public Collection<String> getEnzymes(boolean specificOnly) {
		return Arrays.asList(EnzymeMap.getNames(specificOnly));
	}

	@Override
	public Collection<String> getFragmentationRules() {
		return Arrays.asList(MsMsFragmentationRulesMap.getNames());
	}

	public Collection<MMDUnit> getMMDUnits() {
		final String[] unitNames = MassTools.getMMDUnitNames();
		List<MMDUnit> result = new ArrayList<>(unitNames.length);
		Arrays.stream(unitNames).forEach(unit -> result.add(new MMDUnit(unit)));
		return result;
	}

	@Override
	public Collection<PTM> getAvailablePTMs() {
		mscanlib.ms.mass.PTM[] ptms = PTMMap.getPTMs(true);
		List<PTM> modifications = new ArrayList<PTM>();
		for (mscanlib.ms.mass.PTM ptm : ptms) {
			String fullname = ptm.getFullName();
			modifications.add(new PTM(fullname, ptm.getName(), ptm.getName(), ptm.isHidden()));
		}
		modifications.sort((m1, m2) -> compare(m1.getShort(), m2.getShort()));
		return modifications;
	}

}
