package pl.pw.rb.eng.springconfig;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;

@Configuration
public class PeptidesWebMvcConfiguration extends WebMvcConfigurerAdapter {

	private static final String DATE_TIME_FORMAT_STRING = "yyyy-MM-dd HH:mm:ss";

	public static final DateFormat DATE_STRING_FORMAT = new SimpleDateFormat(DATE_TIME_FORMAT_STRING);

	@Value("password.secret")
	private String secret;

	@Bean
	@Primary
	public Jackson2ObjectMapperBuilder jacksonBuilder() {
		Jackson2ObjectMapperBuilder b = new Jackson2ObjectMapperBuilder();
		b.autoDetectFields(true);
		b.serializationInclusion(Include.ALWAYS);
		b.autoDetectGettersSetters(true);
		b.indentOutput(true);
		b.dateFormat(DATE_STRING_FORMAT);
		b.timeZone(TimeZone.getDefault());
		b.simpleDateFormat(DATE_TIME_FORMAT_STRING);
		b.featuresToEnable(
			SerializationFeature.WRITE_ENUMS_USING_TO_STRING,
			DeserializationFeature.READ_ENUMS_USING_TO_STRING);
		return b;
	}

	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		Jackson2ObjectMapperBuilder builder = jacksonBuilder();
		converters.add(new MappingJackson2HttpMessageConverter(builder.build()));
	}

	@Bean
	ServerProperties properties() {
		final ServerProperties properties = new ServerProperties();
		properties.setContextPath("/api");
		return properties;
	}

	@Bean
	public PasswordEncoder encoder() {
		return new Pbkdf2PasswordEncoder(secret);
	}

	@Bean
	public HttpSessionHandshakeInterceptor handshakeInterceptor() {
		return new HttpSessionHandshakeInterceptor();
	}

	@Override
	public void addCorsMappings(final CorsRegistry registry) {
		//@formatter:off
		registry.addMapping("/**")
			.allowedMethods(new String[] { 
				HttpMethod.GET.name(), 
				HttpMethod.POST.name(), 
				HttpMethod.PUT.name(), 
				HttpMethod.DELETE.name(),
				HttpMethod.OPTIONS.name(), 
				HttpMethod.HEAD.name() });
		//@formatter:on
	}

}
