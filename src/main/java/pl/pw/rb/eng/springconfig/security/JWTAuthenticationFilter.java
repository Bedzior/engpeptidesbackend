package pl.pw.rb.eng.springconfig.security;

import static java.text.MessageFormat.format;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import pl.pw.rb.eng.db.dto.UserDTO;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.service.UsersService;

// https://auth0.com/blog/implementing-jwt-authentication-on-spring-boot/
class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private final AuthenticationManager authManager;

	private final UsersService usersService;

	private final JWTHelper jwtHelper;

	private final ObjectMapper mapper;

	@JsonAutoDetect
	public static class AuthenticationObject {

		@JsonProperty("j_username")
		String username;

		@JsonProperty("j_password")
		String password;
	}

	JWTAuthenticationFilter(AuthenticationManager authenticationManager, UsersService usersService, JWTHelper jwtHelper, ObjectMapper mapper) {
		this.authManager = authenticationManager;
		this.usersService = usersService;
		this.jwtHelper = jwtHelper;
		this.mapper = mapper;
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		super.doFilter(req, res, chain);
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse rsp)
			throws AuthenticationException {
		try {
			final Map<String, String[]> parameters = req.getParameterMap();
			final String username, password;
			if (parameters.containsKey("username") && parameters.containsKey("password")) {
				username = req.getParameter("username");
				password = req.getParameter("password");
			} else {
				AuthenticationObject input = mapper.readValue(req.getReader(), AuthenticationObject.class);
				username = input.username;
				password = input.password;

			}
			User user = usersService.getByLogin(username);
			if (user == null)
				throw new AuthenticationCredentialsNotFoundException(format("No user {0} found", username));
			return authManager.authenticate(
					new UsernamePasswordAuthenticationToken(username, password, Arrays.asList(user.getAuthorities())));
		} catch (IOException e) {
			throw new InternalAuthenticationServiceException("Error reading credentials from user input", e);
		}
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication auth)
			throws IOException, ServletException {
		String token = Jwts.builder().setSubject(((org.springframework.security.core.userdetails.User) auth.getPrincipal()).getUsername())
				.setExpiration(new Date(System.currentTimeMillis() + jwtHelper.getExpiration()))
				.signWith(SignatureAlgorithm.HS512, jwtHelper.getSecret().getBytes()).compact();
		response.addHeader("Authorization", format("Bearer {0}", token));
		UserDTO user = usersService.getByLoginDTO(auth.getName());
		response.getWriter().write(new ObjectMapper().writeValueAsString(user));
	}

}
