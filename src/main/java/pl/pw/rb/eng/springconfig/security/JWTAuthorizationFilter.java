package pl.pw.rb.eng.springconfig.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

class JWTAuthorizationFilter extends BasicAuthenticationFilter {

	private final JWTHelper jwtHelper;

	JWTAuthorizationFilter(AuthenticationManager authenticationManager, JWTHelper jwtHelper) {
		super(authenticationManager);
		this.jwtHelper = jwtHelper;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		final String auth = request.getHeader(JWTHelper.HEADER);
		final String cookies = request.getHeader(JWTHelper.AUX_HEADER);
		UsernamePasswordAuthenticationToken authentication = null;
		if (StringUtils.startsWith(auth, JWTHelper.PREFIX)) {
			authentication = jwtHelper.getAuthentication(auth.replace(JWTHelper.PREFIX, ""));
		} else if (!StringUtils.isBlank(cookies)) {
			authentication = jwtHelper.getAuthenticationFromCookies(cookies);
		}
		if (authentication != null)
			SecurityContextHolder.getContext().setAuthentication(authentication);
		chain.doFilter(request, response);
	}

}
