package pl.pw.rb.eng.springconfig.security;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;

import javax.servlet.Filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.Http401AuthenticationEntryPoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.fasterxml.jackson.databind.ObjectMapper;

import static java.util.Arrays.asList;

import pl.pw.rb.eng.service.UsersService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService service;

	@Autowired
	private UsersService usersService;

	@Autowired
	private JWTHelper jwtHelper;

	@Autowired
	private ObjectMapper mapper;

	@Autowired
	private PasswordEncoder encoder;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(service).passwordEncoder(encoder);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// @formatter:off
		http.authorizeRequests()
			.antMatchers(POST, "/error", "/register", "/register/confirm", "/resetPassword").permitAll()
			.antMatchers(GET, "/user", "/resetPassword/request").permitAll()
			.antMatchers("/secured/admin/**").hasRole("ADMIN")
			.antMatchers("/secured/**").authenticated()
		.and()
			.addFilter(authenticationFilter())
			.addFilter(authorizationFilter())
			.sessionManagement().maximumSessions(1)
		.and()
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		.and()
			.logout().logoutUrl("/logout").permitAll()
		.and()
			.exceptionHandling()
			.authenticationEntryPoint(new Http401AuthenticationEntryPoint("Bearer realm=\"peptides\""))
		.and()
			.csrf().disable()//.ignoringAntMatchers("/ws/**", "/login")
//		.and()
			.cors();
		// @formatter:on
	}

	public Filter authorizationFilter() throws Exception {
		return new JWTAuthorizationFilter(authenticationManager(), jwtHelper);
	}

	public Filter authenticationFilter() throws Exception {
		return new JWTAuthenticationFilter(authenticationManager(), usersService, jwtHelper, mapper);
	}

	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		final CorsConfiguration corsConfig = new CorsConfiguration();
		corsConfig.setAllowedOrigins(asList(CorsConfiguration.ALL));
		//@formatter:off
		corsConfig.setAllowedMethods(
			asList(
				HttpMethod.GET.name(), 
				HttpMethod.POST.name(), 
				HttpMethod.PUT.name(), 
				HttpMethod.DELETE.name(),
				HttpMethod.OPTIONS.name(), 
				HttpMethod.HEAD.name()
			)
		);
		//@formatter:on
		corsConfig.setAllowedHeaders(asList(CorsConfiguration.ALL));
		source.registerCorsConfiguration("/**", corsConfig.applyPermitDefaultValues());
		corsConfig.addAllowedHeader("Access-Control-Allow-Origin");
		return source;
	}
}
