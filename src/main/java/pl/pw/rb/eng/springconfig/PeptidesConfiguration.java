package pl.pw.rb.eng.springconfig;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.modelmapper.config.Configuration.AccessLevel.PUBLIC;

import java.io.File;

import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@EnableConfigurationProperties
@EnableAutoConfiguration(exclude = HypermediaAutoConfiguration.class)
@Configuration
@PropertySource("classpath:application.properties")
@PropertySource(value = "file:${user.home}/${peptides.application-name}/config/application.properties", ignoreResourceNotFound = true, encoding = "UTF-8")
public class PeptidesConfiguration {

	private static final String TMPDIR = System.getProperty("java.io.tmpdir");

	private static final String HOME = System.getProperty("user.home");

	@Value("${peptides.application-name}")
	private String appName;

	@Bean
	FileAlterationMonitor monitor() {
		return new FileAlterationMonitor(SECONDS.toMillis(1));
	}

	@Bean
	ModelMapper modelMapper() {
		final ModelMapper mapper = new ModelMapper();
		final org.modelmapper.config.Configuration configuration = mapper.getConfiguration();
		configuration.setFieldAccessLevel(PUBLIC);
		configuration.setMethodAccessLevel(PUBLIC);
		return mapper;
	}

	@Bean
	public File tempDir() {
		File tempDir = new File(TMPDIR, appName);
		if (!tempDir.exists())
			tempDir.mkdirs();
		return tempDir;
	}

	@Bean
	public File confDir() {
		return new File(appDir(), "config");
	}

	@Bean
	public File appDir() {
		return new File(HOME, appName);
	}

	@Bean
	public File databaseDir() {
		return new File(appDir(), "databases");
	}

}
