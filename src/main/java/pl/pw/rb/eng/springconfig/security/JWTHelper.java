package pl.pw.rb.eng.springconfig.security;

import java.time.Duration;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.service.UsersService;

/**
 * Class managing different aspects of JWT token authentication.
 * 
 * @author Rafał Będźkowski
 *
 */
@Component
public final class JWTHelper {

	/**
	 * Pattern for localising the {@code Authentication} cookie and its subsequent
	 * JWT token
	 */
	private static final Pattern AUTH_PATTERN = Pattern.compile("Authentication=Bearer\\%20(.+)(\\;|$)");

	public static final String HEADER = "Authentication";

	public static final String PREFIX = "Bearer ";

	public static final String AUX_HEADER = "cookie";

	private JWTHelper() {
	}

	@Value("${jwt.secret}")
	private String secret;

	@Value("${peptides.jwt-expiration:1h}")
	private String tokenExpiration;

	private JwtParser parser;

	@Autowired
	private UsersService usersService;

	@PostConstruct
	private void configure() {
		this.parser = Jwts.parser().setSigningKey(secret.getBytes());
	}

	/**
	 * Retrieves authentication data from bearer string (probably a request header).
	 * 
	 * @param bearerString string containing the JWT token
	 * @return authenticated user, if any ({@code null} if none)
	 */
	public UsernamePasswordAuthenticationToken getAuthentication(@NotNull String bearerString) {
		if (StringUtils.isBlank(bearerString))
			return null;
		String login = parser.parseClaimsJws(bearerString).getBody().getSubject();
		if (login == null)
			return null;
		User user = usersService.getByLogin(login);
		if (user == null)
			return null;
		return new UsernamePasswordAuthenticationToken(login, null, Arrays.asList(user.getAuthorities()));
	}

	/**
	 * Retrieves authentication data from {@code Authentication} cookie (posted as
	 * header).
	 * 
	 * @param cookies string containing cookies data
	 * @return authenticated user, if any ({@code null} if none)
	 */
	public UsernamePasswordAuthenticationToken getAuthenticationFromCookies(String cookies) {
		final Matcher matcher = AUTH_PATTERN.matcher(cookies);
		if (!matcher.find())
			return null;
		String bearer = matcher.group(1);
		if (StringUtils.isBlank(bearer))
			return null;
		return this.getAuthentication(bearer);
	}

	/**
	 * Translates the {@code peptides.jwt-expiration} property (from
	 * <b>{@code application.properties}</b>) into a time {@code Duration} and
	 * returns the expiration time in milliseconds.
	 * 
	 * The default value
	 * 
	 * @return time for authentication expiration in milliseconds
	 */
	public long getExpiration() {
		final CharSequence parsableDuration = tokenExpiration.toLowerCase().startsWith("p") ? tokenExpiration : "p" + tokenExpiration;
		return Duration.parse(parsableDuration).toMillis();
	}

	/**
	 * Returns the {@code jwt.secret} value as defined in
	 * <b>{@code application.properties}</b>
	 * 
	 * @return the secret
	 */
	public String getSecret() {
		return secret;
	}

}
