package pl.pw.rb.eng.springconfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptorAdapter;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.security.config.annotation.web.messaging.MessageSecurityMetadataSourceRegistry;
import org.springframework.security.config.annotation.web.socket.AbstractSecurityWebSocketMessageBrokerConfigurer;
import org.springframework.security.core.Authentication;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.server.HandshakeInterceptor;

import pl.pw.rb.eng.springconfig.security.JWTHelper;

@Configuration
@EnableWebSocket
@EnableWebSocketMessageBroker
@Order(Ordered.HIGHEST_PRECEDENCE)
public class PeptidesWebSocketConfiguration extends AbstractSecurityWebSocketMessageBrokerConfigurer {

	@Autowired
	private HandshakeInterceptor handshakeInterceptor;

	@Autowired
	private JWTHelper jwtHelper;

	@Override
	protected boolean sameOriginDisabled() {
		return true;
	}

	@Override
	public void configureMessageBroker(MessageBrokerRegistry registry) {
		registry.enableSimpleBroker("/topic");
		registry.setApplicationDestinationPrefixes("/app");
	}

	@Override
	protected void customizeClientInboundChannel(ChannelRegistration registration) {
		registration.interceptors(new ChannelInterceptorAdapter() {

			@Override
			public boolean preReceive(MessageChannel channel) {
				return super.preReceive(channel);
			}

			@Override
			public Message<?> preSend(Message<?> message, MessageChannel channel) {
				StompHeaderAccessor accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
				if (StompCommand.CONNECT == accessor.getCommand()) {
					String cookies = message.getHeaders().get(JWTHelper.HEADER, String.class);
					Authentication user = jwtHelper.getAuthentication(cookies);
					accessor.setUser(user);
				}
				return message;
			}
		});

	}

	@Override
	protected void configureInbound(MessageSecurityMetadataSourceRegistry messages) {
		//@formatter:off
		messages.anyMessage().authenticated()
			.simpSubscribeDestMatchers("/db_upload/**").hasRole("admin")
			.simpSubscribeDestMatchers("/scan/**").authenticated();
		//@formatter:on
	}

	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint("/ws").setAllowedOrigins("*").withSockJS().setInterceptors(handshakeInterceptor);
	}

}
