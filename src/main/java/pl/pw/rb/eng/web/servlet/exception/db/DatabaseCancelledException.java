package pl.pw.rb.eng.web.servlet.exception.db;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.GONE, reason = "Someone has aborted the download")
@TranslationToken("ERR_DB_ABORTED")
public class DatabaseCancelledException extends ServletException {

	private static final long serialVersionUID = 9195710084401161723L;

	public DatabaseCancelledException(String message) {
		super(message);
	}
}