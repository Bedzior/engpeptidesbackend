package pl.pw.rb.eng.web.servlet.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class DatabaseDownloadDTO {

	private String name;

	private String regexId;

	private String regexName;

	private String url;

	private String description;

	public DatabaseDownloadDTO() {
	}

	public String getName() {
		return name;
	}

	public String getRegexId() {
		return regexId;
	}

	public String getRegexName() {
		return regexName;
	}

	public String getUrl() {
		return url;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setRegexId(String regexId) {
		this.regexId = regexId;
	}

	public void setRegexName(String regexName) {
		this.regexName = regexName;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
