package pl.pw.rb.eng.web.servlet.exception.template;

public class EmailTemplateCreationException extends Exception {

	private static final long serialVersionUID = 8300208856135870859L;

	public EmailTemplateCreationException(Exception e) {
		super(e);
	}

}
