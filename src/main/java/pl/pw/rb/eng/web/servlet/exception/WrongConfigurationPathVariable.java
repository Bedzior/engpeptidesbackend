package pl.pw.rb.eng.web.servlet.exception;

import static java.text.MessageFormat.format;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
@TranslationToken("ERR_UNAVAILABLE_CONFIGURATION")
public class WrongConfigurationPathVariable extends ServletException {

	public WrongConfigurationPathVariable(String mode) {
		super(format("Unavailable mode: {0}; please select 'simple' or 'advanced' config type", mode));
	}

	private static final long serialVersionUID = -1142285160231487178L;

}
