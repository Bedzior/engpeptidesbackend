package pl.pw.rb.eng.web.servlet.user;

import static java.text.MessageFormat.format;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.compress.archivers.ArchiveOutputStream;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import pl.pw.rb.eng.db.entities.ScanResult;
import pl.pw.rb.eng.service.ScanResultService;
import pl.pw.rb.eng.service.exception.NoProteinFoundException;
import pl.pw.rb.eng.service.exception.NoSuchScanException;
import pl.pw.rb.eng.service.exception.NoSuchScanResultException;
import pl.pw.rb.eng.web.servlet.dto.scan.MsMsShortPeptideHit;
import pl.pw.rb.eng.web.servlet.dto.scan.MsMsShortProteinHit;
import pl.pw.rb.eng.web.servlet.dto.scan.MsOutputDTO;
import pl.pw.rb.eng.web.servlet.exception.ScanResultReadException;
import pl.pw.rb.eng.web.servlet.scan.exception.OngoingScanException;

@Controller
@RequestMapping("/secured/scan/result")
@Secured("ROLE_USER")
public class ScanResultServlet {

	@Autowired
	private ScanResultService service;

	@GetMapping(value = "/{token}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE, params = { "packed=true" })
	void get(@PathVariable String token, HttpServletResponse response)
			throws NoSuchScanException, OngoingScanException, NoSuchScanResultException, ScanResultReadException {
		final ScanResult result = service.getForScan(token);
		response.setContentType(com.google.common.net.MediaType.TAR.toString());
		response.setHeader(HttpHeaders.CONTENT_DISPOSITION, format("attachment; filename={0}.tar.gz", result.getName()));
		try (ArchiveOutputStream aos = new TarArchiveOutputStream(new GzipCompressorOutputStream(response.getOutputStream()))) {
			TarArchiveEntry entry = new TarArchiveEntry(result.getName());
			entry.setSize(result.getResult().length);
			aos.putArchiveEntry(entry);
			aos.write(result.getResult());
			aos.closeArchiveEntry();
			aos.flush();
			response.setContentLength((int) aos.getBytesWritten());
		} catch (final IOException e) {
			throw new ScanResultReadException(format("An error occured while writing the result to output stream for scan {0}", token));
		}
	}

	@GetMapping(value = "/{token}", produces = { APPLICATION_JSON_UTF8_VALUE, APPLICATION_JSON_VALUE })
	@ResponseBody
	MsOutputDTO get(@PathVariable String token) throws NoSuchScanException, OngoingScanException, ScanResultReadException, NoSuchScanResultException {
		return service.getResult(token);
	}

	@GetMapping(value = "/{token}/proteins", produces = { APPLICATION_JSON_UTF8_VALUE, APPLICATION_JSON_VALUE })
	@ResponseBody
	Map<String, MsMsShortProteinHit> getProteins(@PathVariable String token) throws NoSuchScanException, OngoingScanException, ScanResultReadException, NoSuchScanResultException {
		return service.getProteinHashes(token);
	}

	@GetMapping(value = "/{token}/proteins/{protein}", produces = { APPLICATION_JSON_UTF8_VALUE, APPLICATION_JSON_VALUE })
	@ResponseBody
	Map<String, MsMsShortPeptideHit> getPeptides(@PathVariable String token, @PathVariable String protein)
			throws NoSuchScanException, OngoingScanException, ScanResultReadException, NoSuchScanResultException, NoProteinFoundException {
		return service.getPeptideHashes(token, protein);
	}
}
