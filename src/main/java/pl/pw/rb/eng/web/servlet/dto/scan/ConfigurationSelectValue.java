package pl.pw.rb.eng.web.servlet.dto.scan;

import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class ConfigurationSelectValue {

	private String value;

	@JsonProperty("short")
	private String shortName;

	private String name;

	protected ConfigurationSelectValue(String name, String shortName, String value) {
		super();
		this.value = value;
		this.shortName = shortName;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public String getShort() {
		return shortName;
	}

	public String getValue() {
		return value;
	}
}
