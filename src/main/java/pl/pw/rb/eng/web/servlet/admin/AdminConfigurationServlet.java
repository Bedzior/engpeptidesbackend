package pl.pw.rb.eng.web.servlet.admin;

import java.lang.reflect.Type;
import java.util.Collection;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.reflect.TypeToken;

import pl.pw.rb.eng.service.AdminConfigurationService;
import pl.pw.rb.eng.service.exception.MissingConfigurationValueException;
import pl.pw.rb.eng.web.dto.Configuration;
import pl.pw.rb.eng.web.dto.ConfigurationDTO;
import pl.pw.rb.eng.web.servlet.admin.exception.ConfigurationReadException;
import pl.pw.rb.eng.web.servlet.admin.exception.ConfigurationUpdateException;
import pl.pw.rb.eng.web.servlet.admin.exception.NoSuchConfigurationException;

@RestController
@RequestMapping(path = "/secured/configuration")
class AdminConfigurationServlet {

	@Autowired
	AdminConfigurationService service;

	@Autowired
	ModelMapper mapper;

	@GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_JSON_UTF8_VALUE })
	public Collection<Configuration> list() throws Exception {
		return service.list();
	}

	@GetMapping(path = "/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Configuration get(@PathVariable String name) throws NoSuchConfigurationException, ConfigurationReadException {
		return service.get(name);
	}

	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	public Collection<Configuration> update(@RequestBody Collection<ConfigurationDTO> values) throws ConfigurationUpdateException {
		@SuppressWarnings("serial")
		final Type listType = new TypeToken<Collection<Configuration>>() {
		}.getType();
		Collection<Configuration> mapped = mapper.map(values, listType);
		return service.update(mapped);
	}

	@PutMapping(path = "/{name}", consumes = { MediaType.TEXT_PLAIN_VALUE, MediaType.APPLICATION_JSON_UTF8_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, produces = MediaType.APPLICATION_JSON_VALUE)
	@PreAuthorize("hasRole('ADMIN')")
	public Configuration update(@PathVariable String name, @RequestBody String payload)
			throws NoSuchConfigurationException, MissingConfigurationValueException, ConfigurationUpdateException, ConfigurationReadException {
		Configuration config = service.get(name);
		if (config == null)
			throw new NoSuchConfigurationException(name);
		return service.update(name, payload);
	}

}
