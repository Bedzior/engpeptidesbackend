package pl.pw.rb.eng.web.servlet.exception;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
@TranslationToken("ERR_RESULT_READ")
public class ScanResultReadException extends ServletException {

	private static final long serialVersionUID = -3211472632891609234L;

	public ScanResultReadException() {
		super();
	}

	public ScanResultReadException(String message, Throwable rootCause) {
		super(message, rootCause);
	}

	public ScanResultReadException(String message) {
		super(message);
	}

	public ScanResultReadException(Throwable rootCause) {
		super(rootCause);
	}

}
