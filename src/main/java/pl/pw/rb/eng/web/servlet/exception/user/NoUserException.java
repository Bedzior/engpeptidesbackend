package pl.pw.rb.eng.web.servlet.exception.user;

import static java.text.MessageFormat.format;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "User not found")
@TranslationToken("ERR_NO_USER")
public class NoUserException extends ServletException {

	private static final long serialVersionUID = -456709675393166507L;

	public NoUserException(Integer userID) {
		super(format("No user with id {0,number,#} found", userID));
	}

	public NoUserException(String exclusivelyIdentifiableToken) {
		super(format("No user identifiable by {0} found", exclusivelyIdentifiableToken));
	}

}
