package pl.pw.rb.eng.web.controller.dto;

public abstract class ErrorStatusMessage extends StatusMessage {

	protected ErrorStatusMessage(String message) {
		super(false, message);
	}

}
