package pl.pw.rb.eng.web.servlet.exception.user;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "User's e-mail already activated")
@TranslationToken("ERR_USER_ACTIVATED")
public class UserAlreadyActivatedException extends ServletException {

	private static final long serialVersionUID = 1071361717069510646L;

}
