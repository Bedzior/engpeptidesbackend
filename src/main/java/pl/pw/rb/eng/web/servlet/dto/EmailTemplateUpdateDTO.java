package pl.pw.rb.eng.web.servlet.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect
public class EmailTemplateUpdateDTO {

	private String body;

	public EmailTemplateUpdateDTO() {
	}

	public EmailTemplateUpdateDTO(String body) {
		super();
		this.body = body;
	}

	@JsonProperty
	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

}
