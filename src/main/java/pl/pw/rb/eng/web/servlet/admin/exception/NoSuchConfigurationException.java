package pl.pw.rb.eng.web.servlet.admin.exception;

import static java.text.MessageFormat.format;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "The requested configuration does not exist")
@TranslationToken("ERR_NO_CONFIG")
public class NoSuchConfigurationException extends ServletException {

	private static final long serialVersionUID = 899684054944060265L;

	public NoSuchConfigurationException() {
		super("Missing configuration");
	}

	public NoSuchConfigurationException(String name) {
		super(format("Configuration named {0} does not exist", name));
	}

}
