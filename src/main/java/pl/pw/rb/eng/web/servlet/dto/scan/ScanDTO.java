package pl.pw.rb.eng.web.servlet.dto.scan;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import pl.pw.rb.eng.db.entities.ScanStatus;

@JsonAutoDetect
public class ScanDTO {

	@JsonProperty("token")
	private String id;

	private ScanStatus status;

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private Date created;

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private Date started;

	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private Date finished;

	private String parameters;

	public ScanDTO() {
	}

	protected ScanDTO(String token, String parameters, ScanStatus status, Date created, Date started, Date finished) {
		super();
		this.id = token;
		this.parameters = parameters;
		this.status = status;
		this.created = created;
		this.started = started;
		this.finished = finished;
	}

	@JsonProperty("token")
	public String getId() {
		return id;
	}

	@JsonProperty
	public String getParameters() {
		return parameters;
	}

	@JsonProperty
	public ScanStatus getStatus() {
		return status;
	}

	@JsonProperty()
	public Date getCreated() {
		return created;
	}

	@JsonProperty
	public Date getFinished() {
		return finished;
	}

	@JsonProperty
	public Date getStarted() {
		return started;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setStatus(ScanStatus status) {
		this.status = status;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public void setStarted(Date started) {
		this.started = started;
	}

	public void setFinished(Date finished) {
		this.finished = finished;
	}

	public void setParameters(String parameters) {
		this.parameters = parameters;
	}

}
