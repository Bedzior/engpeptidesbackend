package pl.pw.rb.eng.web.servlet.exception.template;

import static java.text.MessageFormat.format;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "E-mail template does not exist")
@TranslationToken("ERR_MISSING")
public class EmailTemplateNotFound extends ServletException {

	private static final long serialVersionUID = -6923261151551412928L;

	public EmailTemplateNotFound(String name, String type) {
		super(format("E-mail template {0}.{1} does not exist in database", name, type));
	}

	public EmailTemplateNotFound(int id) {
		super(format("E-mail template with id {0} does not exist in database", id));
	}

	public EmailTemplateNotFound(String id) {
		super(format("E-mail template with unparsable id {0} does not exist in database", id));
	}

}
