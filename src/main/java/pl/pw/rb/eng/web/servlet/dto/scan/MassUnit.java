package pl.pw.rb.eng.web.servlet.dto.scan;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonCreator.Mode;

public enum MassUnit {
	DALTON("Da"),
	MILLI_MASS_UNIT("mmu"),
	PARTS_PER_MILLION("ppm"),
	PERCENTAGE("%");

	private static final Map<String, MassUnit> map = new Hashtable<>(values().length);

	public final String symbol;
	static {
		Arrays.asList(values()).forEach(unit -> map.put(unit.symbol, unit));
	}

	private MassUnit(String symbol) {
		this.symbol = symbol;
	}

	@JsonCreator(mode = Mode.PROPERTIES)
	public static MassUnit forValue(String value) {
		return map.get(value);
	}

}
