package pl.pw.rb.eng.web.controller.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonGetter;

@JsonAutoDetect
public class TranslatableErrorMessage extends ErrorStatusMessage {

	private String token;

	public TranslatableErrorMessage(String message, String token) {
		super(message);
		this.token = token;
	}

	@JsonGetter
	public String getToken() {
		return token;
	}
}
