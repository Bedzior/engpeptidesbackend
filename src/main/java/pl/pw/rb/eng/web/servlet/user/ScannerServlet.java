package pl.pw.rb.eng.web.servlet.user;

import java.lang.reflect.Type;
import java.security.Principal;
import java.util.Collection;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.util.UriComponentsBuilder;

import com.google.common.reflect.TypeToken;

import pl.pw.rb.eng.db.entities.Scan;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.scan.exception.DbConfigurationException;
import pl.pw.rb.eng.service.ScannerService;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.service.exception.NoScanFileException;
import pl.pw.rb.eng.service.exception.NoSuchScanException;
import pl.pw.rb.eng.service.exception.NullUserException;
import pl.pw.rb.eng.web.servlet.dto.scan.ScanConfiguration;
import pl.pw.rb.eng.web.servlet.dto.scan.ScanDTO;

@RestController
@RequestMapping(path = "/secured/scan")
class ScannerServlet {

	@Autowired
	private ScannerService service;

	@Autowired
	private UsersService usersService;

	@Autowired
	private ModelMapper mapper;

	@GetMapping("/{token}")
	ScanDTO get(@PathVariable("token") final String token) throws NoSuchScanException {
		final Scan scan = service.get(token);
		final ScanDTO mapped = mapper.map(scan, ScanDTO.class);
		return mapped;
	}

	@SuppressWarnings("serial")
	@GetMapping
	@ResponseBody
	Collection<ScanDTO> list(final Principal user, @RequestParam(required = false) Boolean finished) {
		final Collection<Scan> scans;
		if (finished == null)
			scans = service.getForUser(user);
		else
			scans = service.getForUser(user, finished);
		final Type listType = new TypeToken<Collection<ScanDTO>>() {
		}.getType();
		final Collection<ScanDTO> mapped = mapper.map(scans, listType);
		return mapped;
	}

	@PostMapping
	ResponseEntity<ScanDTO> start(final Principal principal, UriComponentsBuilder uriBuilder, @Valid @RequestBody ScanConfiguration configuration)
			throws DbConfigurationException, NullUserException, NoScanFileException {
		final User user = usersService.getByLogin(principal.getName());
		final Scan scan = service.createFor(user, RequestContextHolder.currentRequestAttributes().getSessionId(), configuration);
		final ScanDTO mapped = mapper.map(scan, ScanDTO.class);
		return ResponseEntity.accepted().header(HttpHeaders.LOCATION, uriBuilder.buildAndExpand(scan.getId()).toString()).body(mapped);
	}
}
