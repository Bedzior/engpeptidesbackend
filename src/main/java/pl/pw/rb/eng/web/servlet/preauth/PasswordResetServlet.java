package pl.pw.rb.eng.web.servlet.preauth;

import static java.text.MessageFormat.format;

import javax.mail.MessagingException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pl.pw.rb.eng.service.PasswordResetService;
import pl.pw.rb.eng.web.servlet.dto.PasswordResetRequest;

@RestController
@RequestMapping(path = "/resetPassword")
class PasswordResetServlet {

	private final Logger log4j = Logger.getLogger(getClass());

	@Autowired
	private PasswordResetService passwordResetService;

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	protected ResponseEntity<?> doPost(@RequestParam("uuid") String uuid) {
		log4j.info("User requested a password reset with e-mail link");
		if (passwordResetService.verify(uuid))
			return ResponseEntity.ok().build();
		else
			return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
	}

	@PostMapping(path = "/request", consumes = MediaType.APPLICATION_JSON_VALUE)
	protected ResponseEntity<?> requestPasswordReset(@RequestBody PasswordResetRequest request) {
		log4j.info(format("Password request posted: {0}", request));
		try {
			if (request.hasUsername()) {
				passwordResetService.createForUser(request.getUsername());
				return ResponseEntity.accepted().build();
			} else if (request.hasEmail()) {
				passwordResetService.createForEmail(request.getEmail());
				return ResponseEntity.accepted().build();
			} else {
				log4j.warn(format("Incorrect password reset request: {0}", request.toString()));
				return ResponseEntity.badRequest().build();
			}
		} catch (MessagingException e) {
			log4j.fatal("An unexpected error occured while sending e-mail message to user", e);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}
}
