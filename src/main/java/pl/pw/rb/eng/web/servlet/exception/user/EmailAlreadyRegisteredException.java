package pl.pw.rb.eng.web.servlet.exception.user;

import static java.text.MessageFormat.format;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Somebody already registered with this e-mail")
@TranslationToken("ERR_EMAIL_EXISTS")
public class EmailAlreadyRegisteredException extends ServletException {

	private static final long serialVersionUID = 2836568653202204830L;

	public EmailAlreadyRegisteredException(String email) {
		super(format("E-mail address {0} already registered", email));
	}

}
