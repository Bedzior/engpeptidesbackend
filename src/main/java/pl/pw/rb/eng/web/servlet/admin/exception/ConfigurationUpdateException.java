package pl.pw.rb.eng.web.servlet.admin.exception;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(reason = "Error saving the updated configuration values", code = HttpStatus.INTERNAL_SERVER_ERROR)
@TranslationToken("ERR_SAVE")
public class ConfigurationUpdateException extends ServletException {

	private static final long serialVersionUID = -1991087050636037759L;

}
