package pl.pw.rb.eng.web.dto;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class DatabaseDownloadRequest {

	private Integer id;

	private String url;

	private String token;

	private String name;

	private String regexId;

	private String regexName;

	private String description;

	public boolean withURL() {
		return StringUtils.isNotBlank(url);
	}

	public boolean withUpload() {
		return StringUtils.isNotBlank(token);
	}

	public String getToken() {
		return token;
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getRegexId() {
		return regexId;
	}

	public String getRegexName() {
		return regexName;
	}

	public String getDescription() {
		return description;
	}

	public String getUrl() {
		return url;
	}

}
