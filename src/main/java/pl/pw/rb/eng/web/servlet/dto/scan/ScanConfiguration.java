package pl.pw.rb.eng.web.servlet.dto.scan;

import static java.text.MessageFormat.format;

import java.util.Arrays;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonGetter;

@JsonAutoDetect(getterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE)
public class ScanConfiguration {

	@JsonAutoDetect(getterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE)
	public static class DataConfiguration {

		@NotNull
		@Min(1)
		private int database;

		@JsonGetter
		public int getDatabase() {
			return database;
		}

		public void setDatabase(int database) {
			this.database = database;
		}

		@Override
		public String toString() {
			return format("Database ID: {0,number,#}", database);
		}

	}

	@JsonAutoDetect(getterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE)
	public static class BioConfiguration {

		@NotNull
		@NotEmpty
		private String enzyme;

		@Min(0)
		private int missedCleavage;

		@Min(value = 0, message = "Peak detection should be non-negative")
		@Max(value = 2, message = "Peak detection should not be more than 2")
		private int peakDetection;

		@Min(0)
		private double peptideTolerance;

		@NotNull
		private MassUnit peptideToleranceUnits;

		@Min(0)
		private double tandemMassTolerance;

		@NotNull
		private MassUnit tandemMassToleranceUnits;

		@Pattern(regexp = "^((([0-9]+),[\\s]?)*([0-9]+))?$")
		private String peptideCharge;

		@JsonGetter
		public String getEnzyme() {
			return enzyme;
		}

		public void setEnzyme(String enzyme) {
			this.enzyme = enzyme;
		}

		@JsonGetter
		public int getMissedCleavage() {
			return missedCleavage;
		}

		public void setMissedCleavage(int missedCleavage) {
			this.missedCleavage = missedCleavage;
		}

		@JsonGetter
		public int getPeakDetection() {
			return peakDetection;
		}

		public void setPeakDetection(int peakDetection) {
			this.peakDetection = peakDetection;
		}

		@JsonGetter
		public double getPeptideTolerance() {
			return peptideTolerance;
		}

		public void setPeptideTolerance(double peptideTolerance) {
			this.peptideTolerance = peptideTolerance;
		}

		@JsonGetter
		public MassUnit getPeptideToleranceUnits() {
			return peptideToleranceUnits;
		}

		public void setPeptideToleranceUnits(MassUnit peptideToleranceUnits) {
			this.peptideToleranceUnits = peptideToleranceUnits;
		}

		@JsonGetter
		public double getTandemMassTolerance() {
			return tandemMassTolerance;
		}

		public void setTandemMassTolerance(double tandemMassTolerance) {
			this.tandemMassTolerance = tandemMassTolerance;
		}

		@JsonGetter
		public MassUnit getTandemMassToleranceUnits() {
			return tandemMassToleranceUnits;
		}

		public void setTandemMassToleranceUnits(MassUnit tandemMassToleranceUnits) {
			this.tandemMassToleranceUnits = tandemMassToleranceUnits;
		}

		@JsonGetter
		public String getPeptideCharge() {
			return peptideCharge;
		}

		public void setPeptideCharge(String peptideCharge) {
			this.peptideCharge = peptideCharge;
		}

		@Override
		public String toString() {
			return "enzyme=" + enzyme + ", missedCleavage=" + missedCleavage + ", peakDetection=" + peakDetection + ", peptideTolerance=" + peptideTolerance
					+ peptideToleranceUnits + ", tandemMassTolerance=" + tandemMassTolerance + tandemMassToleranceUnits + ", peptideCharge=" + peptideCharge;
		}

	}

	@JsonAutoDetect(getterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE)
	public static class ModificationsConfiguration {

		private String[] fixModifications;

		private String[] varModifications;

		@JsonGetter
		public String[] getFixModifications() {
			return fixModifications;
		}

		public void setFixModifications(String[] fixModifications) {
			this.fixModifications = fixModifications;
		}

		@JsonGetter
		public String[] getVarModifications() {
			return varModifications;
		}

		public void setVarModifications(String[] varModifications) {
			this.varModifications = varModifications;
		}

		@Override
		public String toString() {
			return "fixModifications=[" + Arrays.toString(fixModifications) + "]\nvarModifications=[" + Arrays.toString(varModifications) + "]";
		}

	}

	@JsonAutoDetect(getterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE)
	public static class OtherConfiguration {

		private String format;

		private String instrument;

		@NotNull
		@NotEmpty(message = "At least one data file should have been uploaded")
		private String[] files;

		@JsonGetter
		public String getFormat() {
			return format;
		}

		public void setFormat(String format) {
			this.format = format;
		}

		@JsonGetter
		public String getInstrument() {
			return instrument;
		}

		public void setInstrument(String instrument) {
			this.instrument = instrument;
		}

		@JsonGetter
		public String[] getFiles() {
			return files;
		}

		public void setFiles(String... files) {
			this.files = files;
		}

		@Override
		public String toString() {
			return "format=" + format + ", instrument=" + instrument + ", " + Integer.toString(files == null ? 0 : files.length) + " files";
		}
	}

	@NotNull
	@Valid
	private DataConfiguration data;

	@NotNull
	@Valid
	private BioConfiguration bio;

	@NotNull
	@Valid
	private ModificationsConfiguration modifications;

	@NotNull
	@Valid
	private OtherConfiguration other;

	@JsonGetter
	public DataConfiguration getData() {
		return data;
	}

	public void setData(DataConfiguration data) {
		this.data = data;
	}

	@JsonGetter
	public BioConfiguration getBio() {
		return bio;
	}

	public void setBio(BioConfiguration bio) {
		this.bio = bio;
	}

	@JsonGetter
	public ModificationsConfiguration getModifications() {
		return modifications;
	}

	public void setModifications(ModificationsConfiguration modifications) {
		this.modifications = modifications;
	}

	@JsonGetter
	public OtherConfiguration getOther() {
		return other;
	}

	public void setOther(OtherConfiguration other) {
		this.other = other;
	}

	@Override
	public String toString() {
		return "data=[" + data + "]\nbio=[" + bio + "]\nmodifications=[" + modifications + "]\nother=[" + other + "]";
	}
}
