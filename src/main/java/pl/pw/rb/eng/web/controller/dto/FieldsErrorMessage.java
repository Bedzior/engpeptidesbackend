package pl.pw.rb.eng.web.controller.dto;

import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonGetter;

/**
 * Error sent on form validation issues
 * 
 * @author Rafał Będźkowski
 *
 */
@JsonAutoDetect
public class FieldsErrorMessage extends ErrorStatusMessage {

	private String[] errors;

	public FieldsErrorMessage(String message, Collection<String> errors) {
		super(message);
		this.errors = errors.toArray(new String[errors.size()]);
	}

	public FieldsErrorMessage(String message, String... errors) {
		super(message);
		this.errors = errors;
	}

	@JsonGetter
	public String[] getErrors() {
		return errors;
	}

}
