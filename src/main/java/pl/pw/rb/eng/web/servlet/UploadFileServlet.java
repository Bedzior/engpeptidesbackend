package pl.pw.rb.eng.web.servlet;

import static java.text.MessageFormat.format;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import pl.pw.rb.eng.db.dto.UploadFileDTO;
import pl.pw.rb.eng.db.entities.UploadFile;
import pl.pw.rb.eng.service.UploadFileService;
import pl.pw.rb.eng.web.servlet.exception.FileUploadException;

@Controller
@RequestMapping(path = "/secured/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@Secured("ROLE_USER")
class UploadFileServlet {

	private final Logger logger = Logger.getLogger(getClass());

	@Autowired
	private UploadFileService uploadFileService;

	@Autowired
	private ModelMapper mapper;

	private File uploadsDir;

	@Autowired
	UploadFileServlet(File tempDir) {
		this.uploadsDir = new File(tempDir, "uploads");
		if (!uploadsDir.exists())
			uploadsDir.mkdirs();
	}

	@PostMapping
	protected ResponseEntity<UploadFileDTO> upload(@RequestParam("file") MultipartFile file, Principal principal, HttpServletRequest req)
			throws FileUploadException {
		final File userDir = new File(uploadsDir, principal.getName());
		if (!userDir.exists())
			userDir.mkdirs();
		logger.info(format("Uploaded file of {0} from {1}", FileUtils.byteCountToDisplaySize(file.getSize()), req.getRemoteAddr()));
		/*
		 * we prefix the output file with an easily identifiable metadata (like current
		 * time in milliseconds) to avoid issues of uploading the same file more than
		 * once during the same session
		 */
		final File output = new File(userDir, System.currentTimeMillis() + "_" + file.getOriginalFilename());
		/* move the file */
		try (ReadableByteChannel c = Channels.newChannel(file.getInputStream()); FileOutputStream os = new FileOutputStream(output)) {
			os.getChannel().transferFrom(c, 0, Long.MAX_VALUE);
		} catch (final IOException e) {
			logger.error("Error transferring the uploaded file", e);
			throw new FileUploadException();
		}
		/* create the database entity from the moved upload */
		final UploadFile uploaded = uploadFileService.create(principal.getName(), output);
		final UploadFileDTO dto = mapper.map(uploaded, UploadFileDTO.class);
		return ResponseEntity.ok(dto);
	}
}
