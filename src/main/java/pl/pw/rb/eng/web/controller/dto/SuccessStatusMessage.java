package pl.pw.rb.eng.web.controller.dto;

public class SuccessStatusMessage extends StatusMessage {

	public SuccessStatusMessage(String message) {
		super(true, message);
	}

}
