package pl.pw.rb.eng.web.servlet.scan.exception;

import static java.text.MessageFormat.format;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.FOUND, reason = "The requested scan has not yet finished")
@TranslationToken("ERR_SCAN_IN_PROGRESS")
public class OngoingScanException extends ServletException {

	private static final long serialVersionUID = -5166207936160454360L;

	public OngoingScanException(String token) {
		super(format("Scan {0} has not yet finished; its result is not yet available", token));
	}
}
