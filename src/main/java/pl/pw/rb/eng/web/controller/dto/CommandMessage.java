package pl.pw.rb.eng.web.controller.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonCreator.Mode;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect
public class CommandMessage {

	public CommandMessage() {
	}

	public CommandMessage(Command command) {
		this.command = command;
	}

	public enum Command {
		/**
		 * Command ordering a long-running task to end abruptly
		 */
		CANCEL;

		@JsonCreator(mode = Mode.PROPERTIES)
		public static Command forValue(String value) {
			try {
				return Command.valueOf(value.toUpperCase());
			} catch (IllegalArgumentException e) {
				return null;
			}
		}
	}

	@JsonProperty(required = true)
	private Command command;

	public Command getCommand() {
		return this.command;
	}

}
