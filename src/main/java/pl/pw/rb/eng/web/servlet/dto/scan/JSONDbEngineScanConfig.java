package pl.pw.rb.eng.web.servlet.dto.scan;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;

import mscanlib.ms.db.DB;
import mscanlib.ms.mass.EnzymeMap;
import mscanlib.ms.mass.InSilicoDigestConfig;
import mscanlib.ms.mass.MassTools;
import mscanlib.ms.mass.PTMTools;
import mscanlib.ms.msms.MsMsFragmentationConfig;
import mscanlib.ms.msms.MsMsInstrument;
import mscanlib.ms.msms.dbengines.DbEngineSearchConfig;
import mscanlib.ms.msms.spectrum.MsMsSpectrumProcessingConfig;
import pl.pw.rb.eng.db.entities.Database;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.scan.exception.DbConfigurationException;
import pl.pw.rb.eng.web.servlet.dto.scan.ScanConfiguration.BioConfiguration;
import pl.pw.rb.eng.web.servlet.dto.scan.ScanConfiguration.ModificationsConfiguration;
import pl.pw.rb.eng.web.servlet.dto.scan.ScanConfiguration.OtherConfiguration;

public class JSONDbEngineScanConfig extends DbEngineSearchConfig {

	private static final long serialVersionUID = -6365715491602411270L;

	private final String[] files;

	//@formatter:off
	public JSONDbEngineScanConfig(
			User                       user, 
			Database                   database, 
			String[]                   files, 
			BioConfiguration           bioConfig, 
			ModificationsConfiguration modConfig,
			OtherConfiguration         otherConfig) 
		throws DbConfigurationException {
	//@formatter:on
		super();
		this.setEmbedded(true);
		this.setSearchTitle("");
		this.setUser(user.getLogin());
		this.setUserMail(user.getEmail());
		this.setSmp(true);
		this.setSmpThreads(Runtime.getRuntime().availableProcessors());
		final DB db = new DB(database.getId(), database.getName(), "1");
		db.setDbFilename(database.getPath());
		db.setIdRegExp(database.getRegexId());
		db.setNameRegExp(database.getRegexName());

		this.addDB(db);

		MsMsFragmentationConfig fragmentationConfig = this.getScoringConfig().getFragmentationConfig();
		fragmentationConfig.setInstrument(new MsMsInstrument(otherConfig.getInstrument()));
		String peptideCharge = bioConfig.getPeptideCharge();
		if (StringUtils.isNotBlank(peptideCharge))
			for (String charge : peptideCharge.split(",")) {
				fragmentationConfig.addCharge(Integer.parseInt(charge.trim()));
			}
		this.setParentMMD(bioConfig.getPeptideTolerance());
		this.setParentMMDUnit(getMMDUnit(bioConfig.getPeptideToleranceUnits()));
		this.setFragmentMMD(bioConfig.getTandemMassTolerance());
		this.setFragmentMMDUnit(getMMDUnit(bioConfig.getTandemMassToleranceUnits()));
		{
			InSilicoDigestConfig digestConfig = this.getDigestConfig();
			digestConfig.setEnzyme(EnzymeMap.getEnzyme(bioConfig.getEnzyme()));
			digestConfig.setMissedCleavages(bioConfig.getMissedCleavage());
			digestConfig.setFixedPTMs(PTMTools.getPTMs(modConfig.getFixModifications()));
			digestConfig.setVariablePTMs(PTMTools.getPTMs(modConfig.getVarModifications()));
		}
		{
			MsMsSpectrumProcessingConfig processingConfig = new MsMsSpectrumProcessingConfig();
			processingConfig.setPeakPicking(bioConfig.getPeakDetection());
			this.setProcessingConfig(processingConfig);
		}
		this.files = files;
	}

	private int getMMDUnit(MassUnit massUnit) {
		return MassTools.getMMDUnit(massUnit.symbol);
	}

	public String[] getInputFilenames() {
		return files;
	}

	public File[] getOutputFiles() {
		assert (files != null);
		assert (files.length >= 0);
		return Arrays.asList(files).stream().map(t -> {
			File input = new File(t);
			File output = new File(input.getParentFile(), input.getName().substring(0, input.getName().lastIndexOf('.')) + ".out");
			return output;
		}).toArray(value -> new File[value]);
	}
}
