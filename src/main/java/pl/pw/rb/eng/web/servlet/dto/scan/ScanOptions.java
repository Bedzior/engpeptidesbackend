package pl.pw.rb.eng.web.servlet.dto.scan;

import java.util.Collection;

import pl.pw.rb.eng.db.dto.SimpleDatabaseDTO;

/**
 * Configuration options available for setting up a peptide scan as presented to
 * the frontend user. <br/>
 * <br/>
 * The options should mostly represent values provided and understood by
 * {@link mscanlib.ms.msms.dbengines.DbEngine}, for a user to choose from. The
 * {@link #databases} field however represents peptide databases available on
 * the host system, interpreted by the scanner during peptide search. Those are
 * proviced as files external to the system.
 * 
 * @author Rafał Będźkowski
 *
 */
public class ScanOptions {

	private Collection<PTM> ptms;

	private Collection<MMDUnit> mmdUnits;

	private Collection<String> enzymes;

	// TODO fragmentation_rules
	private Collection<String> instruments;

	private Collection<SimpleDatabaseDTO> databases;

	public void setPTMs(Collection<PTM> ptms) {
		this.ptms = ptms;
	}

	public void setEnzymes(Collection<String> enzymes) {
		this.enzymes = enzymes;
	}

	public void setMMDUnits(Collection<MMDUnit> collection) {
		this.mmdUnits = collection;
	}

	public void setInstruments(Collection<String> instruments) {
		this.instruments = instruments;
	}

	public void setDatabases(Collection<SimpleDatabaseDTO> simpleDatabasesInfo) {
		this.databases = simpleDatabasesInfo;
	}

	public Collection<PTM> getPtms() {
		return ptms;
	}

	public void setPtms(Collection<PTM> ptms) {
		this.ptms = ptms;
	}

	public Collection<MMDUnit> getMmdUnits() {
		return mmdUnits;
	}

	public void setMmdUnits(Collection<MMDUnit> mmdUnits) {
		this.mmdUnits = mmdUnits;
	}

	public Collection<SimpleDatabaseDTO> getDatabases() {
		return databases;
	}

	public Collection<String> getEnzymes() {
		return enzymes;
	}

	public Collection<String> getInstruments() {
		return instruments;
	}

}
