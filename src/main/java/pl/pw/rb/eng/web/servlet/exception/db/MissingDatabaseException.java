package pl.pw.rb.eng.web.servlet.exception.db;

import static java.text.MessageFormat.format;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "No such database exists")
@TranslationToken("ERR_MISSING")
public class MissingDatabaseException extends ServletException {

	private static final long serialVersionUID = -5286175566419586153L;

	public MissingDatabaseException(String databaseName) {
		super(format("Database with given name does not exist: {0}", databaseName));
	}

	public MissingDatabaseException(int databaseID) {
		super(format("Database with given ID does not exist: {0,number,#}", databaseID));
	}
}
