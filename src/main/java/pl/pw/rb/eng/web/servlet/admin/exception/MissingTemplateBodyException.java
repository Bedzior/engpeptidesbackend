package pl.pw.rb.eng.web.servlet.admin.exception;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Missing template body")
@TranslationToken("ERR_NO_BODY")
public class MissingTemplateBodyException extends ServletException {

	private static final long serialVersionUID = 6128649875826788962L;

	public MissingTemplateBodyException() {
		super();
	}

}
