package pl.pw.rb.eng.web.servlet.dto.scan;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect(getterVisibility = NONE, isGetterVisibility = NONE)
public class MsMsShortPeptideHit {

	private String sequence;

	private double calcMass;

	private int queriesCount;

	private Collection<MsMsShortQuery> queries;

	@JsonProperty
	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	@JsonProperty
	public double getCalcMass() {
		return calcMass;
	}

	public void setCalcMass(double calcMass) {
		this.calcMass = calcMass;
	}

	@JsonProperty
	public int getQueriesCount() {
		return queriesCount;
	}

	@JsonProperty("queries")
	public Collection<MsMsShortQuery> getQueriesList() {
		return this.queries;
	}

	public void setQueriesCount(int mQueriesCount) {
		this.queriesCount = mQueriesCount;
	}
	
	public void setQueriesList(Collection<MsMsShortQuery> queries) {
		this.queries = queries;
	}

}
