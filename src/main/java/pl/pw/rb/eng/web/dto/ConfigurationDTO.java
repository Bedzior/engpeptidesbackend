package pl.pw.rb.eng.web.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

/**
 * Represents a named configuration value
 * 
 * @author Rafał Będźkowski
 */
@JsonAutoDetect
public class ConfigurationDTO {

	private String name;

	private Object value;

	public ConfigurationDTO() {
	}

	public ConfigurationDTO(String name, Object value) {
		super();
		this.name = name;
		this.value = value;
	}

	@JsonGetter
	public String getName() {
		return this.name;
	}

	@JsonGetter
	public Object getValue() {
		return this.value;
	}

	@JsonSetter
	public void setName(String name) {
		this.name = name;
	}

	@JsonSetter
	public void setValue(Object value) {
		this.value = value;
	}

}
