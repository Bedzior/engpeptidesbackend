package pl.pw.rb.eng.web.context;

import static java.text.MessageFormat.format;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.data.repository.CrudRepository;

import pl.pw.rb.eng.db.dao.UsersDAO;
import pl.pw.rb.eng.service.DatabaseService;
import pl.pw.rb.eng.service.ScannerService;
import pl.pw.rb.eng.service.TemplateService;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.service.exception.DatabaseAlreadyDownloadingException;
import pl.pw.rb.eng.service.exception.DatabaseAlreadyExistsException;
import pl.pw.rb.eng.web.servlet.exception.db.DatabaseDownloadException;
import pl.pw.rb.eng.web.servlet.exception.template.EmailTemplateCreationException;
import pl.pw.rb.eng.web.servlet.exception.user.EmailAlreadyRegisteredException;
import pl.pw.rb.eng.web.servlet.exception.user.UserAlreadyExistsException;

/**
 * Application Lifecycle Listener implementation class
 * InitializationContextListener
 *
 */
@ManagedBean
final class InitializationContextListener implements ServletContextInitializer {

	private static final String PROPERTY_KEY = "clean";

	private static final String DEFAULT_ADMIN_USER = "admin";

	private static final String DEFAULT_TEST_USER = "test";

	private final Logger log4j = Logger.getLogger(getClass());

	@Autowired
	private File appDir;

	@Autowired
	private File databaseDir;

	@Autowired
	private List<? extends CrudRepository<?, ?>> daoBeans;

	@Autowired
	private TemplateService templateService;

	@Autowired
	private UsersService userService;

	@Autowired
	private DatabaseService databaseService;

	@Autowired
	private ScannerService scannerService;

	@Autowired
	private UsersDAO usersDAO;

	private void repopulateUsers() throws EmailAlreadyRegisteredException, UserAlreadyExistsException  {
		userService.createActivatedAdmin(DEFAULT_ADMIN_USER, "test_admin@test.org", "admin");
		userService.createActivatedUser(DEFAULT_TEST_USER, "test_user@test.org", "test1");
	}

	private void clearAll() {
		for (CrudRepository<?, ?> repository : daoBeans)
			if (!(repository instanceof UsersDAO))
				repository.deleteAll();
		usersDAO.deleteAll();
	}

	private void populateDBs() throws DatabaseAlreadyDownloadingException {
		try {
			if (!databaseDir.exists()) {
				final String name = "UniProt (Swiss-Prot)";
				final String url = "ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.fasta.gz";
				log4j.info(format("Downloading {0} from {1}", name, url));
				databaseService.create(DEFAULT_ADMIN_USER, name, "Initial download of Swiss Uniprot FASTA", url, "",
						"");
				log4j.info(format("Registered {0} database at"));
			} else {
				log4j.info("Database directory already exists in the user directory");
				log4j.info(format("Importing databases from {0}", databaseDir.getAbsolutePath()));
				databaseService.importFrom(databaseDir);
			}
		} catch (DatabaseDownloadException | DatabaseAlreadyExistsException e) {
			log4j.error("Couldn't download the specified database", e);
		}
	}

	@Override
	public void onStartup(ServletContext servletContext) throws ServletException  {
		final String property = System.getProperty(PROPERTY_KEY);
		boolean clean = System.getProperties().containsKey(PROPERTY_KEY) && (property.equals("") || Boolean.parseBoolean(property));
		if (clean || usersDAO.count() == 0) {
			appDir.mkdirs();
			log4j.info("Server starting in clean mode, recreating table schemas");
			clearAll();
			log4j.info(format("Re-registering the default admin user: {0}", DEFAULT_ADMIN_USER));
			repopulateUsers();
			log4j.info(format("Populating database table with peptide file databases from: {0}", databaseDir.getAbsolutePath()));
			populateDBs();
			try {
				populateEmailTemplates();
			} catch (EmailTemplateCreationException e) {
				throw new ServletException(e);
			}
		}
		databaseService.resetDownloading();
		scannerService.resetRunning();
	}

	private void populateEmailTemplates() throws EmailTemplateCreationException {
		try {
			templateService.populateFromResources();
		} catch (IOException e) {
			throw new EmailTemplateCreationException(e);
		}
	}
}
