package pl.pw.rb.eng.web.servlet.dto.scan;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect(getterVisibility = NONE, isGetterVisibility = NONE)
public class MsMsShortProteinHit {

	private String id;

	private String name;

	private double score;

	private int peptidesCount;

	private String shortName;

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("id")
	public String getId() {
		return id;
	}

	@JsonProperty("score")
	public double getScore() {
		return score;
	}

	@JsonProperty("peptidesCount")
	public int getPeptidesCount() {
		return peptidesCount;
	}

	@JsonProperty("shortName")
	public String getShortName() {
		return shortName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setId(String mId) {
		this.id = mId;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public void setPeptidesCount(int peptidesCount) {
		this.peptidesCount = peptidesCount;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

}
