package pl.pw.rb.eng.web.servlet.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect
public class ServerStatusDTO {

	private int scanCount;

	private int waitingScanCount;

	private Date date;

	public ServerStatusDTO(int scanCount, int waitingScanCount) {
		super();
		this.date = new Date();
		this.scanCount = scanCount;
		this.waitingScanCount = waitingScanCount;
	}

	@JsonProperty
	public int getScanCount() {
		return this.scanCount;
	}

	@JsonProperty
	public int getWaitingScanCount() {
		return this.waitingScanCount;
	}

	@JsonProperty
	public Date getDate() {
		return date;
	}
}
