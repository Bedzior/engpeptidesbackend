package pl.pw.rb.eng.web.servlet.dto;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect
public class UserRegistration {

	public UserRegistration() {
	}

	protected UserRegistration(@JsonProperty(required = true) String login, @JsonProperty(required = true) String email,
			@JsonProperty(required = true) String password) {
		super();
		this.login = login;
		this.email = email;
		this.password = password;
	}

	@JsonProperty(required = true)
	@NotNull
	private String login;

	@JsonProperty(required = true)
	@NotNull
	private String email;

	@JsonProperty(required = true)
	@NotNull
	private String password;

	public String getLogin() {
		return login;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}
}
