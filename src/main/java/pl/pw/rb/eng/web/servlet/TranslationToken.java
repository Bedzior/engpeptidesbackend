package pl.pw.rb.eng.web.servlet;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Annotation holding internationalization (dictionary) token for any exception
 * type.
 * 
 * @author Rafał Będźkowski
 *
 */
@Retention(RUNTIME)
@Target(ElementType.TYPE)
public @interface TranslationToken {

	String value();

}
