package pl.pw.rb.eng.web.servlet.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect
public class PasswordResetRequest {

	private String username;

	private String email;

	public PasswordResetRequest() {
	}

	protected PasswordResetRequest(@JsonProperty(required = false) String username, @JsonProperty(required = false) String email) {
		super();
		this.username = username;
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public String getEmail() {
		return email;
	}

	public boolean hasEmail() {
		return email != null;
	}

	public boolean hasUsername() {
		return username != null;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
