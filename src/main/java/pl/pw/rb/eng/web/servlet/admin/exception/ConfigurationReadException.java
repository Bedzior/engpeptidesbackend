package pl.pw.rb.eng.web.servlet.admin.exception;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
@TranslationToken("ERR_READ")
public class ConfigurationReadException extends ServletException {

	private static final long serialVersionUID = -5836372223310616084L;

}
