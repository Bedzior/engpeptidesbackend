package pl.pw.rb.eng.web.servlet.dto.scan;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect(getterVisibility = NONE, isGetterVisibility = NONE)
public class MsMsShortQuery {

	private int nr;

	private double mz;

	private double mass;

	private byte charge;

	private double score;

	private double deltaPPM;

	@JsonProperty
	public int getNr() {
		return nr;
	}

	public void setNr(int nr) {
		this.nr = nr;
	}

	@JsonProperty
	public double getMz() {
		return mz;
	}

	public void setMz(double mz) {
		this.mz = mz;
	}

	@JsonProperty
	public double getMass() {
		return mass;
	}

	public void setMass(double mass) {
		this.mass = mass;
	}

	@JsonProperty
	public byte getCharge() {
		return charge;
	}

	public void setCharge(byte charge) {
		this.charge = charge;
	}

	@JsonProperty
	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	@JsonProperty
	public double getDeltaPPM() {
		return deltaPPM;
	}

	public void setDeltaPPM(double deltaPPM) {
		this.deltaPPM = deltaPPM;
	}

}
