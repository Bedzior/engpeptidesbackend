package pl.pw.rb.eng.web.servlet.dto.scan;

public class PTM extends ConfigurationSelectValue {

	private boolean advanced;

	public PTM(String name, String shortName, String value, boolean advanced) {
		super(name, shortName, value);
		this.advanced = advanced;
	}

	public boolean isAdvanced() {
		return advanced;
	}
}
