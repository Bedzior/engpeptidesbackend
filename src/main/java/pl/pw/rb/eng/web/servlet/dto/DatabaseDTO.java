package pl.pw.rb.eng.web.servlet.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect
public class DatabaseDTO {

	private int id;

	private String name;

	private Date added;

	private Date downloaded;

	private boolean downloading;

	private String description;

	private String regexId;

	private String regexName;

	private String url;

	private Long size;

	public DatabaseDTO() {
	}

	@JsonProperty
	public int getId() {
		return id;
	}

	@JsonProperty
	public String getName() {
		return name;
	}

	@JsonProperty
	public Date getAdded() {
		return added;
	}

	@JsonProperty
	public Date getDownloaded() {
		return downloaded;
	}

	@JsonProperty
	public boolean isDownloading() {
		return downloading;
	}

	@JsonProperty
	public String getDescription() {
		return description;
	}

	@JsonProperty
	public String getRegexId() {
		return regexId;
	}

	@JsonProperty
	public String getRegexName() {
		return regexName;
	}

	@JsonProperty
	public String getUrl() {
		return url;
	}

	@JsonProperty
	public Long getSize() {
		return size;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAdded(Date added) {
		this.added = added;
	}

	public void setDownloaded(Date downloaded) {
		this.downloaded = downloaded;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setRegexId(String regexId) {
		this.regexId = regexId;
	}

	public void setRegexName(String regexName) {
		this.regexName = regexName;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setDownloading(boolean downloading) {
		this.downloading = downloading;
	}

	public void setSize(Long size) {
		this.size = size;
	}
}
