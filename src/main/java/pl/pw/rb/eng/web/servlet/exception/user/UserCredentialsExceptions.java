package pl.pw.rb.eng.web.servlet.exception.user;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.UNAUTHORIZED, reason = "Wrong credentials")
public class UserCredentialsExceptions {

}
