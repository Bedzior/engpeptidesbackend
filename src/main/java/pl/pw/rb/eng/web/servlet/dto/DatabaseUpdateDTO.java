package pl.pw.rb.eng.web.servlet.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class DatabaseUpdateDTO {

	public DatabaseUpdateDTO() {
	}

	public DatabaseUpdateDTO(String name, String regexId, String regexName, String url, String description) {
		super();
		this.name = name;
		this.regexId = regexId;
		this.regexName = regexName;
		this.url = url;
		this.description = description;
	}

	private String name;

	private String regexId;

	private String regexName;

	private String description;

	private String url;

	public String getName() {
		return name;
	}

	public String getRegexId() {
		return regexId;
	}

	public String getRegexName() {
		return regexName;
	}

	public String getUrl() {
		return url;
	}

	public String getDescription() {
		return description;
	}
}
