package pl.pw.rb.eng.web.servlet;

import java.security.Principal;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pl.pw.rb.eng.db.dto.UserDTO;
import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.service.exception.NullUserException;

@RestController
@RequestMapping(path = "/session")
class SessionServlet {

	@Autowired
	private UsersService users;

	@Autowired
	ModelMapper mapper;

	@GetMapping
	@ResponseBody
	private UserDTO doGet(Principal user) throws NullUserException  {
		if (user == null)
			throw new NullUserException();
		final User localUser = users.getByLogin(user.getName());
		if (localUser == null)
			throw new NullUserException();
		return mapper.map(localUser, UserDTO.class);
	}
}
