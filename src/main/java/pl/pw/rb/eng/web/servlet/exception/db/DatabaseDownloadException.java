package pl.pw.rb.eng.web.servlet.exception.db;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Could not download the database from the specified location")
@TranslationToken("ERR_DB_DOWNLOAD")
public class DatabaseDownloadException extends ServletException {

	private static final long serialVersionUID = 1960572718593673771L;

	public DatabaseDownloadException(String message) {
		super(message);
	}

	public DatabaseDownloadException(String message, Throwable cause) {
		super(message, cause);
	}

}
