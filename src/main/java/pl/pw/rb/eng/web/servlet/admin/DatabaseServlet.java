package pl.pw.rb.eng.web.servlet.admin;

import static java.text.MessageFormat.format;

import java.security.Principal;
import java.util.Collection;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import pl.pw.rb.eng.db.entities.Database;
import pl.pw.rb.eng.service.DatabaseService;
import pl.pw.rb.eng.service.exception.DatabaseAlreadyDownloadingException;
import pl.pw.rb.eng.service.exception.DatabaseAlreadyExistsException;
import pl.pw.rb.eng.service.exception.MissingFileUploadException;
import pl.pw.rb.eng.web.dto.DatabaseDownloadRequest;
import pl.pw.rb.eng.web.servlet.dto.DatabaseDTO;
import pl.pw.rb.eng.web.servlet.dto.DatabaseDownloadDTO;
import pl.pw.rb.eng.web.servlet.dto.DatabaseUpdateDTO;
import pl.pw.rb.eng.web.servlet.exception.db.DatabaseDownloadException;
import pl.pw.rb.eng.web.servlet.exception.db.MissingDatabaseException;
import pl.pw.rb.eng.web.servlet.exception.db.NoDatabaseSourceException;

/**
 * Servlet implementation class DatabaseServlet
 */
@RestController
@RequestMapping(path = "/secured/dbs")
class DatabaseServlet {

	private Logger log4j = Logger.getLogger(DatabaseServlet.class);

	@Autowired
	private DatabaseService databaseService;

	@Autowired
	private ModelMapper mapper;

	@GetMapping
	protected Collection<DatabaseDTO> list() {
		final Collection<Database> databases = databaseService.getDatabases(true);
		Collection<DatabaseDTO> mapped = map(databases);
		return mapped;
	}

	@GetMapping(path = "/{id}")
	public DatabaseDTO get(@PathVariable("id") Integer id) {
		return map(databaseService.getDatabase(id));
	}

	@PostMapping
	@ResponseBody
	@RolesAllowed("admin")
	protected ResponseEntity<DatabaseDownloadDTO> create(Principal user, UriComponentsBuilder uri, @RequestBody @Valid DatabaseDownloadRequest json)
			throws NoDatabaseSourceException, DatabaseDownloadException, DatabaseAlreadyExistsException, DatabaseAlreadyDownloadingException,
			MissingFileUploadException {
		log4j.trace(format("Adding new database from {0}", user.getName()));
		if (json.withURL()) {
			databaseService.create(user.getName(), json);
			return ResponseEntity.accepted().body(mapper.map(json, DatabaseDownloadDTO.class));
		} else if (json.withUpload()) {
			Database database = databaseService.create(user.getName(), json, json.getToken());
			final HttpHeaders headers = new HttpHeaders();
			headers.setLocation(uri.path("/secured/dbs/{id}").buildAndExpand(database.getId()).toUri());
			return new ResponseEntity<>(mapper.map(database, DatabaseDownloadDTO.class), headers, HttpStatus.CREATED);
		} else {
			log4j.error(format("No path or URL specified in the new database from {0}", user.getName()));
			throw new NoDatabaseSourceException();
		}
	}

	@PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseBody
	@RolesAllowed("admin")
	protected DatabaseDTO update(@RequestBody DatabaseUpdateDTO request, @PathVariable("id") Integer id) throws MissingDatabaseException {
		final Database updated = databaseService.update(id, request);
		final DatabaseDTO mapped = map(updated);
		return mapped;
	}

	@DeleteMapping("/{id}")
	@RolesAllowed("admin")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	protected void delete(@PathVariable("id") @Valid @NotNull Integer id) throws MissingDatabaseException {
		databaseService.delete(id);
	}

	private final DatabaseDTO map(final Database database) {
		return mapper.map(database, DatabaseDTO.class);
	}

	private Collection<DatabaseDTO> map(final Collection<Database> databases) {
		return mapper.map(databases, new TypeToken<Collection<DatabaseDTO>>() {
		}.getType());
	}
}
