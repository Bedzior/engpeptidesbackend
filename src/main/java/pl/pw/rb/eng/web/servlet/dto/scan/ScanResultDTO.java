package pl.pw.rb.eng.web.servlet.dto.scan;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import pl.pw.rb.eng.db.entities.ScanStatus;

public class ScanResultDTO extends ScanDTO {
	@JsonProperty("result")
	private String result;

	public ScanResultDTO() {
		super();
	}

	public ScanResultDTO(String token, String parameters, ScanStatus status, Date created, Date started, Date finished, String result) {
		super(token, parameters, status, created, started, finished);
		this.result = result;
	}
}
