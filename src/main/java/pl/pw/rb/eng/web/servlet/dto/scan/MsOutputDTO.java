package pl.pw.rb.eng.web.servlet.dto.scan;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

import java.util.ArrayList;
import java.util.Vector;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

import mscanlib.ms.exp.Sample;
import mscanlib.ms.msms.MsMsProteinHit;
import mscanlib.ms.msms.dbengines.mscandb.io.MScanDbOutFileReader;

/**
 * Mapped from {@link MScanDbOutFileReader}
 * 
 * @author Rafał Będźkowski
 *
 */
@JsonAutoDetect(getterVisibility = NONE, isGetterVisibility = NONE)
public class MsOutputDTO {

	/* filesystem */
	private String fileFormatName;

	/**/
	private MsMsShortFileHeader header;

	private int iTraqType;

	private Vector<Sample> mergedList;

	private double[] phRange;

	private Vector<MsMsProteinHit> proteinsList;

	private MsMsShortQuery[] queries;

	private ArrayList<String> warnings;

	private String[] errors;

	public MsOutputDTO() {
		super();
	}

	public void setFileFormatName(String fileFormatName) {
		this.fileFormatName = fileFormatName;
	}

	public void setHeader(MsMsShortFileHeader header) {
		this.header = header;
	}

	public void setiTraqType(int iTraqType) {
		this.iTraqType = iTraqType;
	}

	public void setMergedList(Vector<Sample> mergedList) {
		this.mergedList = mergedList;
	}

	public void setPhRange(double[] phRange) {
		this.phRange = phRange;
	}

	public void setProteinsList(Vector<MsMsProteinHit> proteinsList) {
		this.proteinsList = proteinsList;
	}

	public void setQueries(MsMsShortQuery[] queries) {
		this.queries = queries;
	}

	public void setWarnings(ArrayList<String> warnings) {
		this.warnings = warnings;
	}

	public void setErrors(String[] errors) {
		this.errors = errors;
	}

	@JsonProperty
	public String getFileFormatName() {
		return fileFormatName;
	}

	public MsMsShortFileHeader getHeader() {
		return header;
	}

	@JsonProperty
	public int getiTraqType() {
		return iTraqType;
	}

	@JsonProperty
	public Vector<Sample> getMergedList() {
		return mergedList;
	}

	@JsonProperty
	public double[] getPhRange() {
		return phRange;
	}

	@JsonProperty
	public Vector<MsMsProteinHit> getProteinsList() {
		return proteinsList;
	}

	@JsonProperty
	public MsMsShortQuery[] getQueries() {
		return queries;
	}

	@JsonProperty
	public ArrayList<String> getWarnings() {
		return warnings;
	}

	@JsonProperty
	public String[] getErrors() {
		return errors;
	}
}
