package pl.pw.rb.eng.web.servlet.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pl.pw.rb.eng.db.entities.ScanStatus;
import pl.pw.rb.eng.service.ScannerService;
import pl.pw.rb.eng.web.servlet.dto.ServerStatusDTO;

@RestController
@RequestMapping("/secured/admin/status")
class AdminServlet {

	@Autowired
	ScannerService scanner;

	@ResponseBody
	@GetMapping
	ServerStatusDTO getStatus() {
		final ServerStatusDTO serverStatusDTO = new ServerStatusDTO(scanner.count(ScanStatus.RUNNING), scanner.count(ScanStatus.WAITING));
		return serverStatusDTO;
	}
}
