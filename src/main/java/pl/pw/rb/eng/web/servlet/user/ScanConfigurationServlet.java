package pl.pw.rb.eng.web.servlet.user;

import static java.text.MessageFormat.format;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.pw.rb.eng.service.DatabaseService;
import pl.pw.rb.eng.service.ScanConfigurationService;
import pl.pw.rb.eng.web.servlet.dto.scan.ScanOptions;
import pl.pw.rb.eng.web.servlet.exception.WrongConfigurationPathVariable;

@RestController
@RequestMapping(path = "/secured/config")
class ScanConfigurationServlet {

	private Logger log4j = Logger.getLogger(ScanConfigurationServlet.class);

	@Autowired
	private DatabaseService dbService;

	@Autowired
	private ScanConfigurationService configService;

	@GetMapping(path = "/{mode}")
	protected ScanOptions doGet(@PathVariable("mode") String mode) throws WrongConfigurationPathVariable {
		ScanOptions options = new ScanOptions();
		switch (mode) {
		default:
			log4j.warn(format("An incorrect request was made: no {0} type search configuration; available values are 'simple' and 'advanced'", mode));
			throw new WrongConfigurationPathVariable(mode);
		case "simple":
			log4j.info("Loading simple search configuration values");
			options.setEnzymes(configService.getEnzymes(true));
			break;
		case "advanced":
			log4j.info("Loading advanced search configuration values");
			options.setEnzymes(configService.getEnzymes(false));
			break;
		}
		options.setPTMs(configService.getAvailablePTMs());
		options.setMMDUnits(configService.getMMDUnits());
		options.setInstruments(configService.getFragmentationRules());
		options.setDatabases(dbService.getSimpleDatabasesInfo());
		return options;
	}

}
