package pl.pw.rb.eng.web.servlet.dto.scan;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Vector;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonGetter;

import mscanlib.ms.db.DB;
import mscanlib.ms.exp.Sample;
import mscanlib.ms.mass.Enzyme;
import mscanlib.ms.mass.PTMSymbolsMap;
import mscanlib.ms.msms.MsMsInstrument;
import mscanlib.ms.msms.io.MsMsScanConfig;
import mscanlib.ms.msms.io.MsMsScanInfo;

@JsonAutoDetect(getterVisibility = Visibility.NONE, isGetterVisibility = Visibility.NONE)
public class MsMsShortFileHeader {

	private String mSigThresholdType = null;

	private double mSigThreshold = 0.0;

	private double mSigLevel = 0.05;

	private Date mTimeStamp = null;

	private String mTypeOfSearch = null;

	private MsMsInstrument mInstrument = null;

	private String mQuantitation = null;

	private String mParentMassValues = null;

	private String mFragmentMassValues = null;

	private String mProteinMass = null;

	private String mCharge = null;

	private boolean mPercolator = false;

	private boolean mErrorTolerant = false;

	private int mDecoy = -1;

	private Vector<DB> mDBs = null;

	private Enzyme mEnzyme = null;

	private int mMissedCleavages = -1;

	private Vector<mscanlib.ms.mass.PTM> mFixedPtms = null;

	private Vector<mscanlib.ms.mass.PTM> mVariablePtms = null;

	private PTMSymbolsMap mVariablePtmsMap = null;

	private boolean mMerged = false;

	private int mMergeType = -1;

	private Vector<Sample> mMergedList = null;

	private double mParentPosMMD = -1.0;

	private double mParentNegMMD = -1.0;

	private int mParentMMDUnit = 0;

	private boolean mParentCalibrated = false;

	private double mFragmentPosMMD = -1.0;

	private double mFragmentNegMMD = -1.0;

	private int mFragmentMMDUnit = 1;

	private boolean mFragmentCalibrated = false;

	private boolean mRtInSeconds = false;

	private MsMsScanInfo mScanInfo = null;

	private MsMsScanConfig mScanConfig = null;

	private int mQueriesCount = 0;

	private LinkedHashMap<String, String> mDBEngineParams = null;

	@JsonGetter
	public String getSigThresholdType() {
		return mSigThresholdType;
	}

	public void setSigThresholdType(String mSigThresholdType) {
		this.mSigThresholdType = mSigThresholdType;
	}

	@JsonGetter
	public double getSigThreshold() {
		return mSigThreshold;
	}

	public void setSigThreshold(double mSigThreshold) {
		this.mSigThreshold = mSigThreshold;
	}

	@JsonGetter
	public double getSigLevel() {
		return mSigLevel;
	}

	public void setSigLevel(double mSigLevel) {
		this.mSigLevel = mSigLevel;
	}

	@JsonGetter
	public Date getTimeStamp() {
		return mTimeStamp;
	}

	public void setTimeStamp(Date mTimeStamp) {
		this.mTimeStamp = mTimeStamp;
	}

	@JsonGetter
	public String getTypeOfSearch() {
		return mTypeOfSearch;
	}

	public void setTypeOfSearch(String mTypeOfSearch) {
		this.mTypeOfSearch = mTypeOfSearch;
	}

	@JsonGetter
	public MsMsInstrument getInstrument() {
		return mInstrument;
	}

	public void setInstrument(MsMsInstrument mInstrument) {
		this.mInstrument = mInstrument;
	}

	@JsonGetter
	public String getQuantitation() {
		return mQuantitation;
	}

	public void setQuantitation(String mQuantitation) {
		this.mQuantitation = mQuantitation;
	}

	@JsonGetter
	public String getParentMassValues() {
		return mParentMassValues;
	}

	public void setParentMassValues(String mParentMassValues) {
		this.mParentMassValues = mParentMassValues;
	}

	@JsonGetter
	public String getFragmentMassValues() {
		return mFragmentMassValues;
	}

	public void setFragmentMassValues(String mFragmentMassValues) {
		this.mFragmentMassValues = mFragmentMassValues;
	}

	@JsonGetter
	public String getProteinMass() {
		return mProteinMass;
	}

	public void setProteinMass(String mProteinMass) {
		this.mProteinMass = mProteinMass;
	}

	@JsonGetter
	public String getCharge() {
		return mCharge;
	}

	public void setCharge(String mCharge) {
		this.mCharge = mCharge;
	}

	@JsonGetter
	public boolean ismPercolator() {
		return mPercolator;
	}

	public void setPercolator(boolean mPercolator) {
		this.mPercolator = mPercolator;
	}

	@JsonGetter
	public boolean ismErrorTolerant() {
		return mErrorTolerant;
	}

	public void setErrorTolerant(boolean mErrorTolerant) {
		this.mErrorTolerant = mErrorTolerant;
	}

	@JsonGetter
	public int getDecoy() {
		return mDecoy;
	}

	public void setDecoy(int mDecoy) {
		this.mDecoy = mDecoy;
	}

	@JsonGetter
	public Vector<DB> getDBs() {
		return mDBs;
	}

	public void setDBs(Vector<DB> mDBs) {
		this.mDBs = mDBs;
	}

//	@JsonGetter
	public Enzyme getEnzyme() {
		return mEnzyme;
	}

	public void setEnzyme(Enzyme mEnzyme) {
		this.mEnzyme = mEnzyme;
	}

	@JsonGetter
	public int getMissedCleavages() {
		return mMissedCleavages;
	}

	public void setMissedCleavages(int mMissedCleavages) {
		this.mMissedCleavages = mMissedCleavages;
	}

//	@JsonGetter
	public Vector<mscanlib.ms.mass.PTM> getFixedPtms() {
		return mFixedPtms;
	}

	public void setFixedPtms(Vector<mscanlib.ms.mass.PTM> mFixedPtms) {
		this.mFixedPtms = mFixedPtms;
	}

//	@JsonGetter
	public Vector<mscanlib.ms.mass.PTM> getVariablePtms() {
		return mVariablePtms;
	}

	public void setVariablePtms(Vector<mscanlib.ms.mass.PTM> mVariablePtms) {
		this.mVariablePtms = mVariablePtms;
	}

	@JsonGetter
	public boolean ismMerged() {
		return mMerged;
	}

	public void setMerged(boolean mMerged) {
		this.mMerged = mMerged;
	}

	@JsonGetter
	public int getMergeType() {
		return mMergeType;
	}

	public void setMergeType(int mMergeType) {
		this.mMergeType = mMergeType;
	}

	@JsonGetter
	public Vector<Sample> getMergedList() {
		return mMergedList;
	}

	public void setMergedList(Vector<Sample> mMergedList) {
		this.mMergedList = mMergedList;
	}

	@JsonGetter
	public double getParentPosMMD() {
		return mParentPosMMD;
	}

	public void setParentPosMMD(double mParentPosMMD) {
		this.mParentPosMMD = mParentPosMMD;
	}

	@JsonGetter
	public double getParentNegMMD() {
		return mParentNegMMD;
	}

	public void setParentNegMMD(double mParentNegMMD) {
		this.mParentNegMMD = mParentNegMMD;
	}

	@JsonGetter
	public int getParentMMDUnit() {
		return mParentMMDUnit;
	}

	public void setParentMMDUnit(int mParentMMDUnit) {
		this.mParentMMDUnit = mParentMMDUnit;
	}

	@JsonGetter
	public boolean ismParentCalibrated() {
		return mParentCalibrated;
	}

	public void setParentCalibrated(boolean mParentCalibrated) {
		this.mParentCalibrated = mParentCalibrated;
	}

	@JsonGetter
	public double getFragmentPosMMD() {
		return mFragmentPosMMD;
	}

	public void setFragmentPosMMD(double mFragmentPosMMD) {
		this.mFragmentPosMMD = mFragmentPosMMD;
	}

	@JsonGetter
	public double getFragmentNegMMD() {
		return mFragmentNegMMD;
	}

	public void setFragmentNegMMD(double mFragmentNegMMD) {
		this.mFragmentNegMMD = mFragmentNegMMD;
	}

	@JsonGetter
	public int getFragmentMMDUnit() {
		return mFragmentMMDUnit;
	}

	public void setFragmentMMDUnit(int mFragmentMMDUnit) {
		this.mFragmentMMDUnit = mFragmentMMDUnit;
	}

	@JsonGetter
	public boolean ismFragmentCalibrated() {
		return mFragmentCalibrated;
	}

	public void setFragmentCalibrated(boolean mFragmentCalibrated) {
		this.mFragmentCalibrated = mFragmentCalibrated;
	}

	@JsonGetter
	public boolean ismRtInSeconds() {
		return mRtInSeconds;
	}

	public void setRtInSeconds(boolean mRtInSeconds) {
		this.mRtInSeconds = mRtInSeconds;
	}

	@JsonGetter
	public MsMsScanInfo getScanInfo() {
		return mScanInfo;
	}

	public void setScanInfo(MsMsScanInfo mScanInfo) {
		this.mScanInfo = mScanInfo;
	}

	@JsonGetter
	public MsMsScanConfig getScanConfig() {
		return mScanConfig;
	}

	public void setScanConfig(MsMsScanConfig mScanConfig) {
		this.mScanConfig = mScanConfig;
	}

	@JsonGetter
	public int getQueriesCount() {
		return mQueriesCount;
	}

	public void setQueriesCount(int mQueriesCount) {
		this.mQueriesCount = mQueriesCount;
	}

	@JsonGetter
	public LinkedHashMap<String, String> getDBEngineParams() {
		return mDBEngineParams;
	}

	public void setDBEngineParams(LinkedHashMap<String, String> mDBEngineParams) {
		this.mDBEngineParams = mDBEngineParams;
	}

	public PTMSymbolsMap getVariablePtmsMap() {
		return mVariablePtmsMap;
	}

	//@JsonGetter
	public void setVariablePtmsMap(PTMSymbolsMap mVariablePtmsMap) {
		this.mVariablePtmsMap = mVariablePtmsMap;
	}
}
