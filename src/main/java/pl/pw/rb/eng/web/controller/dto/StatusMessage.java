package pl.pw.rb.eng.web.controller.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonGetter;

import pl.pw.rb.eng.db.dto.Message;

/**
 * Generic status message inherited by all other messages sent via websocket
 * 
 * @author Rafał Będźkowski
 *
 */
@JsonAutoDetect
public abstract class StatusMessage extends Message {

	private boolean success;

	public StatusMessage(boolean status, String message) {
		super(message);
		this.success = status;
	}

	@JsonGetter
	public boolean isSuccess() {
		return success;
	}

}
