package pl.pw.rb.eng.web.servlet.exception.user;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Username already registered")
@TranslationToken("ERR_USER_EXISTS")
public class UserAlreadyExistsException extends ServletException {

	private static final long serialVersionUID = 3221338590293531697L;

}
