package pl.pw.rb.eng.web.servlet.admin;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import pl.pw.rb.eng.db.entities.EmailTemplate;
import pl.pw.rb.eng.service.TemplateService;
import pl.pw.rb.eng.web.servlet.admin.exception.MissingTemplateBodyException;
import pl.pw.rb.eng.web.servlet.dto.EmailTemplateUpdateDTO;
import pl.pw.rb.eng.web.servlet.exception.template.EmailTemplateNotFound;

@RestController
@RequestMapping(value = "/secured/admin/templates", produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_JSON_UTF8_VALUE })
class EmailTemplateServlet {

	@Autowired
	protected TemplateService service;

	@Autowired
	protected ModelMapper mapper;

	@GetMapping
	@ResponseBody
	private Collection<EmailTemplate> get() {
		return service.list();
	}

	@GetMapping("/{id}")
	@ResponseBody
	private EmailTemplate get(@PathVariable int id) throws EmailTemplateNotFound {
		EmailTemplate template = service.getTemplate(id);
		if (template == null)
			throw new EmailTemplateNotFound(id);
		return template;
	}

	@GetMapping("/{name}/{type}")
	@ResponseBody
	private EmailTemplate get(@PathVariable String name, @PathVariable String type) throws EmailTemplateNotFound {
		EmailTemplate template = service.getTemplate(name, type);
		if (template == null)
			throw new EmailTemplateNotFound(name, type);
		return template;
	}

	@PutMapping("/{id}")
	@ResponseBody
	private EmailTemplate update(@RequestBody EmailTemplateUpdateDTO payload, @PathVariable int id) throws MissingTemplateBodyException {
		if (StringUtils.isBlank(payload.getBody()))
			throw new MissingTemplateBodyException();
		EmailTemplate template = service.getTemplate(id);
		mapper.map(payload, template);
		final EmailTemplate updated = service.update(template);
		return updated;
	}
}
