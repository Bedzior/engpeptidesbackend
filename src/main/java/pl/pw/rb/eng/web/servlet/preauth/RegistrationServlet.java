package pl.pw.rb.eng.web.servlet.preauth;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.service.exception.RegistrationEmailNotSentException;
import pl.pw.rb.eng.web.servlet.dto.UserRegistration;
import pl.pw.rb.eng.web.servlet.exception.user.EmailAlreadyRegisteredException;
import pl.pw.rb.eng.web.servlet.exception.user.NoUserException;
import pl.pw.rb.eng.web.servlet.exception.user.UserAlreadyActivatedException;
import pl.pw.rb.eng.web.servlet.exception.user.UserAlreadyExistsException;

@RestController
@RequestMapping(path = "/register")
class RegistrationServlet {

	@Autowired
	private UsersService userService;

	@PostMapping
	@ResponseStatus(HttpStatus.ACCEPTED)
	void doPost(@RequestBody @Valid UserRegistration form) 
			throws EmailAlreadyRegisteredException, UserAlreadyExistsException, RegistrationEmailNotSentException {
		userService.createUser(form);
	}

	@PostMapping
	@RequestMapping(path = "/confirm", params = {"uuid"})
	void confirm(@RequestParam(name="uuid") String uuid) throws NoUserException, UserAlreadyActivatedException {
		userService.activateUser(uuid);
	}

}
