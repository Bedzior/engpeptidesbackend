package pl.pw.rb.eng.web.dto;

import static java.text.MessageFormat.format;

import org.apache.commons.lang3.ClassUtils;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Represents a named configuration value
 * 
 * @author Rafał Będźkowski
 */
@JsonAutoDetect
public class Configuration {

	private String name;

	private String description;

	private Object value;

	public Configuration() {
		super();
	}

	public Configuration(String name, String description, Object value) {
		super();
		this.name = name;
		this.description = description;
		this.value = value;
	}

	@JsonProperty
	public String getName() {
		return this.name;
	}

	@JsonProperty
	public String getDescription() {
		return this.description;
	}

	@JsonProperty
	public Object getValue() {
		return this.value;
	}

	@JsonProperty
	public String getType() {
		final Class<? extends Object> _class = this.value.getClass();
		if (_class.equals(String.class)) {
			return "string";
		} else if (ClassUtils.isAssignable(Long.class, _class) || ClassUtils.isAssignable(Integer.class, _class)
				|| ClassUtils.isAssignable(Byte.class, _class)) {
			return "number";
		} else if (ClassUtils.isAssignable(Boolean.class, _class)) {
			return "boolean";
		}
		return _class.getName();
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return format("{0} = {1}", name, value);
	}
}
