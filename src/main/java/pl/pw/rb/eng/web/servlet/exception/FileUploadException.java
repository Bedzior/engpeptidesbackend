package pl.pw.rb.eng.web.servlet.exception;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(reason = "Unexpected error while transferring file", code = HttpStatus.INTERNAL_SERVER_ERROR)
@TranslationToken("ERR_UNEXPECTED")
public class FileUploadException extends ServletException {

	private static final long serialVersionUID = -7394570904288949589L;

}
