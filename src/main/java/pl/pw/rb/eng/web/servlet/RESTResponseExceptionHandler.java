package pl.pw.rb.eng.web.servlet;

import static java.util.stream.Collectors.toSet;

import static java.text.MessageFormat.format;
import java.util.Set;

import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import pl.pw.rb.eng.web.controller.dto.ErrorStatusMessage;
import pl.pw.rb.eng.web.controller.dto.FieldsErrorMessage;
import pl.pw.rb.eng.web.controller.dto.TranslatableErrorMessage;

@ControllerAdvice
public class RESTResponseExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler({ ServletException.class })
	public ResponseEntity<ErrorStatusMessage> handleServletExceptions(Exception e, WebRequest req) {
		ResponseStatus annotation = AnnotatedElementUtils.findMergedAnnotation(e.getClass(), ResponseStatus.class);
		if (annotation == null) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new TranslatableErrorMessage("Unexpected server error; please contact the administrator", "UNEXPECTED"));
		}
		String reason = annotation.reason();
		if (StringUtils.isEmpty(reason))
			reason = e.getMessage();
		ErrorStatusMessage body;
		if (AnnotatedElementUtils.hasAnnotation(e.getClass(), TranslationToken.class)) {
			TranslationToken translationToken = AnnotatedElementUtils.findMergedAnnotation(e.getClass(), TranslationToken.class);
			body = new TranslatableErrorMessage(reason, translationToken.value());
			return ResponseEntity.status(annotation.code()).body(body);
		}
		throw new RuntimeException(format("Missing internationalization token for {0}", e.getClass().getName()));
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status,
			WebRequest request) {
		Set<String> fields = ex.getBindingResult().getFieldErrors().stream().map(FieldError::getField).collect(toSet());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).headers(headers)
				.body(new FieldsErrorMessage("Incorrect input parameters", fields.toArray(new String[fields.size()])));
	}

	@Override
	protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		return super.handleBindException(ex, headers, status, request);
	}

}
