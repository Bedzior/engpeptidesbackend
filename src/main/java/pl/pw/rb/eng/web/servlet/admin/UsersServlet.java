package pl.pw.rb.eng.web.servlet.admin;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.Collection;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import pl.pw.rb.eng.db.entities.User;
import pl.pw.rb.eng.service.UsersService;
import pl.pw.rb.eng.web.servlet.exception.user.NoUserException;

@RestController
@RequestMapping("/secured/admin/users")
class UsersServlet {

	@Autowired
	UsersService usersService;

	@Autowired
	ModelMapper mapper;

	@GetMapping(produces = { APPLICATION_JSON_VALUE, APPLICATION_JSON_UTF8_VALUE })
	@ResponseStatus(code = HttpStatus.OK)
	@ResponseBody
	private Collection<User> list() {
		final Page<User> users = usersService.listAll();
		return users.getContent();
	}

	@PutMapping(path = "/{id}", consumes = { APPLICATION_JSON_UTF8_VALUE, APPLICATION_JSON_VALUE }, produces = { APPLICATION_JSON_VALUE,
			APPLICATION_JSON_UTF8_VALUE })
	@ResponseStatus(code = HttpStatus.OK)
	@ResponseBody
	private User update(@PathVariable Integer id, @RequestBody User req) //
			throws NoUserException {
		return this.usersService.update(req);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	private void delete(@PathVariable Integer id) {
		User user = usersService.getByID(id);
		if (user != null)
			usersService.delete(user.getLogin());
		else
			throw new ResourceNotFoundException();
	}
}
