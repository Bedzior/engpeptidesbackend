package pl.pw.rb.eng.web.servlet.exception.db;

import javax.servlet.ServletException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import pl.pw.rb.eng.web.servlet.TranslationToken;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "No database source specified (url/path parameter))")
@TranslationToken("ERR_NO_SOURCE")
public class NoDatabaseSourceException extends ServletException {

	private static final long serialVersionUID = 1039503313847987009L;

}
