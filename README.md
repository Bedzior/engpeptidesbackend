# README #

Jetty backend project.

Jetty configuration placed in ./config/jetty.

More servlet containers' compatibility might be introduced in the future.

## Testing ##

Testing done with JUnit. However, full server-side testing requires another project.